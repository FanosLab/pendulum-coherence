# PENDULUM: Time-Based Coherence Protocol for Mixed Criticality Systems

* This is the repository for the source code of PENDULUM (initial version was named HourGlass) in gem5.

# PENDULUM specific files:
* Gem5 is modified to support snooping bus architecture with a criticality-aware arbitration scheme.
* File `build_opts/X86_MSI_Snooping_Time_based` provides the build configuration settings (architecure and the protocol to be used).
* The configuration file for PENDULUM in `configs/ruby/X86_MSI_Snooping_Time_based.py` provides settings for the memory hierarchy and network.
* The PENDULUM protocol states and transitions are included in `src/mem/protocol/X86_MSI_Snooping_Time_based*` files. They are written in [SLICC] (http://gem5.org/SLICC).
* The bus arbitration details are included in src/mem/ruby/network/simple/Throttle.cc 
* Gem5 is also extended to support trace-based simulations by modifying the Ruby Random Tester to read traces from a trace.trc file.
    * Trace.trc file contains all requests in the form - `Addr Read/Write ArrivalTimeOfRequest`. 
    * The requests in the trace.trc file are replicated on all cores with same time-stamp. This gives maximum sharing of data across the cores.

# PENDULUM initial configurations:
* The configuration details for the system are provided in src/cpu/testers/rubytest/Trace.hh
    * `NPROC_C` - number of critical cores.
    * `NPROC_NC` - number of non-critical cores.
    * `NPROC` - total number of cores - NPROC_C+NPROC_NC.
    * `SLOT_WIDTH` - TDM slot width set in terms of cycles.
    * `ALL_CRITICAL` - This is set to 1 when we want to disable mixed criticality and use all the cores with same level of criticality.
        * While using `ALL_CRITICAL` mode, NPROC_C should be set equal to NPROC.
    * `TRACE` - This is set to 1 to run trace-based simulations.
* The initial values for timers v(cr,cr), v(cr,ncr), v(ncr,cr) and v(ncr,ncr) can be set at run-time with the options `timeout_cc`, `timeout_cn`, `timeout_nc` and `timeout_nn` in terms of cycles.

# Building gem5 with PENDULUM protocol:
* `scons ./build/X86_MSI_Snooping_Time_based/gem5.opt -j8`

# Running gem5:
* To run PENDULUM, gem5 should be run with the options `--ruby`, `--cpu-type=timing` and `--mem-type=SimpleMemory` (in-order cores).
* This version of gem5 accepts only static executables.
* Multi-threaded workloads should be compiled with `static` flag and include [m5threads] (http://repo.gem5.org/m5threads), pthread library for gem5, instead of the standard pthread library.

* Running multithreaded application, say FFT of SPLASH-2, on gem5:
    * `./build/X86_MSI_Snooping_Time_based/gem5.opt ./configs/example/se.py --ruby -n4 --cpu-type=timing --l1d_size=16kB --l1i_size=16kB --l1d_assoc=1 --l1i_assoc=1 --mem-size=4194304kB --topology=Crossbar --cpu-clock=2GHz --ruby-clock=2GHz --mem-type=SimpleMemory --timeout_cc=200 --timeout_cn=400 --timeout_nc=100 --timeout_nn=200 -c /PATH_TO_SPLASH2/codes/kernels/fft/fft -o '-m 10 -p 4 -t'`
    
* Running trace-based simulation: 
    * `./build/X86_MSI_Snooping_Time_based/gem5.opt ./configs/example/ruby_random_test.py --ruby -n4 --mem-size=4194304kB --topology=Crossbar --cpu-clock=2GHz --ruby-clock=2GHz --mem-type=SimpleMemory --maxloads=1000000 --timeout_cc=200 --timeout_cn=400 --timeout_nc=100 --timeout_nn=200 --wakeup_freq=1`
    * The latency in src/mem/SimpleMemory.py needs to be set to 22ns to trace-based simulation.

* `--debug-flags` can be enabled to understand the network and protocol details. 
* A sample trace.trc file is included in gem5-stable/.
 
# Contact:
* If you have any questions, please don't hesitate to contact [us] (mailto:mohamed.hassan@mcmaster.ca).
