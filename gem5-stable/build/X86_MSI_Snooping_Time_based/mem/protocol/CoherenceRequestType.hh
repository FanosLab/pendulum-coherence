/** \file CoherenceRequestType.hh
 *
 * Auto generated C++ code started by /home/mohamed/hourglass/gem5-stable/src/mem/slicc/symbols/Type.py:454
 */

#ifndef __CoherenceRequestType_HH__
#define __CoherenceRequestType_HH__

#include <iostream>
#include <string>


// Class definition
/** \enum CoherenceRequestType
 *  \brief ...
 */
enum CoherenceRequestType {
    CoherenceRequestType_FIRST,
    CoherenceRequestType_GETM = CoherenceRequestType_FIRST, /**< Get Modified */
    CoherenceRequestType_GETMA, /**< Get Modified approx */
    CoherenceRequestType_GETX, /**< Get Modified */
    CoherenceRequestType_GETS, /**< Get Shared */
    CoherenceRequestType_GETI, /**< Get instruction */
    CoherenceRequestType_INV, /**< Get INV */
    CoherenceRequestType_UPG, /**< Get UPG */
    CoherenceRequestType_PUTM, /**< Put Modified */
    CoherenceRequestType_GETMATOMICEN, /**< Atomic RMW end */
    CoherenceRequestType_GETMATOMICST, /**< Atomic RMW start */
    CoherenceRequestType_WB_ACK, /**< Writeback ack */
    CoherenceRequestType_DMA_READ, /**< DMA Read */
    CoherenceRequestType_DMA_WRITE, /**< DMA Write */
    CoherenceRequestType_SELFINV, /**< invalidating the sharer */
    CoherenceRequestType_SENDDATA, /**< message to indicate cache to cache transfer */
    CoherenceRequestType_ALLINV, /**< all sharers are invalidated */
    CoherenceRequestType_NUM
};

// Code to convert from a string to the enumeration
CoherenceRequestType string_to_CoherenceRequestType(const std::string& str);

// Code to convert state to a string
std::string CoherenceRequestType_to_string(const CoherenceRequestType& obj);

// Code to increment an enumeration type
CoherenceRequestType &operator++(CoherenceRequestType &e);
std::ostream& operator<<(std::ostream& out, const CoherenceRequestType& obj);

#endif // __CoherenceRequestType_HH__
