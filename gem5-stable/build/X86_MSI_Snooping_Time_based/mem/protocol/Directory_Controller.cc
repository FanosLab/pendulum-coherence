/** \file Directory_Controller.cc
 *
 * Auto generated C++ code started by /home/mohamed/hourglass/gem5-stable/src/mem/slicc/symbols/StateMachine.py:449
 * Created by slicc definition of Module "MSI Two Level directory protocol"
 */

#include <sys/types.h>
#include <unistd.h>

#include <cassert>
#include <sstream>
#include <string>

#include "base/compiler.hh"
#include "base/cprintf.hh"
#include "debug/RubyGenerated.hh"
#include "debug/RubySlicc.hh"
#include "mem/protocol/Directory_Controller.hh"
#include "mem/protocol/Directory_Event.hh"
#include "mem/protocol/Directory_State.hh"
#include "mem/protocol/Types.hh"
#include "mem/ruby/common/Global.hh"
#include "mem/ruby/system/System.hh"
#include "mem/ruby/slicc_interface/RubySlicc_includes.hh"

using namespace std;
#include "mem/protocol/TBETable.hh"
Directory_Controller *
Directory_ControllerParams::create()
{
    return new Directory_Controller(this);
}

int Directory_Controller::m_num_controllers = 0;
std::vector<Stats::Vector *>  Directory_Controller::eventVec;
std::vector<std::vector<Stats::Vector *> >  Directory_Controller::transVec;

// for adding information to the protocol debug trace
stringstream Directory_transitionComment;

#ifndef NDEBUG
#define APPEND_TRANSITION_COMMENT(str) (Directory_transitionComment << str)
#else
#define APPEND_TRANSITION_COMMENT(str) do {} while (0)
#endif

/** \brief constructor */
Directory_Controller::Directory_Controller(const Params *p)
    : AbstractController(p)
{
    m_machineID.type = MachineType_Directory;
    m_machineID.num = m_version;
    m_num_controllers++;

    m_in_ports = 4;
    m_directory_ptr = p->directory;
    m_to_mem_ctrl_latency = p->to_mem_ctrl_latency;
    m_to_bus_ack_latency = p->to_bus_ack_latency;
    m_directory_latency = p->directory_latency;
    m_l1_request_latency = p->l1_request_latency;

    for (int state = 0; state < Directory_State_NUM; state++) {
        for (int event = 0; event < Directory_Event_NUM; event++) {
            m_possible[state][event] = false;
            m_counters[state][event] = 0;
        }
    }
    for (int event = 0; event < Directory_Event_NUM; event++) {
        m_event_counters[event] = 0;
    }
}

void
Directory_Controller::setNetQueue(const std::string& name, MessageBuffer *b)
{
  
    MachineType machine_type = string_to_MachineType("Directory");
    int base M5_VAR_USED = MachineType_base_number(machine_type);


    if ("requestToDir" == name) {
        m_requestToDir_ptr = b;
        assert(m_requestToDir_ptr != NULL);
        m_net_ptr->setFromNetQueue(m_version + base, false, 2,
                                         "request", b);
        m_requestToDir_ptr->setReceiver(this);
        m_requestToDir_ptr->setOrdering(false);
        m_requestToDir_ptr->resize(m_buffer_size);
        m_requestToDir_ptr->setRecycleLatency(m_recycle_latency);
        m_requestToDir_ptr->setDescription("[Version " + to_string(m_version) + ", Directory, name=requestToDir]");
    }
    if ("requestToDir_WB" == name) {
        m_requestToDir_WB_ptr = b;
        assert(m_requestToDir_WB_ptr != NULL);
        m_net_ptr->setFromNetQueue(m_version + base, false, 6,
                                         "request", b);
        m_requestToDir_WB_ptr->setReceiver(this);
        m_requestToDir_WB_ptr->setOrdering(false);
        m_requestToDir_WB_ptr->resize(m_buffer_size);
        m_requestToDir_WB_ptr->setRecycleLatency(m_recycle_latency);
        m_requestToDir_WB_ptr->setDescription("[Version " + to_string(m_version) + ", Directory, name=requestToDir_WB]");
    }
    if ("responseToDir" == name) {
        m_responseToDir_ptr = b;
        assert(m_responseToDir_ptr != NULL);
        m_net_ptr->setFromNetQueue(m_version + base, false, 4,
                                         "response", b);
        m_responseToDir_ptr->setReceiver(this);
        m_responseToDir_ptr->setOrdering(false);
        m_responseToDir_ptr->resize(m_buffer_size);
        m_responseToDir_ptr->setRecycleLatency(m_recycle_latency);
        m_responseToDir_ptr->setDescription("[Version " + to_string(m_version) + ", Directory, name=responseToDir]");
    }
    if ("responseFromDir" == name) {
        m_responseFromDir_ptr = b;
        assert(m_responseFromDir_ptr != NULL);
        m_net_ptr->setToNetQueue(m_version + base, false, 4,
                                         "response", b);
        m_responseFromDir_ptr->setSender(this);
        m_responseFromDir_ptr->setOrdering(false);
        m_responseFromDir_ptr->resize(m_buffer_size);
        m_responseFromDir_ptr->setRecycleLatency(m_recycle_latency);
        m_responseFromDir_ptr->setDescription("[Version " + to_string(m_version) + ", Directory, name=responseFromDir]");
    }
    if ("requestFromDir_WB" == name) {
        m_requestFromDir_WB_ptr = b;
        assert(m_requestFromDir_WB_ptr != NULL);
        m_net_ptr->setToNetQueue(m_version + base, false, 6,
                                         "requestWB", b);
        m_requestFromDir_WB_ptr->setSender(this);
        m_requestFromDir_WB_ptr->setOrdering(false);
        m_requestFromDir_WB_ptr->resize(m_buffer_size);
        m_requestFromDir_WB_ptr->setRecycleLatency(m_recycle_latency);
        m_requestFromDir_WB_ptr->setDescription("[Version " + to_string(m_version) + ", Directory, name=requestFromDir_WB]");
    }
}

void
Directory_Controller::init()
{
    // initialize objects

    m_TBEs_ptr  = new TBETable<Directory_TBE>(m_number_of_TBEs);
    assert(m_TBEs_ptr != NULL);


    (*m_requestToDir_ptr).setConsumer(this);
    (*m_requestToDir_ptr).setDescription("[Version " + to_string(m_version) + ", Directory, requestNetwork_in]");
    (*m_requestToDir_WB_ptr).setConsumer(this);
    (*m_requestToDir_WB_ptr).setDescription("[Version " + to_string(m_version) + ", Directory, requestNetworkWB_in]");
    (*m_responseToDir_ptr).setConsumer(this);
    (*m_responseToDir_ptr).setDescription("[Version " + to_string(m_version) + ", Directory, responseNetwork_in]");
    (*m_responseFromMemory_ptr).setConsumer(this);
    (*m_responseFromMemory_ptr).setDescription("[Version " + to_string(m_version) + ", Directory, memQueue_in]");

    possibleTransition(Directory_State_I, Directory_Event_GETS_C);
    possibleTransition(Directory_State_I, Directory_Event_GETS_NC);
    possibleTransition(Directory_State_ISA_C, Directory_Event_DATAACK);
    possibleTransition(Directory_State_ISA_NC, Directory_Event_DATAACK);
    possibleTransition(Directory_State_SSA_C, Directory_Event_DATAACK);
    possibleTransition(Directory_State_SSA_NC, Directory_Event_DATAACK);
    possibleTransition(Directory_State_S, Directory_Event_DATAACK);
    possibleTransition(Directory_State_SM_NC, Directory_Event_DATAACK);
    possibleTransition(Directory_State_SM_C, Directory_Event_DATAACK);
    possibleTransition(Directory_State_I, Directory_Event_DATAACK);
    possibleTransition(Directory_State_SSA_NC, Directory_Event_InvLatest);
    possibleTransition(Directory_State_SSA_C, Directory_Event_InvLatest);
    possibleTransition(Directory_State_IS, Directory_Event_Memory_Data);
    possibleTransition(Directory_State_ISA_C, Directory_Event_GETM_C);
    possibleTransition(Directory_State_ISA_C, Directory_Event_GETM_NC);
    possibleTransition(Directory_State_ISA_C, Directory_Event_GETS_C);
    possibleTransition(Directory_State_ISA_C, Directory_Event_GETS_NC);
    possibleTransition(Directory_State_ISA_NC, Directory_Event_GETS_C);
    possibleTransition(Directory_State_ISA_NC, Directory_Event_GETS_NC);
    possibleTransition(Directory_State_ISA_NC, Directory_Event_GETM_NC);
    possibleTransition(Directory_State_ISA_NC, Directory_Event_GETM_C);
    possibleTransition(Directory_State_S, Directory_Event_GETS_C);
    possibleTransition(Directory_State_S, Directory_Event_GETS_NC);
    possibleTransition(Directory_State_SSA_C, Directory_Event_GETS_C);
    possibleTransition(Directory_State_SSA_C, Directory_Event_GETS_NC);
    possibleTransition(Directory_State_SSA_C, Directory_Event_GETM_C);
    possibleTransition(Directory_State_SSA_C, Directory_Event_GETM_NC);
    possibleTransition(Directory_State_SSA_NC, Directory_Event_GETS_C);
    possibleTransition(Directory_State_SSA_NC, Directory_Event_GETS_NC);
    possibleTransition(Directory_State_SSA_NC, Directory_Event_GETM_NC);
    possibleTransition(Directory_State_SSA_NC, Directory_Event_GETM_C);
    possibleTransition(Directory_State_S, Directory_Event_InvLatest);
    possibleTransition(Directory_State_S, Directory_Event_GETM_C);
    possibleTransition(Directory_State_S, Directory_Event_GETM_NC);
    possibleTransition(Directory_State_SM_NC, Directory_Event_InvLatest);
    possibleTransition(Directory_State_SM_C, Directory_Event_InvLatest);
    possibleTransition(Directory_State_SM_C, Directory_Event_GETM_C);
    possibleTransition(Directory_State_SM_C, Directory_Event_GETM_NC);
    possibleTransition(Directory_State_SM_C, Directory_Event_GETS_C);
    possibleTransition(Directory_State_SM_C, Directory_Event_GETS_NC);
    possibleTransition(Directory_State_SM_NC, Directory_Event_GETM_NC);
    possibleTransition(Directory_State_SM_NC, Directory_Event_GETS_NC);
    possibleTransition(Directory_State_SM_NC, Directory_Event_GETM_C);
    possibleTransition(Directory_State_SM_NC, Directory_Event_GETS_C);
    possibleTransition(Directory_State_I, Directory_Event_GETM_C);
    possibleTransition(Directory_State_I, Directory_Event_GETM_NC);
    possibleTransition(Directory_State_IMA_C, Directory_Event_DATAACK);
    possibleTransition(Directory_State_IMA_NC, Directory_Event_DATAACK);
    possibleTransition(Directory_State_IMA_NC, Directory_Event_GETM_C);
    possibleTransition(Directory_State_IMA_NC, Directory_Event_GETS_C);
    possibleTransition(Directory_State_IMA_C, Directory_Event_GETS_C);
    possibleTransition(Directory_State_IMA_C, Directory_Event_GETS_NC);
    possibleTransition(Directory_State_IMA_C, Directory_Event_GETM_C);
    possibleTransition(Directory_State_IMA_C, Directory_Event_GETM_NC);
    possibleTransition(Directory_State_IMA_NC, Directory_Event_GETS_NC);
    possibleTransition(Directory_State_IMA_NC, Directory_Event_GETM_NC);
    possibleTransition(Directory_State_IM, Directory_Event_Memory_Data);
    possibleTransition(Directory_State_M, Directory_Event_PUTM);
    possibleTransition(Directory_State_S, Directory_Event_PUTM);
    possibleTransition(Directory_State_M_D, Directory_Event_GETM_C);
    possibleTransition(Directory_State_M_D, Directory_Event_GETM_NC);
    possibleTransition(Directory_State_M_D, Directory_Event_GETS_C);
    possibleTransition(Directory_State_M_D, Directory_Event_GETS_NC);
    possibleTransition(Directory_State_M_D, Directory_Event_Data);
    possibleTransition(Directory_State_I, Directory_Event_Memory_Ack);
    possibleTransition(Directory_State_M, Directory_Event_Memory_Ack);
    possibleTransition(Directory_State_IS, Directory_Event_Memory_Ack);
    possibleTransition(Directory_State_IM, Directory_Event_Memory_Ack);
    possibleTransition(Directory_State_M_D, Directory_Event_Memory_Ack);
    possibleTransition(Directory_State_S, Directory_Event_Memory_Ack);
    possibleTransition(Directory_State_SM_NC, Directory_Event_Memory_Ack);
    possibleTransition(Directory_State_SM_C, Directory_Event_Memory_Ack);
    possibleTransition(Directory_State_SSA_NC, Directory_Event_Memory_Ack);
    possibleTransition(Directory_State_SSA_C, Directory_Event_Memory_Ack);
    possibleTransition(Directory_State_IMA_C, Directory_Event_Memory_Ack);
    possibleTransition(Directory_State_IMA_NC, Directory_Event_Memory_Ack);
    possibleTransition(Directory_State_ISA_C, Directory_Event_Memory_Ack);
    possibleTransition(Directory_State_ISA_NC, Directory_Event_Memory_Ack);
    possibleTransition(Directory_State_M, Directory_Event_GETM_C);
    possibleTransition(Directory_State_M, Directory_Event_GETM_NC);
    possibleTransition(Directory_State_M, Directory_Event_GETS_C);
    possibleTransition(Directory_State_M, Directory_Event_GETS_NC);
    possibleTransition(Directory_State_M, Directory_Event_SENDDATA);
    possibleTransition(Directory_State_MA, Directory_Event_GETS_C);
    possibleTransition(Directory_State_MA, Directory_Event_GETS_NC);
    possibleTransition(Directory_State_S_D, Directory_Event_Data);
    possibleTransition(Directory_State_S_D, Directory_Event_GETM_NC);
    possibleTransition(Directory_State_S_D, Directory_Event_GETM_C);
    possibleTransition(Directory_State_S_D, Directory_Event_GETS_C);
    possibleTransition(Directory_State_S_D, Directory_Event_GETS_NC);
    possibleTransition(Directory_State_MA, Directory_Event_GETM_C);
    possibleTransition(Directory_State_MA, Directory_Event_GETM_NC);
    AbstractController::init();
    resetStats();
}

void
Directory_Controller::regStats()
{
    AbstractController::regStats();

    if (m_version == 0) {
        for (Directory_Event event = Directory_Event_FIRST;
             event < Directory_Event_NUM; ++event) {
            Stats::Vector *t = new Stats::Vector();
            t->init(m_num_controllers);
            t->name(g_system_ptr->name() + ".Directory_Controller." +
                Directory_Event_to_string(event));
            t->flags(Stats::pdf | Stats::total | Stats::oneline |
                     Stats::nozero);

            eventVec.push_back(t);
        }

        for (Directory_State state = Directory_State_FIRST;
             state < Directory_State_NUM; ++state) {

            transVec.push_back(std::vector<Stats::Vector *>());

            for (Directory_Event event = Directory_Event_FIRST;
                 event < Directory_Event_NUM; ++event) {

                Stats::Vector *t = new Stats::Vector();
                t->init(m_num_controllers);
                t->name(g_system_ptr->name() + ".Directory_Controller." +
                        Directory_State_to_string(state) +
                        "." + Directory_Event_to_string(event));

                t->flags(Stats::pdf | Stats::total | Stats::oneline |
                         Stats::nozero);
                transVec[state].push_back(t);
            }
        }
    }
}

void
Directory_Controller::collateStats()
{
    for (Directory_Event event = Directory_Event_FIRST;
         event < Directory_Event_NUM; ++event) {
        for (unsigned int i = 0; i < m_num_controllers; ++i) {
            std::map<uint32_t, AbstractController *>::iterator it =
                                g_abs_controls[MachineType_Directory].find(i);
            assert(it != g_abs_controls[MachineType_Directory].end());
            (*eventVec[event])[i] =
                ((Directory_Controller *)(*it).second)->getEventCount(event);
        }
    }

    for (Directory_State state = Directory_State_FIRST;
         state < Directory_State_NUM; ++state) {

        for (Directory_Event event = Directory_Event_FIRST;
             event < Directory_Event_NUM; ++event) {

            for (unsigned int i = 0; i < m_num_controllers; ++i) {
                std::map<uint32_t, AbstractController *>::iterator it =
                                g_abs_controls[MachineType_Directory].find(i);
                assert(it != g_abs_controls[MachineType_Directory].end());
                (*transVec[state][event])[i] =
                    ((Directory_Controller *)(*it).second)->getTransitionCount(state, event);
            }
        }
    }
}

void
Directory_Controller::countTransition(Directory_State state, Directory_Event event)
{
    assert(m_possible[state][event]);
    m_counters[state][event]++;
    m_event_counters[event]++;
}
void
Directory_Controller::possibleTransition(Directory_State state,
                             Directory_Event event)
{
    m_possible[state][event] = true;
}

uint64
Directory_Controller::getEventCount(Directory_Event event)
{
    return m_event_counters[event];
}

bool
Directory_Controller::isPossible(Directory_State state, Directory_Event event)
{
    return m_possible[state][event];
}

uint64
Directory_Controller::getTransitionCount(Directory_State state,
                             Directory_Event event)
{
    return m_counters[state][event];
}

int
Directory_Controller::getNumControllers()
{
    return m_num_controllers;
}

MessageBuffer*
Directory_Controller::getMandatoryQueue() const
{
    return NULL;
}

Sequencer*
Directory_Controller::getSequencer() const
{
    return NULL;
}

void
Directory_Controller::print(ostream& out) const
{
    out << "[Directory_Controller " << m_version << "]";
}

void Directory_Controller::resetStats()
{
    for (int state = 0; state < Directory_State_NUM; state++) {
        for (int event = 0; event < Directory_Event_NUM; event++) {
            m_counters[state][event] = 0;
        }
    }

    for (int event = 0; event < Directory_Event_NUM; event++) {
        m_event_counters[event] = 0;
    }

    AbstractController::resetStats();
}

// Set and Reset for tbe variable
void
Directory_Controller::set_tbe(Directory_TBE*& m_tbe_ptr, Directory_TBE* m_new_tbe)
{
  m_tbe_ptr = m_new_tbe;
}

void
Directory_Controller::unset_tbe(Directory_TBE*& m_tbe_ptr)
{
  m_tbe_ptr = NULL;
}

void
Directory_Controller::recordCacheTrace(int cntrl, CacheRecorder* tr)
{
}

// Actions
/** \brief clear owner */
void
Directory_Controller::c_clearOwner(Directory_TBE*& m_tbe_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing c_clearOwner on address %s\n", addr);
    (((*(getDirectoryEntry(addr))).m_Owner).clear());

}

/** \brief sharer */
void
Directory_Controller::incSharercount(Directory_TBE*& m_tbe_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing incSharercount on address %s\n", addr);
    (*(getDirectoryEntry(addr))).m_Sharerscount = ((*(getDirectoryEntry(addr))).m_Sharerscount + (1));

}

/** \brief sharer */
void
Directory_Controller::clearSharercount(Directory_TBE*& m_tbe_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing clearSharercount on address %s\n", addr);
    (*(getDirectoryEntry(addr))).m_Sharerscount = (0);

}

/** \brief sharer */
void
Directory_Controller::updateSharercount_data(Directory_TBE*& m_tbe_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing updateSharercount_data on address %s\n", addr);
    Directory_TBE* tbetemp
 = (((*m_TBEs_ptr)).lookup(addr));
    if ((tbetemp != NULL)) {
        {
            // Declare message
            const RequestMsg* in_msg_ptr M5_VAR_USED;
            in_msg_ptr = dynamic_cast<const RequestMsg *>(((*m_requestToDir_ptr)).peek());
            assert(in_msg_ptr != NULL); // Check the cast result
            if ((((*m_tbe_ptr).m_Sharers).isElement(((*in_msg_ptr)).m_Requestor))) {
                (((*m_tbe_ptr).m_Sharers).remove(((*in_msg_ptr)).m_Requestor));
                (*(getDirectoryEntry(addr))).m_Sharerscount = ((*(getDirectoryEntry(addr))).m_Sharerscount - (1));
            }
            }
        }

}

/** \brief sharer */
void
Directory_Controller::updateSharercount_memdata(Directory_TBE*& m_tbe_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing updateSharercount_memdata on address %s\n", addr);
    Directory_TBE* tbetemp
 = (((*m_TBEs_ptr)).lookup(addr));
    if ((tbetemp != NULL)) {
        {
            // Declare message
            const MemoryMsg* in_msg_ptr M5_VAR_USED;
            in_msg_ptr = dynamic_cast<const MemoryMsg *>(((*m_responseFromMemory_ptr)).peek());
            assert(in_msg_ptr != NULL); // Check the cast result
            if ((((*m_tbe_ptr).m_Sharers).isElement(((*in_msg_ptr)).m_OriginalRequestorMachId))) {
                (((*m_tbe_ptr).m_Sharers).remove(((*in_msg_ptr)).m_OriginalRequestorMachId));
                (*(getDirectoryEntry(addr))).m_Sharerscount = ((*(getDirectoryEntry(addr))).m_Sharerscount - (1));
            }
            }
        }

}

/** \brief Issue inv when latest */
void
Directory_Controller::issueInvLatest(Directory_TBE*& m_tbe_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing issueInvLatest on address %s\n", addr);
    {
    // Declare message
    const RequestMsg* in_msg_ptr M5_VAR_USED;
    in_msg_ptr = dynamic_cast<const RequestMsg *>(((*m_requestToDir_WB_ptr)).peek());
    assert(in_msg_ptr != NULL); // Check the cast result
{
    std::shared_ptr<RequestMsg> out_msg = std::make_shared<RequestMsg>(clockEdge());
    (*out_msg).m_Addr = addr;
    (*out_msg).m_Type = CoherenceRequestType_ALLINV;
    (*out_msg).m_Requestor = m_machineID;
    (((*out_msg).m_Destination).broadcast(MachineType_L1Cache));
    (*out_msg).m_MessageSize = MessageSizeType_Control;
    (*out_msg).m_DataDestination = ((*in_msg_ptr)).m_DataDestination;
    ((*m_requestFromDir_WB_ptr)).enqueue(out_msg, Cycles(m_l1_request_latency));
}
}

}

/** \brief owner is requestor */
void
Directory_Controller::e_ownerIsRequestor(Directory_TBE*& m_tbe_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing e_ownerIsRequestor on address %s\n", addr);
    {
    // Declare message
    const RequestMsg* in_msg_ptr M5_VAR_USED;
    in_msg_ptr = dynamic_cast<const RequestMsg *>(((*m_requestToDir_ptr)).peek());
    assert(in_msg_ptr != NULL); // Check the cast result
(((*(getDirectoryEntry(addr))).m_Owner).clear());
(((*(getDirectoryEntry(addr))).m_Owner).add(((*in_msg_ptr)).m_Requestor));
}

}

/** \brief owner is requestor */
void
Directory_Controller::e_ownerIsRequestor_ack(Directory_TBE*& m_tbe_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing e_ownerIsRequestor_ack on address %s\n", addr);
    {
    // Declare message
    const ResponseMsg* in_msg_ptr M5_VAR_USED;
    in_msg_ptr = dynamic_cast<const ResponseMsg *>(((*m_responseToDir_ptr)).peek());
    assert(in_msg_ptr != NULL); // Check the cast result
(((*(getDirectoryEntry(addr))).m_Owner).clear());
(((*(getDirectoryEntry(addr))).m_Owner).add(((((*in_msg_ptr)).m_DataDestination).smallestElement())));
}

}

/** \brief owner is requestor */
void
Directory_Controller::e_ownerIsRequestorxx(Directory_TBE*& m_tbe_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing e_ownerIsRequestorxx on address %s\n", addr);
    (((*(getDirectoryEntry(addr))).m_Owner).clear());
(((*(getDirectoryEntry(addr))).m_Owner).add((*m_tbe_ptr).m_L1_GetM_ID));

}

/** \brief owner is requestor wb */
void
Directory_Controller::e_ownerIsRequest(Directory_TBE*& m_tbe_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing e_ownerIsRequest on address %s\n", addr);
    {
    // Declare message
    const RequestMsg* in_msg_ptr M5_VAR_USED;
    in_msg_ptr = dynamic_cast<const RequestMsg *>(((*m_requestToDir_WB_ptr)).peek());
    assert(in_msg_ptr != NULL); // Check the cast result
(((*(getDirectoryEntry(addr))).m_Owner).clear());
(((*(getDirectoryEntry(addr))).m_Owner).add(((*in_msg_ptr)).m_Requestor));
}

}

/** \brief Send ack to L2 */
void
Directory_Controller::a_sendAck(Directory_TBE*& m_tbe_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing a_sendAck on address %s\n", addr);
    {
    // Declare message
    const MemoryMsg* in_msg_ptr M5_VAR_USED;
    in_msg_ptr = dynamic_cast<const MemoryMsg *>(((*m_responseFromMemory_ptr)).peek());
    assert(in_msg_ptr != NULL); // Check the cast result
{
    std::shared_ptr<ResponseMsg> out_msg = std::make_shared<ResponseMsg>(clockEdge());
    (*out_msg).m_Addr = addr;
    (*out_msg).m_Type = CoherenceResponseType_MEMORY_ACK;
    (*out_msg).m_Sender = m_machineID;
    (((*out_msg).m_Destination).add(((*in_msg_ptr)).m_Sender));
    (*out_msg).m_MessageSize = MessageSizeType_Response_Control;
    ((*m_responseFromDir_ptr)).enqueue(out_msg, Cycles(m_to_mem_ctrl_latency));
}
}

}

/** \brief Send data to whoever requested */
void
Directory_Controller::d_sendDataToCores(Directory_TBE*& m_tbe_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing d_sendDataToCores on address %s\n", addr);
    #ifndef NDEBUG
if (!(((((*m_tbe_ptr).m_L1_Get_IDs).count()) > (0)))) {
    panic("Runtime Error at MSI_Snooping_Time_based_base-dir.sm:550: %s.\n", "assert failure");

}
#endif
;
{
    // Declare message
    const MemoryMsg* in_msg_ptr M5_VAR_USED;
    in_msg_ptr = dynamic_cast<const MemoryMsg *>(((*m_responseFromMemory_ptr)).peek());
    assert(in_msg_ptr != NULL); // Check the cast result
{
    std::shared_ptr<ResponseMsg> out_msg = std::make_shared<ResponseMsg>(clockEdge());
    (*out_msg).m_Addr = addr;
    (*out_msg).m_Type = CoherenceResponseType_DATA;
    (*out_msg).m_Sender = m_machineID;
    (*out_msg).m_Destination = (*m_tbe_ptr).m_L1_Get_IDs;
    (*out_msg).m_DataBlk = ((*in_msg_ptr)).m_DataBlk;
    (*out_msg).m_Dirty = (false);
    (*out_msg).m_MessageSize = MessageSizeType_Response_Data;
    ((*m_responseFromDir_ptr)).enqueue(out_msg, Cycles(m_to_mem_ctrl_latency));
}
}

}

/** \brief Send data to whoever requested */
void
Directory_Controller::d_sendDataToCoresRN(Directory_TBE*& m_tbe_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing d_sendDataToCoresRN on address %s\n", addr);
    #ifndef NDEBUG
if (!(((((*m_tbe_ptr).m_L1_Get_IDs).count()) > (0)))) {
    panic("Runtime Error at MSI_Snooping_Time_based_base-dir.sm:567: %s.\n", "assert failure");

}
#endif
;
{
    // Declare message
    const ResponseMsg* in_msg_ptr M5_VAR_USED;
    in_msg_ptr = dynamic_cast<const ResponseMsg *>(((*m_responseToDir_ptr)).peek());
    assert(in_msg_ptr != NULL); // Check the cast result
{
    std::shared_ptr<ResponseMsg> out_msg = std::make_shared<ResponseMsg>(clockEdge());
    (*out_msg).m_Addr = addr;
    (*out_msg).m_Type = CoherenceResponseType_DATA;
    (*out_msg).m_Sender = m_machineID;
    (*out_msg).m_Destination = (*m_tbe_ptr).m_L1_Get_IDs;
    (*out_msg).m_DataBlk = ((*in_msg_ptr)).m_DataBlk;
    (*out_msg).m_Dirty = (false);
    (*out_msg).m_MessageSize = MessageSizeType_Response_Data;
    ((*m_responseFromDir_ptr)).enqueue(out_msg, Cycles(m_to_mem_ctrl_latency));
}
}

}

/** \brief Send data to sharers from responseNetwork */
void
Directory_Controller::d_sendDataToSharersRN(Directory_TBE*& m_tbe_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing d_sendDataToSharersRN on address %s\n", addr);
    #ifndef NDEBUG
if (!((m_tbe_ptr != NULL))) {
    panic("Runtime Error at MSI_Snooping_Time_based_base-dir.sm:585: %s.\n", "assert failure");

}
#endif
;
#ifndef NDEBUG
if (!(((((*m_tbe_ptr).m_L1_GetS_IDs).count()) > (0)))) {
    panic("Runtime Error at MSI_Snooping_Time_based_base-dir.sm:586: %s.\n", "assert failure");

}
#endif
;
{
    // Declare message
    const ResponseMsg* in_msg_ptr M5_VAR_USED;
    in_msg_ptr = dynamic_cast<const ResponseMsg *>(((*m_responseToDir_ptr)).peek());
    assert(in_msg_ptr != NULL); // Check the cast result
{
    std::shared_ptr<ResponseMsg> out_msg = std::make_shared<ResponseMsg>(clockEdge());
    (*out_msg).m_Addr = addr;
    (*out_msg).m_Type = CoherenceResponseType_DATA;
    (*out_msg).m_Sender = m_machineID;
    (*out_msg).m_Destination = (*m_tbe_ptr).m_L1_GetS_IDs;
    (*out_msg).m_DataBlk = ((*in_msg_ptr)).m_DataBlk;
    (*out_msg).m_Dirty = (false);
    (*out_msg).m_MessageSize = MessageSizeType_Response_Data;
    ((*m_responseFromDir_ptr)).enqueue(out_msg, Cycles(m_to_mem_ctrl_latency));
}
}

}

/** \brief Send data to requestor from ResponseNetwork */
void
Directory_Controller::d_sendDataToRequestorRN(Directory_TBE*& m_tbe_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing d_sendDataToRequestorRN on address %s\n", addr);
    {
    // Declare message
    const ResponseMsg* in_msg_ptr M5_VAR_USED;
    in_msg_ptr = dynamic_cast<const ResponseMsg *>(((*m_responseToDir_ptr)).peek());
    assert(in_msg_ptr != NULL); // Check the cast result
{
    std::shared_ptr<ResponseMsg> out_msg = std::make_shared<ResponseMsg>(clockEdge());
    (*out_msg).m_Addr = addr;
    (*out_msg).m_Type = CoherenceResponseType_DATA;
    (*out_msg).m_Sender = m_machineID;
    (((*out_msg).m_Destination).add((*m_tbe_ptr).m_L1_GetM_ID));
    (*out_msg).m_DataBlk = ((*in_msg_ptr)).m_DataBlk;
    (*out_msg).m_MessageSize = MessageSizeType_Response_Data;
    ((*m_responseFromDir_ptr)).enqueue(out_msg, Cycles(m_to_mem_ctrl_latency));
}
}

}

/** \brief Send data to sharers */
void
Directory_Controller::d_sendDataToSharers(Directory_TBE*& m_tbe_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing d_sendDataToSharers on address %s\n", addr);
    #ifndef NDEBUG
if (!((m_tbe_ptr != NULL))) {
    panic("Runtime Error at MSI_Snooping_Time_based_base-dir.sm:616: %s.\n", "assert failure");

}
#endif
;
#ifndef NDEBUG
if (!(((((*m_tbe_ptr).m_L1_GetS_IDs).count()) > (0)))) {
    panic("Runtime Error at MSI_Snooping_Time_based_base-dir.sm:617: %s.\n", "assert failure");

}
#endif
;
{
    // Declare message
    const MemoryMsg* in_msg_ptr M5_VAR_USED;
    in_msg_ptr = dynamic_cast<const MemoryMsg *>(((*m_responseFromMemory_ptr)).peek());
    assert(in_msg_ptr != NULL); // Check the cast result
{
    std::shared_ptr<ResponseMsg> out_msg = std::make_shared<ResponseMsg>(clockEdge());
    (*out_msg).m_Addr = addr;
    (*out_msg).m_Type = CoherenceResponseType_DATA;
    (*out_msg).m_Sender = m_machineID;
    (*out_msg).m_Destination = (*m_tbe_ptr).m_L1_GetS_IDs;
    (*out_msg).m_DataBlk = ((*in_msg_ptr)).m_DataBlk;
    (*out_msg).m_Dirty = (false);
    (*out_msg).m_MessageSize = MessageSizeType_Response_Data;
    ((*m_responseFromDir_ptr)).enqueue(out_msg, Cycles(m_to_mem_ctrl_latency));
}
}

}

/** \brief Send data to requestor */
void
Directory_Controller::d_sendDataToRequestor(Directory_TBE*& m_tbe_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing d_sendDataToRequestor on address %s\n", addr);
    {
    // Declare message
    const MemoryMsg* in_msg_ptr M5_VAR_USED;
    in_msg_ptr = dynamic_cast<const MemoryMsg *>(((*m_responseFromMemory_ptr)).peek());
    assert(in_msg_ptr != NULL); // Check the cast result
{
    std::shared_ptr<ResponseMsg> out_msg = std::make_shared<ResponseMsg>(clockEdge());
    (*out_msg).m_Addr = addr;
    (*out_msg).m_Type = CoherenceResponseType_DATA;
    (*out_msg).m_Sender = m_machineID;
    (((*out_msg).m_Destination).add((*m_tbe_ptr).m_L1_GetM_ID));
    (*out_msg).m_DataBlk = ((*in_msg_ptr)).m_DataBlk;
    (*out_msg).m_MessageSize = MessageSizeType_Response_Data;
    ((*m_responseFromDir_ptr)).enqueue(out_msg, Cycles(m_to_mem_ctrl_latency));
}
}

}

/** \brief Send data ack to bus */
void
Directory_Controller::sendDataAck(Directory_TBE*& m_tbe_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing sendDataAck on address %s\n", addr);
    {
    // Declare message
    const RequestMsg* in_msg_ptr M5_VAR_USED;
    in_msg_ptr = dynamic_cast<const RequestMsg *>(((*m_requestToDir_ptr)).peek());
    assert(in_msg_ptr != NULL); // Check the cast result
{
    std::shared_ptr<ResponseMsg> out_msg = std::make_shared<ResponseMsg>(clockEdge());
    (*out_msg).m_Addr = addr;
    (*out_msg).m_Type = CoherenceResponseType_DATAACK;
    (*out_msg).m_Sender = m_machineID;
    (((*out_msg).m_Destination).add(m_machineID));
    (*out_msg).m_MessageSize = MessageSizeType_Control;
    (((*out_msg).m_DataDestination).add(((*in_msg_ptr)).m_Requestor));
        if ((((*in_msg_ptr)).m_Type == CoherenceRequestType_GETM)) {
            (*out_msg).m_isSpecial = (true);
        } else {
            (*out_msg).m_isSpecial = (false);
        }
        ((*m_responseFromDir_ptr)).enqueue(out_msg, Cycles(m_to_bus_ack_latency));
    }
    }

}

/** \brief Send data to requestor */
void
Directory_Controller::d_sendData(Directory_TBE*& m_tbe_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing d_sendData on address %s\n", addr);
    {
    // Declare message
    const MemoryMsg* in_msg_ptr M5_VAR_USED;
    in_msg_ptr = dynamic_cast<const MemoryMsg *>(((*m_responseFromMemory_ptr)).peek());
    assert(in_msg_ptr != NULL); // Check the cast result
{
    std::shared_ptr<ResponseMsg> out_msg = std::make_shared<ResponseMsg>(clockEdge());
    (*out_msg).m_Addr = addr;
    (*out_msg).m_Type = CoherenceResponseType_DATA;
    (*out_msg).m_Sender = m_machineID;
    (((*out_msg).m_Destination).add(((*in_msg_ptr)).m_OriginalRequestorMachId));
    (*out_msg).m_DataBlk = ((*in_msg_ptr)).m_DataBlk;
    (*out_msg).m_Dirty = (false);
    (*out_msg).m_MessageSize = MessageSizeType_Response_Data;
    (*out_msg).m_SendTime = (((*m_responseFromMemory_ptr)).dequeue());
        if (((((*out_msg).m_Destination).smallestNodeID()) == (0))) {
                if ((inBound((*out_msg).m_SendTime, (0)))) {
                    (*out_msg).m_SendTime = (getCurrentStartSlot((*out_msg).m_SendTime, (0)));
                } else {
                    (*out_msg).m_SendTime = (getNearestSlot((*out_msg).m_SendTime, (0)));
                }
            } else {
                    if (((((*out_msg).m_Destination).smallestNodeID()) == (1))) {
                            if ((inBound((*out_msg).m_SendTime, (1)))) {
                                (*out_msg).m_SendTime = (getCurrentStartSlot((*out_msg).m_SendTime, (1)));
                            } else {
                                (*out_msg).m_SendTime = (getNearestSlot((*out_msg).m_SendTime, (1)));
                            }
                        } else {
                                if (((((*out_msg).m_Destination).smallestNodeID()) == (2))) {
                                        if ((inBound((*out_msg).m_SendTime, (2)))) {
                                            (*out_msg).m_SendTime = (getCurrentStartSlot((*out_msg).m_SendTime, (2)));
                                        } else {
                                            (*out_msg).m_SendTime = (getNearestSlot((*out_msg).m_SendTime, (2)));
                                        }
                                    } else {
                                            if (((((*out_msg).m_Destination).smallestNodeID()) == (3))) {
                                                    if ((inBound((*out_msg).m_SendTime, (3)))) {
                                                        (*out_msg).m_SendTime = (getCurrentStartSlot((*out_msg).m_SendTime, (3)));
                                                    } else {
                                                        (*out_msg).m_SendTime = (getNearestSlot((*out_msg).m_SendTime, (3)));
                                                    }
                                                } else {
                                                        if (((((*out_msg).m_Destination).smallestNodeID()) == (4))) {
                                                                if ((inBound((*out_msg).m_SendTime, (4)))) {
                                                                    (*out_msg).m_SendTime = (getCurrentStartSlot((*out_msg).m_SendTime, (4)));
                                                                } else {
                                                                    (*out_msg).m_SendTime = (getNearestSlot((*out_msg).m_SendTime, (4)));
                                                                }
                                                            } else {
                                                                    if (((((*out_msg).m_Destination).smallestNodeID()) == (5))) {
                                                                            if ((inBound((*out_msg).m_SendTime, (5)))) {
                                                                                (*out_msg).m_SendTime = (getCurrentStartSlot((*out_msg).m_SendTime, (5)));
                                                                            } else {
                                                                                (*out_msg).m_SendTime = (getNearestSlot((*out_msg).m_SendTime, (5)));
                                                                            }
                                                                        } else {
                                                                                if (((((*out_msg).m_Destination).smallestNodeID()) == (6))) {
                                                                                        if ((inBound((*out_msg).m_SendTime, (6)))) {
                                                                                            (*out_msg).m_SendTime = (getCurrentStartSlot((*out_msg).m_SendTime, (6)));
                                                                                        } else {
                                                                                            (*out_msg).m_SendTime = (getNearestSlot((*out_msg).m_SendTime, (6)));
                                                                                        }
                                                                                    } else {
                                                                                            if (((((*out_msg).m_Destination).smallestNodeID()) == (7))) {
                                                                                                    if ((inBound((*out_msg).m_SendTime, (7)))) {
                                                                                                        (*out_msg).m_SendTime = (getCurrentStartSlot((*out_msg).m_SendTime, (7)));
                                                                                                    } else {
                                                                                                        (*out_msg).m_SendTime = (getNearestSlot((*out_msg).m_SendTime, (7)));
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    ((*m_responseFromDir_ptr)).enqueue(out_msg, Cycles(m_to_mem_ctrl_latency));
                                                                }
                                                                }

}

/** \brief Send ack to L2 */
void
Directory_Controller::aa_sendAck(Directory_TBE*& m_tbe_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing aa_sendAck on address %s\n", addr);
    {
    // Declare message
    const MemoryMsg* in_msg_ptr M5_VAR_USED;
    in_msg_ptr = dynamic_cast<const MemoryMsg *>(((*m_responseFromMemory_ptr)).peek());
    assert(in_msg_ptr != NULL); // Check the cast result
{
    std::shared_ptr<ResponseMsg> out_msg = std::make_shared<ResponseMsg>(clockEdge());
    (*out_msg).m_Addr = addr;
    (*out_msg).m_Type = CoherenceResponseType_MEMORY_ACK;
    (*out_msg).m_Sender = m_machineID;
    (((*out_msg).m_Destination).add(((*in_msg_ptr)).m_OriginalRequestorMachId));
    (*out_msg).m_MessageSize = MessageSizeType_Response_Control;
    ((*m_responseFromDir_ptr)).enqueue(out_msg, Cycles(m_to_mem_ctrl_latency));
}
}

}

/** \brief allocate TBE */
void
Directory_Controller::i_allocateTBE(Directory_TBE*& m_tbe_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing i_allocateTBE on address %s\n", addr);
    Directory_TBE* tbetemp
 = (((*m_TBEs_ptr)).lookup(addr));
    if ((tbetemp != NULL)) {
    } else {
        (((*m_TBEs_ptr)).allocate(addr));
        set_tbe(m_tbe_ptr, (((*m_TBEs_ptr)).lookup(addr)));;
    }

}

/** \brief deallocate TBE */
void
Directory_Controller::w_deallocateTBE(Directory_TBE*& m_tbe_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing w_deallocateTBE on address %s\n", addr);
    Directory_TBE* tbetemp
 = (((*m_TBEs_ptr)).lookup(addr));
    if ((tbetemp != NULL)) {
        (*(getDirectoryEntry(addr))).m_Sharerscount = ((*(getDirectoryEntry(addr))).m_Sharerscount - (((*(((*m_TBEs_ptr)).lookup(addr))).m_Sharers).count()));
        (((*m_TBEs_ptr)).deallocate(addr));
        unset_tbe(m_tbe_ptr);;
    }

}

/** \brief Pop incoming request queue */
void
Directory_Controller::j_popIncomingRequestQueue(Directory_TBE*& m_tbe_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing j_popIncomingRequestQueue on address %s\n", addr);
    (((*m_requestToDir_ptr)).dequeue());

}

/** \brief Pop incoming WB request queue */
void
Directory_Controller::j_popIncomingRequestWBQueue(Directory_TBE*& m_tbe_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing j_popIncomingRequestWBQueue on address %s\n", addr);
    (((*m_requestToDir_WB_ptr)).dequeue());

}

/** \brief Pop incoming request queue */
void
Directory_Controller::k_popIncomingResponseQueue(Directory_TBE*& m_tbe_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing k_popIncomingResponseQueue on address %s\n", addr);
    (((*m_responseToDir_ptr)).dequeue());

}

/** \brief Pop off-chip request queue */
void
Directory_Controller::l_popMemQueue(Directory_TBE*& m_tbe_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing l_popMemQueue on address %s\n", addr);
    (((*m_responseFromMemory_ptr)).dequeue());

}

/** \brief wake-up dependents */
void
Directory_Controller::kd_wakeUpDependents(Directory_TBE*& m_tbe_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing kd_wakeUpDependents on address %s\n", addr);
    (wakeUpAllBuffers(addr));

}

/** \brief Queue off-chip fetch request */
void
Directory_Controller::qf_queueMemoryFetchRequest(Directory_TBE*& m_tbe_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing qf_queueMemoryFetchRequest on address %s\n", addr);
    {
    // Declare message
    const RequestMsg* in_msg_ptr M5_VAR_USED;
    in_msg_ptr = dynamic_cast<const RequestMsg *>(((*m_requestToDir_ptr)).peek());
    assert(in_msg_ptr != NULL); // Check the cast result
(queueMemoryRead(((*in_msg_ptr)).m_Requestor, addr, m_to_mem_ctrl_latency));
}

}

/** \brief Queue off-chip fetch request */
void
Directory_Controller::qf_queueMemoryFetchRequest_ack(Directory_TBE*& m_tbe_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing qf_queueMemoryFetchRequest_ack on address %s\n", addr);
    {
    // Declare message
    const ResponseMsg* in_msg_ptr M5_VAR_USED;
    in_msg_ptr = dynamic_cast<const ResponseMsg *>(((*m_responseToDir_ptr)).peek());
    assert(in_msg_ptr != NULL); // Check the cast result
(queueMemoryRead(((((*in_msg_ptr)).m_DataDestination).smallestElement()), addr, m_to_mem_ctrl_latency));
}

}

/** \brief Queue off-chip fetch request */
void
Directory_Controller::qf_queueDirectMemoryFetchRequest(Directory_TBE*& m_tbe_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing qf_queueDirectMemoryFetchRequest on address %s\n", addr);
    (queueMemoryRead((*m_tbe_ptr).m_L1_GetM_ID, addr, m_to_mem_ctrl_latency));

}

/** \brief Queue off-chip fetch WB request */
void
Directory_Controller::qf_queueMemoryFetchRequestWB(Directory_TBE*& m_tbe_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing qf_queueMemoryFetchRequestWB on address %s\n", addr);
    {
    // Declare message
    const RequestMsg* in_msg_ptr M5_VAR_USED;
    in_msg_ptr = dynamic_cast<const RequestMsg *>(((*m_requestToDir_WB_ptr)).peek());
    assert(in_msg_ptr != NULL); // Check the cast result
(queueMemoryRead(((*in_msg_ptr)).m_Requestor, addr, m_to_mem_ctrl_latency));
}

}

/** \brief Queue off-chip writeback request */
void
Directory_Controller::qw_queueMemoryWBRequest(Directory_TBE*& m_tbe_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing qw_queueMemoryWBRequest on address %s\n", addr);
    {
    // Declare message
    const ResponseMsg* in_msg_ptr M5_VAR_USED;
    in_msg_ptr = dynamic_cast<const ResponseMsg *>(((*m_responseToDir_ptr)).peek());
    assert(in_msg_ptr != NULL); // Check the cast result
(queueMemoryWrite(((*in_msg_ptr)).m_Sender, addr, m_to_mem_ctrl_latency, ((*in_msg_ptr)).m_DataBlk));
}

}

/** \brief Queue off-chip fetch request */
void
Directory_Controller::qf_queueMemoryFetchRequestDMA(Directory_TBE*& m_tbe_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing qf_queueMemoryFetchRequestDMA on address %s\n", addr);
    {
    // Declare message
    const RequestMsg* in_msg_ptr M5_VAR_USED;
    in_msg_ptr = dynamic_cast<const RequestMsg *>(((*m_requestToDir_ptr)).peek());
    assert(in_msg_ptr != NULL); // Check the cast result
(queueMemoryRead(((*in_msg_ptr)).m_Requestor, addr, m_to_mem_ctrl_latency));
}

}

/** \brief Queue off-chip fetch request WB */
void
Directory_Controller::qf_queueMemoryFetchRequestWBDMA(Directory_TBE*& m_tbe_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing qf_queueMemoryFetchRequestWBDMA on address %s\n", addr);
    {
    // Declare message
    const RequestMsg* in_msg_ptr M5_VAR_USED;
    in_msg_ptr = dynamic_cast<const RequestMsg *>(((*m_requestToDir_WB_ptr)).peek());
    assert(in_msg_ptr != NULL); // Check the cast result
(queueMemoryRead(((*in_msg_ptr)).m_Requestor, addr, m_to_mem_ctrl_latency));
}

}

/** \brief Pop incoming DMA queue */
void
Directory_Controller::p_popIncomingDMARequestQueue(Directory_TBE*& m_tbe_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing p_popIncomingDMARequestQueue on address %s\n", addr);
    (((*m_requestToDir_ptr)).dequeue());

}

/** \brief Send Data to DMA controller from directory */
void
Directory_Controller::dr_sendDMAData(Directory_TBE*& m_tbe_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing dr_sendDMAData on address %s\n", addr);
    {
    // Declare message
    const MemoryMsg* in_msg_ptr M5_VAR_USED;
    in_msg_ptr = dynamic_cast<const MemoryMsg *>(((*m_responseFromMemory_ptr)).peek());
    assert(in_msg_ptr != NULL); // Check the cast result
{
    std::shared_ptr<ResponseMsg> out_msg = std::make_shared<ResponseMsg>(clockEdge());
    (*out_msg).m_Addr = addr;
    (*out_msg).m_Type = CoherenceResponseType_DATA;
    (*out_msg).m_DataBlk = ((*in_msg_ptr)).m_DataBlk;
    (((*out_msg).m_Destination).add((map_Address_to_DMA(addr))));
    (*out_msg).m_MessageSize = MessageSizeType_Response_Data;
    ((*m_responseFromDir_ptr)).enqueue(out_msg, Cycles(m_to_mem_ctrl_latency));
}
}

}

/** \brief Queue off-chip writeback request */
void
Directory_Controller::qw_queueMemoryWBRequest_partial(Directory_TBE*& m_tbe_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing qw_queueMemoryWBRequest_partial on address %s\n", addr);
    {
    // Declare message
    const RequestMsg* in_msg_ptr M5_VAR_USED;
    in_msg_ptr = dynamic_cast<const RequestMsg *>(((*m_requestToDir_ptr)).peek());
    assert(in_msg_ptr != NULL); // Check the cast result
(queueMemoryWritePartial(m_machineID, addr, m_to_mem_ctrl_latency, ((*in_msg_ptr)).m_DataBlk, ((*in_msg_ptr)).m_Len));
}

}

/** \brief queue off-chip wb request wb */
void
Directory_Controller::qw_queueMemoryWBRequest_partialWB(Directory_TBE*& m_tbe_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing qw_queueMemoryWBRequest_partialWB on address %s\n", addr);
    {
    // Declare message
    const RequestMsg* in_msg_ptr M5_VAR_USED;
    in_msg_ptr = dynamic_cast<const RequestMsg *>(((*m_requestToDir_WB_ptr)).peek());
    assert(in_msg_ptr != NULL); // Check the cast result
(queueMemoryWritePartial(m_machineID, addr, m_to_mem_ctrl_latency, ((*in_msg_ptr)).m_DataBlk, ((*in_msg_ptr)).m_Len));
}

}

/** \brief Send Ack to DMA controller */
void
Directory_Controller::da_sendDMAAck(Directory_TBE*& m_tbe_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing da_sendDMAAck on address %s\n", addr);
    {
    std::shared_ptr<ResponseMsg> out_msg = std::make_shared<ResponseMsg>(clockEdge());
    (*out_msg).m_Addr = addr;
    (*out_msg).m_Type = CoherenceResponseType_ACK;
    (((*out_msg).m_Destination).add((map_Address_to_DMA(addr))));
    (*out_msg).m_MessageSize = MessageSizeType_Writeback_Control;
    ((*m_responseFromDir_ptr)).enqueue(out_msg, Cycles(m_to_mem_ctrl_latency));
}

}

/** \brief stall */
void
Directory_Controller::z_stall(Directory_TBE*& m_tbe_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing z_stall on address %s\n", addr);
    
}

/** \brief recycle request queue */
void
Directory_Controller::z_stallAndWaitRequest(Directory_TBE*& m_tbe_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing z_stallAndWaitRequest on address %s\n", addr);
            stallBuffer(&((*m_requestToDir_ptr)), addr);
        (*m_requestToDir_ptr).stallMessage(addr);
        

}

/** \brief recycle response data WB queue */
void
Directory_Controller::z_stallAndWaitResponse(Directory_TBE*& m_tbe_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing z_stallAndWaitResponse on address %s\n", addr);
            stallBuffer(&((*m_responseToDir_ptr)), addr);
        (*m_responseToDir_ptr).stallMessage(addr);
        

}

/** \brief recycle request queue wb */
void
Directory_Controller::z_stallAndWaitRequestWB(Directory_TBE*& m_tbe_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing z_stallAndWaitRequestWB on address %s\n", addr);
            stallBuffer(&((*m_requestToDir_WB_ptr)), addr);
        (*m_requestToDir_WB_ptr).stallMessage(addr);
        

}

/** \brief recycle DMA queue */
void
Directory_Controller::zz_recycleDMAQueue(Directory_TBE*& m_tbe_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing zz_recycleDMAQueue on address %s\n", addr);
    (((*m_requestToDir_ptr)).recycle());

}

/** \brief recycle DMA WB queue */
void
Directory_Controller::zz_recycleDMAWBQueue(Directory_TBE*& m_tbe_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing zz_recycleDMAWBQueue on address %s\n", addr);
    (((*m_requestToDir_WB_ptr)).recycle());

}

/** \brief Record L1 gets */
void
Directory_Controller::ss_recordGetSL1ID(Directory_TBE*& m_tbe_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing ss_recordGetSL1ID on address %s\n", addr);
    {
    // Declare message
    const RequestMsg* in_msg_ptr M5_VAR_USED;
    in_msg_ptr = dynamic_cast<const RequestMsg *>(((*m_requestToDir_ptr)).peek());
    assert(in_msg_ptr != NULL); // Check the cast result
(((*m_tbe_ptr).m_L1_GetS_IDs).add(((*in_msg_ptr)).m_Requestor));
(((*m_tbe_ptr).m_L1_Get_IDs).add(((*in_msg_ptr)).m_Requestor));
}

}

/** \brief Send Data to DMA controller from incoming PUTX */
void
Directory_Controller::drp_sendDMAData(Directory_TBE*& m_tbe_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing drp_sendDMAData on address %s\n", addr);
    {
    // Declare message
    const ResponseMsg* in_msg_ptr M5_VAR_USED;
    in_msg_ptr = dynamic_cast<const ResponseMsg *>(((*m_responseToDir_ptr)).peek());
    assert(in_msg_ptr != NULL); // Check the cast result
{
    std::shared_ptr<ResponseMsg> out_msg = std::make_shared<ResponseMsg>(clockEdge());
    (*out_msg).m_Addr = addr;
    (*out_msg).m_Type = CoherenceResponseType_DATA;
    (*out_msg).m_DataBlk = ((*in_msg_ptr)).m_DataBlk;
    (((*out_msg).m_Destination).add((map_Address_to_DMA(addr))));
    (*out_msg).m_MessageSize = MessageSizeType_Response_Data;
    ((*m_responseFromDir_ptr)).enqueue(out_msg, Cycles(m_to_mem_ctrl_latency));
}
}

}

/** \brief Allocate TBE */
void
Directory_Controller::v_allocateTBE(Directory_TBE*& m_tbe_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing v_allocateTBE on address %s\n", addr);
    {
    // Declare message
    const RequestMsg* in_msg_ptr M5_VAR_USED;
    in_msg_ptr = dynamic_cast<const RequestMsg *>(((*m_requestToDir_ptr)).peek());
    assert(in_msg_ptr != NULL); // Check the cast result
(((*m_TBEs_ptr)).allocate(addr));
set_tbe(m_tbe_ptr, (((*m_TBEs_ptr)).lookup(addr)));;
(*m_tbe_ptr).m_DataBlk = ((*in_msg_ptr)).m_DataBlk;
(*m_tbe_ptr).m_PhysicalAddress = ((*in_msg_ptr)).m_Addr;
(*m_tbe_ptr).m_Len = ((*in_msg_ptr)).m_Len;
}

}

/** \brief Allocate TBE WB */
void
Directory_Controller::v_allocateTBEWB(Directory_TBE*& m_tbe_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing v_allocateTBEWB on address %s\n", addr);
    {
    // Declare message
    const RequestMsg* in_msg_ptr M5_VAR_USED;
    in_msg_ptr = dynamic_cast<const RequestMsg *>(((*m_requestToDir_WB_ptr)).peek());
    assert(in_msg_ptr != NULL); // Check the cast result
(((*m_TBEs_ptr)).allocate(addr));
set_tbe(m_tbe_ptr, (((*m_TBEs_ptr)).lookup(addr)));;
(*m_tbe_ptr).m_DataBlk = ((*in_msg_ptr)).m_DataBlk;
(*m_tbe_ptr).m_PhysicalAddress = ((*in_msg_ptr)).m_Addr;
(*m_tbe_ptr).m_Len = ((*in_msg_ptr)).m_Len;
}

}

/** \brief Queue off-chip writeback request */
void
Directory_Controller::qw_queueMemoryWBRequest_partialTBE(Directory_TBE*& m_tbe_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing qw_queueMemoryWBRequest_partialTBE on address %s\n", addr);
    {
    // Declare message
    const ResponseMsg* in_msg_ptr M5_VAR_USED;
    in_msg_ptr = dynamic_cast<const ResponseMsg *>(((*m_responseToDir_ptr)).peek());
    assert(in_msg_ptr != NULL); // Check the cast result
(queueMemoryWritePartial(((*in_msg_ptr)).m_Sender, (*m_tbe_ptr).m_PhysicalAddress, m_to_mem_ctrl_latency, (*m_tbe_ptr).m_DataBlk, (*m_tbe_ptr).m_Len));
}

}

Directory_Entry*
Directory_Controller::getDirectoryEntry(const Address& param_addr)
{
Directory_Entry* dir_entry
 = static_cast<Directory_Entry *>((((*m_directory_ptr)).lookup(param_addr)))
;
    if ((dir_entry != NULL)) {
        return dir_entry;
    }
    dir_entry = static_cast<Directory_Entry *>((((*m_directory_ptr)).allocate(param_addr, new Directory_Entry)))
    ;
    return dir_entry;

}
Directory_State
Directory_Controller::getState(Directory_TBE* param_tbe, const Address& param_addr)
{
    if ((param_tbe != NULL)) {
        return (*param_tbe).m_TBEState;
    } else {
            if ((((*m_directory_ptr)).isPresent(param_addr))) {
                return (*(getDirectoryEntry(param_addr))).m_DirectoryState;
            } else {
                return Directory_State_I;
            }
        }

}
void
Directory_Controller::setState(Directory_TBE* param_tbe, const Address& param_addr, const Directory_State& param_state)
{
    if ((param_tbe != NULL)) {
        (*param_tbe).m_TBEState = param_state;
    }
        if ((((*m_directory_ptr)).isPresent(param_addr))) {
            (*(getDirectoryEntry(param_addr))).m_DirectoryState = param_state;
        }

}
AccessPermission
Directory_Controller::getAccessPermission(const Address& param_addr)
{
Directory_TBE* tbe
 = (((*m_TBEs_ptr)).lookup(param_addr));
    if ((tbe != NULL)) {
        return (Directory_State_to_permission((*tbe).m_TBEState));
    }
        if ((((*m_directory_ptr)).isPresent(param_addr))) {
            return (Directory_State_to_permission((*(getDirectoryEntry(param_addr))).m_DirectoryState));
        }
        return AccessPermission_NotPresent;

}
void
Directory_Controller::functionalRead(const Address& param_addr, Packet* param_pkt)
{
Directory_TBE* tbe
 = (((*m_TBEs_ptr)).lookup(param_addr));
    if ((tbe != NULL)) {
        (testAndRead(param_addr, (*tbe).m_DataBlk, param_pkt));
    } else {
        (functionalMemoryRead(param_pkt));
    }

}
int
Directory_Controller::functionalWrite(const Address& param_addr, Packet* param_pkt)
{
int num_functional_writes
 = (0);
Directory_TBE* tbe
 = (((*m_TBEs_ptr)).lookup(param_addr));
    if ((tbe != NULL)) {
        num_functional_writes = (num_functional_writes + (testAndWrite(param_addr, (*tbe).m_DataBlk, param_pkt)));
    }
    num_functional_writes = (num_functional_writes + (functionalMemoryWrite(param_pkt)));
    return num_functional_writes;

}
void
Directory_Controller::setAccessPermission(const Address& param_addr, const Directory_State& param_state)
{
    if ((((*m_directory_ptr)).isPresent(param_addr))) {
        ((*((getDirectoryEntry(param_addr)))).changePermission((Directory_State_to_permission(param_state))));
    }

}
int
Directory_Controller::functionalWriteBuffers(PacketPtr& pkt)
{
    int num_functional_writes = 0;
num_functional_writes += m_requestToDir_ptr->functionalWrite(pkt);
num_functional_writes += m_requestToDir_WB_ptr->functionalWrite(pkt);
num_functional_writes += m_responseToDir_ptr->functionalWrite(pkt);
num_functional_writes += m_responseFromDir_ptr->functionalWrite(pkt);
num_functional_writes += m_requestFromDir_WB_ptr->functionalWrite(pkt);
    return num_functional_writes;
}
