/** \file Directory_Controller.hh
 *
 * Auto generated C++ code started by /home/mohamed/hourglass/gem5-stable/src/mem/slicc/symbols/StateMachine.py:257
 * Created by slicc definition of Module "MSI Two Level directory protocol"
 */

#ifndef __Directory_CONTROLLER_HH__
#define __Directory_CONTROLLER_HH__

#include <iostream>
#include <sstream>
#include <string>

#include "mem/protocol/TransitionResult.hh"
#include "mem/protocol/Types.hh"
#include "mem/ruby/common/Consumer.hh"
#include "mem/ruby/common/Global.hh"
#include "mem/ruby/slicc_interface/AbstractController.hh"
#include "params/Directory_Controller.hh"
#include "mem/protocol/TBETable.hh"
extern std::stringstream Directory_transitionComment;

class Directory_Controller : public AbstractController
{
  public:
    typedef Directory_ControllerParams Params;
    Directory_Controller(const Params *p);
    static int getNumControllers();
    void init();

    MessageBuffer* getMandatoryQueue() const;

    void print(std::ostream& out) const;
    void wakeup();
    void resetStats();
    void regStats();
    void collateStats();
    void setNetQueue(const std::string& name, MessageBuffer *b);

    void recordCacheTrace(int cntrl, CacheRecorder* tr);
    Sequencer* getSequencer() const;

    int functionalWriteBuffers(PacketPtr&);

    void countTransition(Directory_State state, Directory_Event event);
    void possibleTransition(Directory_State state, Directory_Event event);
    uint64 getEventCount(Directory_Event event);
    bool isPossible(Directory_State state, Directory_Event event);
    uint64 getTransitionCount(Directory_State state, Directory_Event event);

private:
    DirectoryMemory* m_directory_ptr;
    Cycles m_to_mem_ctrl_latency;
    Cycles m_to_bus_ack_latency;
    Cycles m_directory_latency;
    Cycles m_l1_request_latency;
    MessageBuffer* m_requestToDir_ptr;
    MessageBuffer* m_requestToDir_WB_ptr;
    MessageBuffer* m_responseToDir_ptr;
    MessageBuffer* m_responseFromDir_ptr;
    MessageBuffer* m_requestFromDir_WB_ptr;
    TransitionResult doTransition(Directory_Event event,
                                  Directory_TBE* m_tbe_ptr,
                                  const Address addr);

    TransitionResult doTransitionWorker(Directory_Event event,
                                        Directory_State state,
                                        Directory_State& next_state,
                                        Directory_TBE*& m_tbe_ptr,
                                        const Address& addr);

    int m_counters[Directory_State_NUM][Directory_Event_NUM];
    int m_event_counters[Directory_Event_NUM];
    bool m_possible[Directory_State_NUM][Directory_Event_NUM];

    static std::vector<Stats::Vector *> eventVec;
    static std::vector<std::vector<Stats::Vector *> > transVec;
    static int m_num_controllers;
    // Internal functions
    Directory_Entry* getDirectoryEntry(const Address& param_addr);
    Directory_State getState(Directory_TBE* param_tbe, const Address& param_addr);
    void setState(Directory_TBE* param_tbe, const Address& param_addr, const Directory_State& param_state);
    AccessPermission getAccessPermission(const Address& param_addr);
    void functionalRead(const Address& param_addr, Packet* param_pkt);
    int functionalWrite(const Address& param_addr, Packet* param_pkt);
    void setAccessPermission(const Address& param_addr, const Directory_State& param_state);

    // Set and Reset for tbe variable
    void set_tbe(Directory_TBE*& m_tbe_ptr, Directory_TBE* m_new_tbe);
    void unset_tbe(Directory_TBE*& m_tbe_ptr);

    // Actions
    /** \brief clear owner */
    void c_clearOwner(Directory_TBE*& m_tbe_ptr, const Address& addr);
    /** \brief sharer */
    void incSharercount(Directory_TBE*& m_tbe_ptr, const Address& addr);
    /** \brief sharer */
    void clearSharercount(Directory_TBE*& m_tbe_ptr, const Address& addr);
    /** \brief sharer */
    void updateSharercount_data(Directory_TBE*& m_tbe_ptr, const Address& addr);
    /** \brief sharer */
    void updateSharercount_memdata(Directory_TBE*& m_tbe_ptr, const Address& addr);
    /** \brief Issue inv when latest */
    void issueInvLatest(Directory_TBE*& m_tbe_ptr, const Address& addr);
    /** \brief owner is requestor */
    void e_ownerIsRequestor(Directory_TBE*& m_tbe_ptr, const Address& addr);
    /** \brief owner is requestor */
    void e_ownerIsRequestor_ack(Directory_TBE*& m_tbe_ptr, const Address& addr);
    /** \brief owner is requestor */
    void e_ownerIsRequestorxx(Directory_TBE*& m_tbe_ptr, const Address& addr);
    /** \brief owner is requestor wb */
    void e_ownerIsRequest(Directory_TBE*& m_tbe_ptr, const Address& addr);
    /** \brief Send ack to L2 */
    void a_sendAck(Directory_TBE*& m_tbe_ptr, const Address& addr);
    /** \brief Send data to whoever requested */
    void d_sendDataToCores(Directory_TBE*& m_tbe_ptr, const Address& addr);
    /** \brief Send data to whoever requested */
    void d_sendDataToCoresRN(Directory_TBE*& m_tbe_ptr, const Address& addr);
    /** \brief Send data to sharers from responseNetwork */
    void d_sendDataToSharersRN(Directory_TBE*& m_tbe_ptr, const Address& addr);
    /** \brief Send data to requestor from ResponseNetwork */
    void d_sendDataToRequestorRN(Directory_TBE*& m_tbe_ptr, const Address& addr);
    /** \brief Send data to sharers */
    void d_sendDataToSharers(Directory_TBE*& m_tbe_ptr, const Address& addr);
    /** \brief Send data to requestor */
    void d_sendDataToRequestor(Directory_TBE*& m_tbe_ptr, const Address& addr);
    /** \brief Send data ack to bus */
    void sendDataAck(Directory_TBE*& m_tbe_ptr, const Address& addr);
    /** \brief Send data to requestor */
    void d_sendData(Directory_TBE*& m_tbe_ptr, const Address& addr);
    /** \brief Send ack to L2 */
    void aa_sendAck(Directory_TBE*& m_tbe_ptr, const Address& addr);
    /** \brief allocate TBE */
    void i_allocateTBE(Directory_TBE*& m_tbe_ptr, const Address& addr);
    /** \brief deallocate TBE */
    void w_deallocateTBE(Directory_TBE*& m_tbe_ptr, const Address& addr);
    /** \brief Pop incoming request queue */
    void j_popIncomingRequestQueue(Directory_TBE*& m_tbe_ptr, const Address& addr);
    /** \brief Pop incoming WB request queue */
    void j_popIncomingRequestWBQueue(Directory_TBE*& m_tbe_ptr, const Address& addr);
    /** \brief Pop incoming request queue */
    void k_popIncomingResponseQueue(Directory_TBE*& m_tbe_ptr, const Address& addr);
    /** \brief Pop off-chip request queue */
    void l_popMemQueue(Directory_TBE*& m_tbe_ptr, const Address& addr);
    /** \brief wake-up dependents */
    void kd_wakeUpDependents(Directory_TBE*& m_tbe_ptr, const Address& addr);
    /** \brief Queue off-chip fetch request */
    void qf_queueMemoryFetchRequest(Directory_TBE*& m_tbe_ptr, const Address& addr);
    /** \brief Queue off-chip fetch request */
    void qf_queueMemoryFetchRequest_ack(Directory_TBE*& m_tbe_ptr, const Address& addr);
    /** \brief Queue off-chip fetch request */
    void qf_queueDirectMemoryFetchRequest(Directory_TBE*& m_tbe_ptr, const Address& addr);
    /** \brief Queue off-chip fetch WB request */
    void qf_queueMemoryFetchRequestWB(Directory_TBE*& m_tbe_ptr, const Address& addr);
    /** \brief Queue off-chip writeback request */
    void qw_queueMemoryWBRequest(Directory_TBE*& m_tbe_ptr, const Address& addr);
    /** \brief Queue off-chip fetch request */
    void qf_queueMemoryFetchRequestDMA(Directory_TBE*& m_tbe_ptr, const Address& addr);
    /** \brief Queue off-chip fetch request WB */
    void qf_queueMemoryFetchRequestWBDMA(Directory_TBE*& m_tbe_ptr, const Address& addr);
    /** \brief Pop incoming DMA queue */
    void p_popIncomingDMARequestQueue(Directory_TBE*& m_tbe_ptr, const Address& addr);
    /** \brief Send Data to DMA controller from directory */
    void dr_sendDMAData(Directory_TBE*& m_tbe_ptr, const Address& addr);
    /** \brief Queue off-chip writeback request */
    void qw_queueMemoryWBRequest_partial(Directory_TBE*& m_tbe_ptr, const Address& addr);
    /** \brief queue off-chip wb request wb */
    void qw_queueMemoryWBRequest_partialWB(Directory_TBE*& m_tbe_ptr, const Address& addr);
    /** \brief Send Ack to DMA controller */
    void da_sendDMAAck(Directory_TBE*& m_tbe_ptr, const Address& addr);
    /** \brief stall */
    void z_stall(Directory_TBE*& m_tbe_ptr, const Address& addr);
    /** \brief recycle request queue */
    void z_stallAndWaitRequest(Directory_TBE*& m_tbe_ptr, const Address& addr);
    /** \brief recycle response data WB queue */
    void z_stallAndWaitResponse(Directory_TBE*& m_tbe_ptr, const Address& addr);
    /** \brief recycle request queue wb */
    void z_stallAndWaitRequestWB(Directory_TBE*& m_tbe_ptr, const Address& addr);
    /** \brief recycle DMA queue */
    void zz_recycleDMAQueue(Directory_TBE*& m_tbe_ptr, const Address& addr);
    /** \brief recycle DMA WB queue */
    void zz_recycleDMAWBQueue(Directory_TBE*& m_tbe_ptr, const Address& addr);
    /** \brief Record L1 gets */
    void ss_recordGetSL1ID(Directory_TBE*& m_tbe_ptr, const Address& addr);
    /** \brief Send Data to DMA controller from incoming PUTX */
    void drp_sendDMAData(Directory_TBE*& m_tbe_ptr, const Address& addr);
    /** \brief Allocate TBE */
    void v_allocateTBE(Directory_TBE*& m_tbe_ptr, const Address& addr);
    /** \brief Allocate TBE WB */
    void v_allocateTBEWB(Directory_TBE*& m_tbe_ptr, const Address& addr);
    /** \brief Queue off-chip writeback request */
    void qw_queueMemoryWBRequest_partialTBE(Directory_TBE*& m_tbe_ptr, const Address& addr);

    // Objects
    TBETable<Directory_TBE>* m_TBEs_ptr;
};
#endif // __Directory_CONTROLLER_H__
