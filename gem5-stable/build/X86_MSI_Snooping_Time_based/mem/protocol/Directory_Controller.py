from m5.params import *
from m5.SimObject import SimObject
from Controller import RubyController

class Directory_Controller(RubyController):
    type = 'Directory_Controller'
    cxx_header = 'mem/protocol/Directory_Controller.hh'
    directory = Param.RubyDirectoryMemory("")
    to_mem_ctrl_latency = Param.Cycles((3), "")
    to_bus_ack_latency = Param.Cycles((2), "")
    directory_latency = Param.Cycles((6), "")
    l1_request_latency = Param.Cycles((5), "")
    requestToDir = SlavePort("")
    requestToDir_WB = SlavePort("")
    responseToDir = SlavePort("")
    responseFromDir = MasterPort("")
    requestFromDir_WB = MasterPort("")
