/** \file Directory_Entry.hh
 *
 *
 * Auto generated C++ code started by /home/mohamed/hourglass/gem5-stable/src/mem/slicc/symbols/Type.py:200
 */

#ifndef __Directory_Entry_HH__
#define __Directory_Entry_HH__

#include <iostream>

#include "mem/ruby/slicc_interface/RubySlicc_Util.hh"
#include "mem/protocol/Directory_State.hh"
#include "mem/protocol/NetDest.hh"
#include "mem/protocol/AbstractEntry.hh"
class Directory_Entry :  public AbstractEntry
{
  public:
    Directory_Entry
()
		{
        m_DirectoryState = Directory_State_I; // default value of Directory_State
        // m_Owner has no default
        m_Sharerscount = 0; // default value of int
    }
    Directory_Entry(const Directory_Entry&other)
        : AbstractEntry(other)
    {
        m_DirectoryState = other.m_DirectoryState;
        m_Owner = other.m_Owner;
        m_Sharerscount = other.m_Sharerscount;
    }
    Directory_Entry(const Directory_State& local_DirectoryState, const NetDest& local_Owner, const int& local_Sharerscount)
        : AbstractEntry()
    {
        m_DirectoryState = local_DirectoryState;
        m_Owner = local_Owner;
        m_Sharerscount = local_Sharerscount;
    }
    Directory_Entry*
    clone() const
    {
         return new Directory_Entry(*this);
    }
    // Const accessors methods for each field
    /** \brief Const accessor method for DirectoryState field.
     *  \return DirectoryState field
     */
    const Directory_State&
    getDirectoryState() const
    {
        return m_DirectoryState;
    }
    /** \brief Const accessor method for Owner field.
     *  \return Owner field
     */
    const NetDest&
    getOwner() const
    {
        return m_Owner;
    }
    /** \brief Const accessor method for Sharerscount field.
     *  \return Sharerscount field
     */
    const int&
    getSharerscount() const
    {
        return m_Sharerscount;
    }
    // Non const Accessors methods for each field
    /** \brief Non-const accessor method for DirectoryState field.
     *  \return DirectoryState field
     */
    Directory_State&
    getDirectoryState()
    {
        return m_DirectoryState;
    }
    /** \brief Non-const accessor method for Owner field.
     *  \return Owner field
     */
    NetDest&
    getOwner()
    {
        return m_Owner;
    }
    /** \brief Non-const accessor method for Sharerscount field.
     *  \return Sharerscount field
     */
    int&
    getSharerscount()
    {
        return m_Sharerscount;
    }
    // Mutator methods for each field
    /** \brief Mutator method for DirectoryState field */
    void
    setDirectoryState(const Directory_State& local_DirectoryState)
    {
        m_DirectoryState = local_DirectoryState;
    }
    /** \brief Mutator method for Owner field */
    void
    setOwner(const NetDest& local_Owner)
    {
        m_Owner = local_Owner;
    }
    /** \brief Mutator method for Sharerscount field */
    void
    setSharerscount(const int& local_Sharerscount)
    {
        m_Sharerscount = local_Sharerscount;
    }
    void print(std::ostream& out) const;
  //private:
    /** Directory state */
    Directory_State m_DirectoryState;
    /** Owner of this block */
    NetDest m_Owner;
    /** num of sharers */
    int m_Sharerscount;
};
inline std::ostream&
operator<<(std::ostream& out, const Directory_Entry& obj)
{
    obj.print(out);
    out << std::flush;
    return out;
}

#endif // __Directory_Entry_HH__
