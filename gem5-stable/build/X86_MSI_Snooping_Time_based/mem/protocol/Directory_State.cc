/** \file Directory_State.hh
 *
 * Auto generated C++ code started by /home/mohamed/hourglass/gem5-stable/src/mem/slicc/symbols/Type.py:550
 */

#include <cassert>
#include <iostream>
#include <string>

#include "base/misc.hh"
#include "mem/protocol/Directory_State.hh"

using namespace std;

// Code to convert the current state to an access permission
AccessPermission Directory_State_to_permission(const Directory_State& obj)
{
    switch(obj) {
      case Directory_State_I:
        return AccessPermission_Read_Write;
      case Directory_State_ID:
        return AccessPermission_Busy;
      case Directory_State_ID_W:
        return AccessPermission_Busy;
      case Directory_State_S:
        return AccessPermission_Read_Only;
      case Directory_State_SM_C:
        return AccessPermission_Read_Only;
      case Directory_State_SM_NC:
        return AccessPermission_Read_Only;
      case Directory_State_S_D:
        return AccessPermission_Read_Only;
      case Directory_State_ISA_C:
        return AccessPermission_Read_Only;
      case Directory_State_ISA_NC:
        return AccessPermission_Read_Only;
      case Directory_State_SSA_C:
        return AccessPermission_Read_Only;
      case Directory_State_SSA_NC:
        return AccessPermission_Read_Only;
      case Directory_State_IMA_C:
        return AccessPermission_Read_Only;
      case Directory_State_IMA_NC:
        return AccessPermission_Read_Only;
      case Directory_State_M:
        return AccessPermission_Maybe_Stale;
      case Directory_State_IM:
        return AccessPermission_Busy;
      case Directory_State_MA:
        return AccessPermission_Maybe_Stale;
      case Directory_State_IS:
        return AccessPermission_Busy;
      case Directory_State_M_D:
        return AccessPermission_Busy;
      default:
        panic("Unknown state access permission converstion for Directory_State");
    }
}

// Code for output operator
ostream&
operator<<(ostream& out, const Directory_State& obj)
{
    out << Directory_State_to_string(obj);
    out << flush;
    return out;
}

// Code to convert state to a string
string
Directory_State_to_string(const Directory_State& obj)
{
    switch(obj) {
      case Directory_State_I:
        return "I";
      case Directory_State_ID:
        return "ID";
      case Directory_State_ID_W:
        return "ID_W";
      case Directory_State_S:
        return "S";
      case Directory_State_SM_C:
        return "SM_C";
      case Directory_State_SM_NC:
        return "SM_NC";
      case Directory_State_S_D:
        return "S_D";
      case Directory_State_ISA_C:
        return "ISA_C";
      case Directory_State_ISA_NC:
        return "ISA_NC";
      case Directory_State_SSA_C:
        return "SSA_C";
      case Directory_State_SSA_NC:
        return "SSA_NC";
      case Directory_State_IMA_C:
        return "IMA_C";
      case Directory_State_IMA_NC:
        return "IMA_NC";
      case Directory_State_M:
        return "M";
      case Directory_State_IM:
        return "IM";
      case Directory_State_MA:
        return "MA";
      case Directory_State_IS:
        return "IS";
      case Directory_State_M_D:
        return "M_D";
      default:
        panic("Invalid range for type Directory_State");
    }
}

// Code to convert from a string to the enumeration
Directory_State
string_to_Directory_State(const string& str)
{
    if (str == "I") {
        return Directory_State_I;
    } else if (str == "ID") {
        return Directory_State_ID;
    } else if (str == "ID_W") {
        return Directory_State_ID_W;
    } else if (str == "S") {
        return Directory_State_S;
    } else if (str == "SM_C") {
        return Directory_State_SM_C;
    } else if (str == "SM_NC") {
        return Directory_State_SM_NC;
    } else if (str == "S_D") {
        return Directory_State_S_D;
    } else if (str == "ISA_C") {
        return Directory_State_ISA_C;
    } else if (str == "ISA_NC") {
        return Directory_State_ISA_NC;
    } else if (str == "SSA_C") {
        return Directory_State_SSA_C;
    } else if (str == "SSA_NC") {
        return Directory_State_SSA_NC;
    } else if (str == "IMA_C") {
        return Directory_State_IMA_C;
    } else if (str == "IMA_NC") {
        return Directory_State_IMA_NC;
    } else if (str == "M") {
        return Directory_State_M;
    } else if (str == "IM") {
        return Directory_State_IM;
    } else if (str == "MA") {
        return Directory_State_MA;
    } else if (str == "IS") {
        return Directory_State_IS;
    } else if (str == "M_D") {
        return Directory_State_M_D;
    } else {
        panic("Invalid string conversion for %s, type Directory_State", str);
    }
}

// Code to increment an enumeration type
Directory_State&
operator++(Directory_State& e)
{
    assert(e < Directory_State_NUM);
    return e = Directory_State(e+1);
}
