// Auto generated C++ code started by /home/mohamed/hourglass/gem5-stable/src/mem/slicc/symbols/StateMachine.py:1159
// Directory: MSI Two Level directory protocol

#include <cassert>

#include "base/misc.hh"
#include "base/trace.hh"
#include "debug/ProtocolTrace.hh"
#include "debug/RubyGenerated.hh"
#include "mem/protocol/Directory_Controller.hh"
#include "mem/protocol/Directory_Event.hh"
#include "mem/protocol/Directory_State.hh"
#include "mem/protocol/Types.hh"
#include "mem/ruby/common/Global.hh"
#include "mem/ruby/system/System.hh"

#define HASH_FUN(state, event)  ((int(state)*Directory_Event_NUM)+int(event))

#define GET_TRANSITION_COMMENT() (Directory_transitionComment.str())
#define CLEAR_TRANSITION_COMMENT() (Directory_transitionComment.str(""))

TransitionResult
Directory_Controller::doTransition(Directory_Event event,
                                  Directory_TBE* m_tbe_ptr,
                                  const Address addr)
{
    Directory_State state = getState(m_tbe_ptr, addr);
    Directory_State next_state = state;

    DPRINTF(RubyGenerated, "%s, Time: %lld, state: %s, event: %s, addr: %s\n",
            *this, curCycle(), Directory_State_to_string(state),
            Directory_Event_to_string(event), addr);

    TransitionResult result =
    doTransitionWorker(event, state, next_state, m_tbe_ptr, addr);

    if (result == TransitionResult_Valid) {
        DPRINTF(RubyGenerated, "next_state: %s\n",
                Directory_State_to_string(next_state));
        countTransition(state, event);

        DPRINTFR(ProtocolTrace, "%15d %3s %10s%20s %6s>%-6s %s %s\n",
                 curTick(), m_version, "Directory",
                 Directory_Event_to_string(event),
                 Directory_State_to_string(state),
                 Directory_State_to_string(next_state),
                 addr, GET_TRANSITION_COMMENT());

        CLEAR_TRANSITION_COMMENT();
    setState(m_tbe_ptr, addr, next_state);
    setAccessPermission(addr, next_state);
    } else if (result == TransitionResult_ResourceStall) {
        DPRINTFR(ProtocolTrace, "%15s %3s %10s%20s %6s>%-6s %s %s\n",
                 curTick(), m_version, "Directory",
                 Directory_Event_to_string(event),
                 Directory_State_to_string(state),
                 Directory_State_to_string(next_state),
                 addr, "Resource Stall");
    } else if (result == TransitionResult_ProtocolStall) {
        DPRINTF(RubyGenerated, "stalling\n");
        DPRINTFR(ProtocolTrace, "%15s %3s %10s%20s %6s>%-6s %s %s\n",
                 curTick(), m_version, "Directory",
                 Directory_Event_to_string(event),
                 Directory_State_to_string(state),
                 Directory_State_to_string(next_state),
                 addr, "Protocol Stall");
    }

    return result;
}

TransitionResult
Directory_Controller::doTransitionWorker(Directory_Event event,
                                        Directory_State state,
                                        Directory_State& next_state,
                                        Directory_TBE*& m_tbe_ptr,
                                        const Address& addr)
{
    switch(HASH_FUN(state, event)) {
  case HASH_FUN(Directory_State_I, Directory_Event_GETS_C):
    next_state = Directory_State_ISA_C;
    if (!(*m_responseFromDir_ptr).areNSlotsAvailable(1))
        return TransitionResult_ResourceStall;
    sendDataAck(m_tbe_ptr, addr);
    j_popIncomingRequestQueue(m_tbe_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(Directory_State_I, Directory_Event_GETS_NC):
    next_state = Directory_State_ISA_NC;
    if (!(*m_responseFromDir_ptr).areNSlotsAvailable(1))
        return TransitionResult_ResourceStall;
    sendDataAck(m_tbe_ptr, addr);
    j_popIncomingRequestQueue(m_tbe_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(Directory_State_ISA_C, Directory_Event_DATAACK):
  case HASH_FUN(Directory_State_ISA_NC, Directory_Event_DATAACK):
  case HASH_FUN(Directory_State_SSA_C, Directory_Event_DATAACK):
  case HASH_FUN(Directory_State_SSA_NC, Directory_Event_DATAACK):
  case HASH_FUN(Directory_State_S, Directory_Event_DATAACK):
  case HASH_FUN(Directory_State_SM_NC, Directory_Event_DATAACK):
  case HASH_FUN(Directory_State_SM_C, Directory_Event_DATAACK):
  case HASH_FUN(Directory_State_I, Directory_Event_DATAACK):
    next_state = Directory_State_IS;
    qf_queueMemoryFetchRequest_ack(m_tbe_ptr, addr);
    k_popIncomingResponseQueue(m_tbe_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(Directory_State_SSA_NC, Directory_Event_InvLatest):
    next_state = Directory_State_ISA_NC;
    if (!(*m_requestFromDir_WB_ptr).areNSlotsAvailable(1))
        return TransitionResult_ResourceStall;
    issueInvLatest(m_tbe_ptr, addr);
    j_popIncomingRequestWBQueue(m_tbe_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(Directory_State_SSA_C, Directory_Event_InvLatest):
    next_state = Directory_State_ISA_C;
    if (!(*m_requestFromDir_WB_ptr).areNSlotsAvailable(1))
        return TransitionResult_ResourceStall;
    issueInvLatest(m_tbe_ptr, addr);
    j_popIncomingRequestWBQueue(m_tbe_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(Directory_State_IS, Directory_Event_Memory_Data):
    next_state = Directory_State_S;
    if (!(*m_responseFromDir_ptr).areNSlotsAvailable(1))
        return TransitionResult_ResourceStall;
    updateSharercount_memdata(m_tbe_ptr, addr);
    incSharercount(m_tbe_ptr, addr);
    d_sendData(m_tbe_ptr, addr);
    kd_wakeUpDependents(m_tbe_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(Directory_State_ISA_C, Directory_Event_GETM_C):
  case HASH_FUN(Directory_State_ISA_C, Directory_Event_GETM_NC):
  case HASH_FUN(Directory_State_ISA_C, Directory_Event_GETS_C):
  case HASH_FUN(Directory_State_ISA_C, Directory_Event_GETS_NC):
  case HASH_FUN(Directory_State_ISA_NC, Directory_Event_GETS_NC):
  case HASH_FUN(Directory_State_ISA_NC, Directory_Event_GETM_NC):
  case HASH_FUN(Directory_State_SSA_C, Directory_Event_GETS_C):
  case HASH_FUN(Directory_State_SSA_C, Directory_Event_GETS_NC):
  case HASH_FUN(Directory_State_SSA_C, Directory_Event_GETM_C):
  case HASH_FUN(Directory_State_SSA_C, Directory_Event_GETM_NC):
  case HASH_FUN(Directory_State_SSA_NC, Directory_Event_GETS_NC):
  case HASH_FUN(Directory_State_SSA_NC, Directory_Event_GETM_NC):
  case HASH_FUN(Directory_State_SM_C, Directory_Event_GETM_C):
  case HASH_FUN(Directory_State_SM_C, Directory_Event_GETM_NC):
  case HASH_FUN(Directory_State_SM_C, Directory_Event_GETS_C):
  case HASH_FUN(Directory_State_SM_C, Directory_Event_GETS_NC):
  case HASH_FUN(Directory_State_SM_NC, Directory_Event_GETM_NC):
  case HASH_FUN(Directory_State_SM_NC, Directory_Event_GETS_NC):
  case HASH_FUN(Directory_State_IMA_C, Directory_Event_GETS_C):
  case HASH_FUN(Directory_State_IMA_C, Directory_Event_GETS_NC):
  case HASH_FUN(Directory_State_IMA_C, Directory_Event_GETM_C):
  case HASH_FUN(Directory_State_IMA_C, Directory_Event_GETM_NC):
  case HASH_FUN(Directory_State_IMA_NC, Directory_Event_GETS_NC):
  case HASH_FUN(Directory_State_IMA_NC, Directory_Event_GETM_NC):
  case HASH_FUN(Directory_State_M_D, Directory_Event_GETM_C):
  case HASH_FUN(Directory_State_M_D, Directory_Event_GETM_NC):
  case HASH_FUN(Directory_State_M_D, Directory_Event_GETS_C):
  case HASH_FUN(Directory_State_M_D, Directory_Event_GETS_NC):
  case HASH_FUN(Directory_State_M, Directory_Event_GETM_C):
  case HASH_FUN(Directory_State_M, Directory_Event_GETM_NC):
  case HASH_FUN(Directory_State_M, Directory_Event_GETS_C):
  case HASH_FUN(Directory_State_M, Directory_Event_GETS_NC):
  case HASH_FUN(Directory_State_S_D, Directory_Event_GETM_NC):
  case HASH_FUN(Directory_State_S_D, Directory_Event_GETM_C):
  case HASH_FUN(Directory_State_S_D, Directory_Event_GETS_C):
  case HASH_FUN(Directory_State_S_D, Directory_Event_GETS_NC):
    z_stallAndWaitRequest(m_tbe_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(Directory_State_ISA_NC, Directory_Event_GETS_C):
  case HASH_FUN(Directory_State_ISA_NC, Directory_Event_GETM_C):
  case HASH_FUN(Directory_State_IMA_NC, Directory_Event_GETM_C):
  case HASH_FUN(Directory_State_IMA_NC, Directory_Event_GETS_C):
    next_state = Directory_State_I;
    z_stallAndWaitRequest(m_tbe_ptr, addr);
    kd_wakeUpDependents(m_tbe_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(Directory_State_S, Directory_Event_GETS_C):
    next_state = Directory_State_SSA_C;
    if (!(*m_responseFromDir_ptr).areNSlotsAvailable(1))
        return TransitionResult_ResourceStall;
    sendDataAck(m_tbe_ptr, addr);
    j_popIncomingRequestQueue(m_tbe_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(Directory_State_S, Directory_Event_GETS_NC):
    next_state = Directory_State_SSA_NC;
    if (!(*m_responseFromDir_ptr).areNSlotsAvailable(1))
        return TransitionResult_ResourceStall;
    sendDataAck(m_tbe_ptr, addr);
    j_popIncomingRequestQueue(m_tbe_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(Directory_State_SSA_NC, Directory_Event_GETS_C):
  case HASH_FUN(Directory_State_SSA_NC, Directory_Event_GETM_C):
  case HASH_FUN(Directory_State_SM_NC, Directory_Event_GETS_C):
    next_state = Directory_State_S;
    z_stallAndWaitRequest(m_tbe_ptr, addr);
    kd_wakeUpDependents(m_tbe_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(Directory_State_S, Directory_Event_InvLatest):
  case HASH_FUN(Directory_State_SM_NC, Directory_Event_InvLatest):
  case HASH_FUN(Directory_State_SM_C, Directory_Event_InvLatest):
    next_state = Directory_State_I;
    if (!(*m_requestFromDir_WB_ptr).areNSlotsAvailable(1))
        return TransitionResult_ResourceStall;
    w_deallocateTBE(m_tbe_ptr, addr);
    issueInvLatest(m_tbe_ptr, addr);
    j_popIncomingRequestWBQueue(m_tbe_ptr, addr);
    kd_wakeUpDependents(m_tbe_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(Directory_State_S, Directory_Event_GETM_C):
    next_state = Directory_State_SM_C;
    i_allocateTBE(m_tbe_ptr, addr);
    z_stallAndWaitRequest(m_tbe_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(Directory_State_S, Directory_Event_GETM_NC):
    next_state = Directory_State_SM_NC;
    i_allocateTBE(m_tbe_ptr, addr);
    z_stallAndWaitRequest(m_tbe_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(Directory_State_SM_NC, Directory_Event_GETM_C):
    next_state = Directory_State_SM_C;
    z_stallAndWaitRequest(m_tbe_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(Directory_State_I, Directory_Event_GETM_C):
    next_state = Directory_State_IMA_C;
    if (!(*m_responseFromDir_ptr).areNSlotsAvailable(1))
        return TransitionResult_ResourceStall;
    sendDataAck(m_tbe_ptr, addr);
    j_popIncomingRequestQueue(m_tbe_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(Directory_State_I, Directory_Event_GETM_NC):
    next_state = Directory_State_IMA_NC;
    if (!(*m_responseFromDir_ptr).areNSlotsAvailable(1))
        return TransitionResult_ResourceStall;
    sendDataAck(m_tbe_ptr, addr);
    j_popIncomingRequestQueue(m_tbe_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(Directory_State_IMA_C, Directory_Event_DATAACK):
  case HASH_FUN(Directory_State_IMA_NC, Directory_Event_DATAACK):
    next_state = Directory_State_IM;
    e_ownerIsRequestor_ack(m_tbe_ptr, addr);
    qf_queueMemoryFetchRequest_ack(m_tbe_ptr, addr);
    k_popIncomingResponseQueue(m_tbe_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(Directory_State_IM, Directory_Event_Memory_Data):
    next_state = Directory_State_M;
    if (!(*m_responseFromDir_ptr).areNSlotsAvailable(1))
        return TransitionResult_ResourceStall;
    d_sendData(m_tbe_ptr, addr);
    kd_wakeUpDependents(m_tbe_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(Directory_State_M, Directory_Event_PUTM):
    next_state = Directory_State_M_D;
    j_popIncomingRequestWBQueue(m_tbe_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(Directory_State_S, Directory_Event_PUTM):
    j_popIncomingRequestWBQueue(m_tbe_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(Directory_State_M_D, Directory_Event_Data):
    next_state = Directory_State_I;
    c_clearOwner(m_tbe_ptr, addr);
    qw_queueMemoryWBRequest(m_tbe_ptr, addr);
    k_popIncomingResponseQueue(m_tbe_ptr, addr);
    kd_wakeUpDependents(m_tbe_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(Directory_State_I, Directory_Event_Memory_Ack):
  case HASH_FUN(Directory_State_M, Directory_Event_Memory_Ack):
  case HASH_FUN(Directory_State_IS, Directory_Event_Memory_Ack):
  case HASH_FUN(Directory_State_IM, Directory_Event_Memory_Ack):
  case HASH_FUN(Directory_State_M_D, Directory_Event_Memory_Ack):
  case HASH_FUN(Directory_State_S, Directory_Event_Memory_Ack):
  case HASH_FUN(Directory_State_SM_NC, Directory_Event_Memory_Ack):
  case HASH_FUN(Directory_State_SM_C, Directory_Event_Memory_Ack):
  case HASH_FUN(Directory_State_SSA_NC, Directory_Event_Memory_Ack):
  case HASH_FUN(Directory_State_SSA_C, Directory_Event_Memory_Ack):
  case HASH_FUN(Directory_State_IMA_C, Directory_Event_Memory_Ack):
  case HASH_FUN(Directory_State_IMA_NC, Directory_Event_Memory_Ack):
  case HASH_FUN(Directory_State_ISA_C, Directory_Event_Memory_Ack):
  case HASH_FUN(Directory_State_ISA_NC, Directory_Event_Memory_Ack):
    l_popMemQueue(m_tbe_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(Directory_State_M, Directory_Event_SENDDATA):
    next_state = Directory_State_MA;
    j_popIncomingRequestWBQueue(m_tbe_ptr, addr);
    kd_wakeUpDependents(m_tbe_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(Directory_State_MA, Directory_Event_GETS_C):
  case HASH_FUN(Directory_State_MA, Directory_Event_GETS_NC):
    next_state = Directory_State_S_D;
    j_popIncomingRequestQueue(m_tbe_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(Directory_State_S_D, Directory_Event_Data):
    next_state = Directory_State_S;
    incSharercount(m_tbe_ptr, addr);
    c_clearOwner(m_tbe_ptr, addr);
    qw_queueMemoryWBRequest(m_tbe_ptr, addr);
    k_popIncomingResponseQueue(m_tbe_ptr, addr);
    kd_wakeUpDependents(m_tbe_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(Directory_State_MA, Directory_Event_GETM_C):
  case HASH_FUN(Directory_State_MA, Directory_Event_GETM_NC):
    next_state = Directory_State_M;
    e_ownerIsRequestor(m_tbe_ptr, addr);
    j_popIncomingRequestQueue(m_tbe_ptr, addr);
    kd_wakeUpDependents(m_tbe_ptr, addr);
    return TransitionResult_Valid;

      default:
        fatal("Invalid transition\n"
              "%s time: %d addr: %s event: %s state: %s\n",
              name(), curCycle(), addr, event, state);
    }

    return TransitionResult_Valid;
}
