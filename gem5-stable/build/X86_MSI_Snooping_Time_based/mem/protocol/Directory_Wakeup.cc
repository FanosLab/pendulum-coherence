// Auto generated C++ code started by /home/mohamed/hourglass/gem5-stable/src/mem/slicc/symbols/StateMachine.py:1065
// Directory: MSI Two Level directory protocol

#include <sys/types.h>
#include <unistd.h>

#include <cassert>

#include "base/misc.hh"
#include "debug/RubySlicc.hh"
#include "mem/protocol/Directory_Controller.hh"
#include "mem/protocol/Directory_Event.hh"
#include "mem/protocol/Directory_State.hh"
#include "mem/protocol/Types.hh"
#include "mem/ruby/common/Global.hh"
#include "mem/ruby/system/System.hh"
#include "mem/ruby/slicc_interface/RubySlicc_includes.hh"

using namespace std;

void
Directory_Controller::wakeup()
{
    int counter = 0;
    while (true) {
        // Some cases will put us into an infinite loop without this limit
   //niv
      //  assert(counter <= m_transitions_per_cycle);
        if (counter == m_transitions_per_cycle) {
            // Count how often we are fully utilized
            m_fully_busy_cycles++;

            // Wakeup in another cycle and try again
      //      scheduleEvent(Cycles(1));
      //      break;
        }
            // DirectoryInPort requestNetwork_in
            m_cur_in_port = 0;
                        if ((((*m_requestToDir_ptr)).isReady())) {
                            {
                                // Declare message
                                const RequestMsg* in_msg_ptr M5_VAR_USED;
                                in_msg_ptr = dynamic_cast<const RequestMsg *>(((*m_requestToDir_ptr)).peek());
                                assert(in_msg_ptr != NULL); // Check the cast result
                            int numproc_c
                             = (getnumproc());
                            #ifndef NDEBUG
                            if (!(((((*in_msg_ptr)).m_Destination).isElement(m_machineID)))) {
                                panic("Runtime Error at MSI_Snooping_Time_based_base-dir.sm:247: %s.\n", "assert failure");

                            }
                            #endif
                            ;
                                if (((((*in_msg_ptr)).m_Type == CoherenceRequestType_GETS) || (((*in_msg_ptr)).m_Type == CoherenceRequestType_GETI))) {
                                        if (((IDToInt(((((*in_msg_ptr)).m_Requestor).getNum()))) < numproc_c)) {
                                            {

                                                TransitionResult result = doTransition(Directory_Event_GETS_C, (((*m_TBEs_ptr)).lookup(((*in_msg_ptr)).m_Addr)), ((*in_msg_ptr)).m_Addr);

                                                if (result == TransitionResult_Valid) {
                                                    counter++;
                                                    continue; // Check the first port again
                                                }

                                                if (result == TransitionResult_ResourceStall) {
                                                    scheduleEvent(Cycles(1));

                                                    // Cannot do anything with this transition, go check next doable transition (mostly likely of next port)
                                                }
                                            }
                                            ;
                                        } else {
                                                if (((IDToInt(((((*in_msg_ptr)).m_Requestor).getNum()))) >= numproc_c)) {
                                                    {

                                                        TransitionResult result = doTransition(Directory_Event_GETS_NC, (((*m_TBEs_ptr)).lookup(((*in_msg_ptr)).m_Addr)), ((*in_msg_ptr)).m_Addr);

                                                        if (result == TransitionResult_Valid) {
                                                            counter++;
                                                            continue; // Check the first port again
                                                        }

                                                        if (result == TransitionResult_ResourceStall) {
                                                            scheduleEvent(Cycles(1));

                                                            // Cannot do anything with this transition, go check next doable transition (mostly likely of next port)
                                                        }
                                                    }
                                                    ;
                                                }
                                            }
                                        } else {
                                                if ((((*in_msg_ptr)).m_Type == CoherenceRequestType_GETM)) {
                                                        if (((IDToInt(((((*in_msg_ptr)).m_Requestor).getNum()))) < numproc_c)) {
                                                            {

                                                                TransitionResult result = doTransition(Directory_Event_GETM_C, (((*m_TBEs_ptr)).lookup(((*in_msg_ptr)).m_Addr)), ((*in_msg_ptr)).m_Addr);

                                                                if (result == TransitionResult_Valid) {
                                                                    counter++;
                                                                    continue; // Check the first port again
                                                                }

                                                                if (result == TransitionResult_ResourceStall) {
                                                                    scheduleEvent(Cycles(1));

                                                                    // Cannot do anything with this transition, go check next doable transition (mostly likely of next port)
                                                                }
                                                            }
                                                            ;
                                                        } else {
                                                                if (((IDToInt(((((*in_msg_ptr)).m_Requestor).getNum()))) >= numproc_c)) {
                                                                    {

                                                                        TransitionResult result = doTransition(Directory_Event_GETM_NC, (((*m_TBEs_ptr)).lookup(((*in_msg_ptr)).m_Addr)), ((*in_msg_ptr)).m_Addr);

                                                                        if (result == TransitionResult_Valid) {
                                                                            counter++;
                                                                            continue; // Check the first port again
                                                                        }

                                                                        if (result == TransitionResult_ResourceStall) {
                                                                            scheduleEvent(Cycles(1));

                                                                            // Cannot do anything with this transition, go check next doable transition (mostly likely of next port)
                                                                        }
                                                                    }
                                                                    ;
                                                                }
                                                            }
                                                        } else {
                                                                if ((((*in_msg_ptr)).m_Type == CoherenceRequestType_UPG)) {
                                                                    {

                                                                        TransitionResult result = doTransition(Directory_Event_UPG, (((*m_TBEs_ptr)).lookup(((*in_msg_ptr)).m_Addr)), ((*in_msg_ptr)).m_Addr);

                                                                        if (result == TransitionResult_Valid) {
                                                                            counter++;
                                                                            continue; // Check the first port again
                                                                        }

                                                                        if (result == TransitionResult_ResourceStall) {
                                                                            scheduleEvent(Cycles(1));

                                                                            // Cannot do anything with this transition, go check next doable transition (mostly likely of next port)
                                                                        }
                                                                    }
                                                                    ;
                                                                } else {
                                                                        if ((((*in_msg_ptr)).m_Type == CoherenceRequestType_PUTM)) {
                                                                                if ((((*(getDirectoryEntry(((*in_msg_ptr)).m_Addr))).m_Owner).isElement(((*in_msg_ptr)).m_Requestor))) {
                                                                                    {

                                                                                        TransitionResult result = doTransition(Directory_Event_PUTM, (((*m_TBEs_ptr)).lookup(((*in_msg_ptr)).m_Addr)), ((*in_msg_ptr)).m_Addr);

                                                                                        if (result == TransitionResult_Valid) {
                                                                                            counter++;
                                                                                            continue; // Check the first port again
                                                                                        }

                                                                                        if (result == TransitionResult_ResourceStall) {
                                                                                            scheduleEvent(Cycles(1));

                                                                                            // Cannot do anything with this transition, go check next doable transition (mostly likely of next port)
                                                                                        }
                                                                                    }
                                                                                    ;
                                                                                } else {
                                                                                    (((*m_requestToDir_ptr)).dequeue());
                                                                                }
                                                                            } else {
                                                                                    if ((((*in_msg_ptr)).m_Type == CoherenceRequestType_SENDDATA)) {
                                                                                        {

                                                                                            TransitionResult result = doTransition(Directory_Event_SENDDATA, (((*m_TBEs_ptr)).lookup(((*in_msg_ptr)).m_Addr)), ((*in_msg_ptr)).m_Addr);

                                                                                            if (result == TransitionResult_Valid) {
                                                                                                counter++;
                                                                                                continue; // Check the first port again
                                                                                            }

                                                                                            if (result == TransitionResult_ResourceStall) {
                                                                                                scheduleEvent(Cycles(1));

                                                                                                // Cannot do anything with this transition, go check next doable transition (mostly likely of next port)
                                                                                            }
                                                                                        }
                                                                                        ;
                                                                                    } else {
                                                                                            if ((((*in_msg_ptr)).m_Type == CoherenceRequestType_SELFINV)) {
                                                                                                Directory_TBE* tbetemp
                                                                                                 = (((*m_TBEs_ptr)).lookup(((*in_msg_ptr)).m_Addr));
                                                                                                    if ((tbetemp != NULL)) {
                                                                                                        (((*(((*m_TBEs_ptr)).lookup(((*in_msg_ptr)).m_Addr))).m_Sharers).add(((*in_msg_ptr)).m_Requestor));
                                                                                                            if (((((*(((*m_TBEs_ptr)).lookup(((*in_msg_ptr)).m_Addr))).m_Sharers).count()) == (*(getDirectoryEntry(((*in_msg_ptr)).m_Addr))).m_Sharerscount)) {
                                                                                                                {

                                                                                                                    TransitionResult result = doTransition(Directory_Event_InvLatest, (((*m_TBEs_ptr)).lookup(((*in_msg_ptr)).m_Addr)), ((*in_msg_ptr)).m_Addr);

                                                                                                                    if (result == TransitionResult_Valid) {
                                                                                                                        counter++;
                                                                                                                        continue; // Check the first port again
                                                                                                                    }

                                                                                                                    if (result == TransitionResult_ResourceStall) {
                                                                                                                        scheduleEvent(Cycles(1));

                                                                                                                        // Cannot do anything with this transition, go check next doable transition (mostly likely of next port)
                                                                                                                    }
                                                                                                                }
                                                                                                                ;
                                                                                                            } else {
                                                                                                                (((*m_requestToDir_ptr)).dequeue());
                                                                                                            }
                                                                                                        } else {
                                                                                                            (*(getDirectoryEntry(((*in_msg_ptr)).m_Addr))).m_Sharerscount = ((*(getDirectoryEntry(((*in_msg_ptr)).m_Addr))).m_Sharerscount - (1));
                                                                                                                if (((*(getDirectoryEntry(((*in_msg_ptr)).m_Addr))).m_Sharerscount == (0))) {
                                                                                                                    {

                                                                                                                        TransitionResult result = doTransition(Directory_Event_InvLatest, (((*m_TBEs_ptr)).lookup(((*in_msg_ptr)).m_Addr)), ((*in_msg_ptr)).m_Addr);

                                                                                                                        if (result == TransitionResult_Valid) {
                                                                                                                            counter++;
                                                                                                                            continue; // Check the first port again
                                                                                                                        }

                                                                                                                        if (result == TransitionResult_ResourceStall) {
                                                                                                                            scheduleEvent(Cycles(1));

                                                                                                                            // Cannot do anything with this transition, go check next doable transition (mostly likely of next port)
                                                                                                                        }
                                                                                                                    }
                                                                                                                    ;
                                                                                                                } else {
                                                                                                                        if (((*(getDirectoryEntry(((*in_msg_ptr)).m_Addr))).m_Sharerscount < (0))) {
                                                                                                                            (*(getDirectoryEntry(((*in_msg_ptr)).m_Addr))).m_Sharerscount = (0);
                                                                                                                        }
                                                                                                                        (((*m_requestToDir_ptr)).dequeue());
                                                                                                                    }
                                                                                                                }
                                                                                                            } else {
                                                                                                                panic("Runtime Error at MSI_Snooping_Time_based_base-dir.sm:327: %s.\n", ("Invalid message"));
                                                                                                                ;
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                        }
                                                                                    }
            // DirectoryInPort requestNetworkWB_in
            m_cur_in_port = 0;
                        if ((((*m_requestToDir_WB_ptr)).isReady())) {
                            {
                                // Declare message
                                const RequestMsg* in_msg_ptr M5_VAR_USED;
                                in_msg_ptr = dynamic_cast<const RequestMsg *>(((*m_requestToDir_WB_ptr)).peek());
                                assert(in_msg_ptr != NULL); // Check the cast result
                            #ifndef NDEBUG
                            if (!(((((*in_msg_ptr)).m_Destination).isElement(m_machineID)))) {
                                panic("Runtime Error at MSI_Snooping_Time_based_base-dir.sm:337: %s.\n", "assert failure");

                            }
                            #endif
                            ;
                                if (((((*in_msg_ptr)).m_Type == CoherenceRequestType_GETS) || (((*in_msg_ptr)).m_Type == CoherenceRequestType_GETI))) {
                                    {

                                        TransitionResult result = doTransition(Directory_Event_GETS, (((*m_TBEs_ptr)).lookup(((*in_msg_ptr)).m_Addr)), ((*in_msg_ptr)).m_Addr);

                                        if (result == TransitionResult_Valid) {
                                            counter++;
                                            continue; // Check the first port again
                                        }

                                        if (result == TransitionResult_ResourceStall) {
                                            scheduleEvent(Cycles(1));

                                            // Cannot do anything with this transition, go check next doable transition (mostly likely of next port)
                                        }
                                    }
                                    ;
                                } else {
                                        if ((((*in_msg_ptr)).m_Type == CoherenceRequestType_GETM)) {
                                            {

                                                TransitionResult result = doTransition(Directory_Event_GETM, (((*m_TBEs_ptr)).lookup(((*in_msg_ptr)).m_Addr)), ((*in_msg_ptr)).m_Addr);

                                                if (result == TransitionResult_Valid) {
                                                    counter++;
                                                    continue; // Check the first port again
                                                }

                                                if (result == TransitionResult_ResourceStall) {
                                                    scheduleEvent(Cycles(1));

                                                    // Cannot do anything with this transition, go check next doable transition (mostly likely of next port)
                                                }
                                            }
                                            ;
                                        } else {
                                                if ((((*in_msg_ptr)).m_Type == CoherenceRequestType_UPG)) {
                                                    {

                                                        TransitionResult result = doTransition(Directory_Event_UPG, (((*m_TBEs_ptr)).lookup(((*in_msg_ptr)).m_Addr)), ((*in_msg_ptr)).m_Addr);

                                                        if (result == TransitionResult_Valid) {
                                                            counter++;
                                                            continue; // Check the first port again
                                                        }

                                                        if (result == TransitionResult_ResourceStall) {
                                                            scheduleEvent(Cycles(1));

                                                            // Cannot do anything with this transition, go check next doable transition (mostly likely of next port)
                                                        }
                                                    }
                                                    ;
                                                } else {
                                                        if ((((*in_msg_ptr)).m_Type == CoherenceRequestType_PUTM)) {
                                                                if ((((*(getDirectoryEntry(((*in_msg_ptr)).m_Addr))).m_Owner).isElement(((*in_msg_ptr)).m_Requestor))) {
                                                                    {

                                                                        TransitionResult result = doTransition(Directory_Event_PUTM, (((*m_TBEs_ptr)).lookup(((*in_msg_ptr)).m_Addr)), ((*in_msg_ptr)).m_Addr);

                                                                        if (result == TransitionResult_Valid) {
                                                                            counter++;
                                                                            continue; // Check the first port again
                                                                        }

                                                                        if (result == TransitionResult_ResourceStall) {
                                                                            scheduleEvent(Cycles(1));

                                                                            // Cannot do anything with this transition, go check next doable transition (mostly likely of next port)
                                                                        }
                                                                    }
                                                                    ;
                                                                } else {
                                                                    (((*m_requestToDir_WB_ptr)).dequeue());
                                                                }
                                                            } else {
                                                                    if ((((*in_msg_ptr)).m_Type == CoherenceRequestType_SENDDATA)) {
                                                                        {

                                                                            TransitionResult result = doTransition(Directory_Event_SENDDATA, (((*m_TBEs_ptr)).lookup(((*in_msg_ptr)).m_Addr)), ((*in_msg_ptr)).m_Addr);

                                                                            if (result == TransitionResult_Valid) {
                                                                                counter++;
                                                                                continue; // Check the first port again
                                                                            }

                                                                            if (result == TransitionResult_ResourceStall) {
                                                                                scheduleEvent(Cycles(1));

                                                                                // Cannot do anything with this transition, go check next doable transition (mostly likely of next port)
                                                                            }
                                                                        }
                                                                        ;
                                                                    } else {
                                                                            if ((((*in_msg_ptr)).m_Type == CoherenceRequestType_SELFINV)) {
                                                                                Directory_TBE* tbetemp
                                                                                 = (((*m_TBEs_ptr)).lookup(((*in_msg_ptr)).m_Addr));
                                                                                    if ((tbetemp != NULL)) {
                                                                                        (((*(((*m_TBEs_ptr)).lookup(((*in_msg_ptr)).m_Addr))).m_Sharers).add(((*in_msg_ptr)).m_Requestor));
                                                                                            if (((((*(((*m_TBEs_ptr)).lookup(((*in_msg_ptr)).m_Addr))).m_Sharers).count()) == (*(getDirectoryEntry(((*in_msg_ptr)).m_Addr))).m_Sharerscount)) {
                                                                                                {

                                                                                                    TransitionResult result = doTransition(Directory_Event_InvLatest, (((*m_TBEs_ptr)).lookup(((*in_msg_ptr)).m_Addr)), ((*in_msg_ptr)).m_Addr);

                                                                                                    if (result == TransitionResult_Valid) {
                                                                                                        counter++;
                                                                                                        continue; // Check the first port again
                                                                                                    }

                                                                                                    if (result == TransitionResult_ResourceStall) {
                                                                                                        scheduleEvent(Cycles(1));

                                                                                                        // Cannot do anything with this transition, go check next doable transition (mostly likely of next port)
                                                                                                    }
                                                                                                }
                                                                                                ;
                                                                                            } else {
                                                                                                (((*m_requestToDir_WB_ptr)).dequeue());
                                                                                            }
                                                                                        } else {
                                                                                            (*(getDirectoryEntry(((*in_msg_ptr)).m_Addr))).m_Sharerscount = ((*(getDirectoryEntry(((*in_msg_ptr)).m_Addr))).m_Sharerscount - (1));
                                                                                                if (((*(getDirectoryEntry(((*in_msg_ptr)).m_Addr))).m_Sharerscount == (0))) {
                                                                                                    {

                                                                                                        TransitionResult result = doTransition(Directory_Event_InvLatest, (((*m_TBEs_ptr)).lookup(((*in_msg_ptr)).m_Addr)), ((*in_msg_ptr)).m_Addr);

                                                                                                        if (result == TransitionResult_Valid) {
                                                                                                            counter++;
                                                                                                            continue; // Check the first port again
                                                                                                        }

                                                                                                        if (result == TransitionResult_ResourceStall) {
                                                                                                            scheduleEvent(Cycles(1));

                                                                                                            // Cannot do anything with this transition, go check next doable transition (mostly likely of next port)
                                                                                                        }
                                                                                                    }
                                                                                                    ;
                                                                                                } else {
                                                                                                        if (((*(getDirectoryEntry(((*in_msg_ptr)).m_Addr))).m_Sharerscount < (0))) {
                                                                                                            (*(getDirectoryEntry(((*in_msg_ptr)).m_Addr))).m_Sharerscount = (0);
                                                                                                        }
                                                                                                        (((*m_requestToDir_WB_ptr)).dequeue());
                                                                                                    }
                                                                                                }
                                                                                            } else {
                                                                                                panic("Runtime Error at MSI_Snooping_Time_based_base-dir.sm:396: %s.\n", ("Invalid message"));
                                                                                                ;
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                        }
                                                                    }
            // DirectoryInPort responseNetwork_in
            m_cur_in_port = 1;
                        if ((((*m_responseToDir_ptr)).isReady())) {
                            {
                                // Declare message
                                const ResponseMsg* in_msg_ptr M5_VAR_USED;
                                in_msg_ptr = dynamic_cast<const ResponseMsg *>(((*m_responseToDir_ptr)).peek());
                                assert(in_msg_ptr != NULL); // Check the cast result
                            #ifndef NDEBUG
                            if (!(((((*in_msg_ptr)).m_Destination).isElement(m_machineID)))) {
                                panic("Runtime Error at MSI_Snooping_Time_based_base-dir.sm:405: %s.\n", "assert failure");

                            }
                            #endif
                            ;
                                if (((((*in_msg_ptr)).m_Type == CoherenceResponseType_DATA_TO_CACHE) || (((*in_msg_ptr)).m_Type == CoherenceResponseType_DATA_TO_WB))) {
                                    {

                                        TransitionResult result = doTransition(Directory_Event_Data, (((*m_TBEs_ptr)).lookup(((*in_msg_ptr)).m_Addr)), ((*in_msg_ptr)).m_Addr);

                                        if (result == TransitionResult_Valid) {
                                            counter++;
                                            continue; // Check the first port again
                                        }

                                        if (result == TransitionResult_ResourceStall) {
                                            scheduleEvent(Cycles(1));

                                            // Cannot do anything with this transition, go check next doable transition (mostly likely of next port)
                                        }
                                    }
                                    ;
                                } else {
                                        if ((((*in_msg_ptr)).m_Type == CoherenceResponseType_DATAACK)) {
                                            {

                                                TransitionResult result = doTransition(Directory_Event_DATAACK, (((*m_TBEs_ptr)).lookup(((*in_msg_ptr)).m_Addr)), ((*in_msg_ptr)).m_Addr);

                                                if (result == TransitionResult_Valid) {
                                                    counter++;
                                                    continue; // Check the first port again
                                                }

                                                if (result == TransitionResult_ResourceStall) {
                                                    scheduleEvent(Cycles(1));

                                                    // Cannot do anything with this transition, go check next doable transition (mostly likely of next port)
                                                }
                                            }
                                            ;
                                        } else {
                                            panic("Runtime Error at MSI_Snooping_Time_based_base-dir.sm:416: %s.\n", ("Invalid message"));
                                            ;
                                        }
                                    }
                                    }
                                }
            // DirectoryInPort memQueue_in
            m_cur_in_port = 2;
                        if ((((*m_responseFromMemory_ptr)).isReady())) {
                            {
                                // Declare message
                                const MemoryMsg* in_msg_ptr M5_VAR_USED;
                                in_msg_ptr = dynamic_cast<const MemoryMsg *>(((*m_responseFromMemory_ptr)).peek());
                                assert(in_msg_ptr != NULL); // Check the cast result
                                if ((((*in_msg_ptr)).m_Type == MemoryRequestType_MEMORY_READ)) {
                                    {

                                        TransitionResult result = doTransition(Directory_Event_Memory_Data, (((*m_TBEs_ptr)).lookup(((*in_msg_ptr)).m_Addr)), ((*in_msg_ptr)).m_Addr);

                                        if (result == TransitionResult_Valid) {
                                            counter++;
                                            continue; // Check the first port again
                                        }

                                        if (result == TransitionResult_ResourceStall) {
                                            scheduleEvent(Cycles(1));

                                            // Cannot do anything with this transition, go check next doable transition (mostly likely of next port)
                                        }
                                    }
                                    ;
                                } else {
                                        if ((((*in_msg_ptr)).m_Type == MemoryRequestType_MEMORY_WB)) {
                                            {

                                                TransitionResult result = doTransition(Directory_Event_Memory_Ack, (((*m_TBEs_ptr)).lookup(((*in_msg_ptr)).m_Addr)), ((*in_msg_ptr)).m_Addr);

                                                if (result == TransitionResult_Valid) {
                                                    counter++;
                                                    continue; // Check the first port again
                                                }

                                                if (result == TransitionResult_ResourceStall) {
                                                    scheduleEvent(Cycles(1));

                                                    // Cannot do anything with this transition, go check next doable transition (mostly likely of next port)
                                                }
                                            }
                                            ;
                                        } else {
                                            panic("Runtime Error at MSI_Snooping_Time_based_base-dir.sm:436: %s.\n", ("Invalid message"));
                                            ;
                                        }
                                    }
                                    }
                                }
        break;  // If we got this far, we have nothing left todo
    }
}
