/** \file L1Cache_Controller.cc
 *
 * Auto generated C++ code started by /home/mohamed/hourglass/gem5-stable/src/mem/slicc/symbols/StateMachine.py:449
 * Created by slicc definition of Module "MSI Snooping L1 Cache CMP"
 */

#include <sys/types.h>
#include <unistd.h>

#include <cassert>
#include <sstream>
#include <string>

#include "base/compiler.hh"
#include "base/cprintf.hh"
#include "debug/RubyGenerated.hh"
#include "debug/RubySlicc.hh"
#include "mem/protocol/L1Cache_Controller.hh"
#include "mem/protocol/L1Cache_Event.hh"
#include "mem/protocol/L1Cache_State.hh"
#include "mem/protocol/Types.hh"
#include "mem/ruby/common/Global.hh"
#include "mem/ruby/system/System.hh"
#include "mem/ruby/slicc_interface/RubySlicc_includes.hh"

using namespace std;
#include "mem/protocol/TBETable.hh"
#include "mem/protocol/MessageBuffer.hh"
#include "mem/protocol/TimerTable.hh"
L1Cache_Controller *
L1Cache_ControllerParams::create()
{
    return new L1Cache_Controller(this);
}

int L1Cache_Controller::m_num_controllers = 0;
std::vector<Stats::Vector *>  L1Cache_Controller::eventVec;
std::vector<std::vector<Stats::Vector *> >  L1Cache_Controller::transVec;

// for adding information to the protocol debug trace
stringstream L1Cache_transitionComment;

#ifndef NDEBUG
#define APPEND_TRANSITION_COMMENT(str) (L1Cache_transitionComment << str)
#else
#define APPEND_TRANSITION_COMMENT(str) do {} while (0)
#endif

/** \brief constructor */
L1Cache_Controller::L1Cache_Controller(const Params *p)
    : AbstractController(p)
{
    m_machineID.type = MachineType_L1Cache;
    m_machineID.num = m_version;
    m_num_controllers++;

    m_in_ports = 8;
    m_sequencer_ptr = p->sequencer;
    m_sequencer_ptr->setController(this);
    m_Icache_ptr = p->Icache;
    m_Dcache_ptr = p->Dcache;
    m_l1_request_latency = p->l1_request_latency;
    m_l1_response_latency = p->l1_response_latency;
    m_send_evictions = p->send_evictions;
    m_is_blocked = p->is_blocked;
    m_max_wcet_latency = p->max_wcet_latency;
    m_max_arbit_latency = p->max_arbit_latency;
    m_max_interC_latency = p->max_interC_latency;
    m_max_intraC_latency = p->max_intraC_latency;
    m_max_S_to_M_latency = p->max_S_to_M_latency;
    m_total_latency = p->total_latency;
    m_timeout_latency_CC = p->timeout_latency_CC;
    m_timeout_latency_CN = p->timeout_latency_CN;
    m_timeout_latency_NC = p->timeout_latency_NC;
    m_timeout_latency_NN = p->timeout_latency_NN;
    m_timeout_0 = p->timeout_0;
    m_mandatoryQueue_ptr = new MessageBuffer();
    m_mandatoryQueue_ptr->setReceiver(this);

    for (int state = 0; state < L1Cache_State_NUM; state++) {
        for (int event = 0; event < L1Cache_Event_NUM; event++) {
            m_possible[state][event] = false;
            m_counters[state][event] = 0;
        }
    }
    for (int event = 0; event < L1Cache_Event_NUM; event++) {
        m_event_counters[event] = 0;
    }
}

void
L1Cache_Controller::setNetQueue(const std::string& name, MessageBuffer *b)
{
  
    MachineType machine_type = string_to_MachineType("L1Cache");
    int base M5_VAR_USED = MachineType_base_number(machine_type);


    if ("requestFromCache" == name) {
        m_requestFromCache_ptr = b;
        assert(m_requestFromCache_ptr != NULL);
        m_net_ptr->setToNetQueue(m_version + base, false, 2,
                                         "request", b);
        m_requestFromCache_ptr->setSender(this);
        m_requestFromCache_ptr->setOrdering(false);
        m_requestFromCache_ptr->resize(m_buffer_size);
        m_requestFromCache_ptr->setRecycleLatency(m_recycle_latency);
        m_requestFromCache_ptr->setDescription("[Version " + to_string(m_version) + ", L1Cache, name=requestFromCache]");
    }
    if ("requestFromCacheWB" == name) {
        m_requestFromCacheWB_ptr = b;
        assert(m_requestFromCacheWB_ptr != NULL);
        m_net_ptr->setToNetQueue(m_version + base, false, 6,
                                         "requestWB", b);
        m_requestFromCacheWB_ptr->setSender(this);
        m_requestFromCacheWB_ptr->setOrdering(false);
        m_requestFromCacheWB_ptr->resize(m_buffer_size);
        m_requestFromCacheWB_ptr->setRecycleLatency(m_recycle_latency);
        m_requestFromCacheWB_ptr->setDescription("[Version " + to_string(m_version) + ", L1Cache, name=requestFromCacheWB]");
    }
    if ("responseFromCache" == name) {
        m_responseFromCache_ptr = b;
        assert(m_responseFromCache_ptr != NULL);
        m_net_ptr->setToNetQueue(m_version + base, false, 4,
                                         "response", b);
        m_responseFromCache_ptr->setSender(this);
        m_responseFromCache_ptr->setOrdering(false);
        m_responseFromCache_ptr->resize(m_buffer_size);
        m_responseFromCache_ptr->setRecycleLatency(m_recycle_latency);
        m_responseFromCache_ptr->setDescription("[Version " + to_string(m_version) + ", L1Cache, name=responseFromCache]");
    }
    if ("atomicRequestFromCache" == name) {
        m_atomicRequestFromCache_ptr = b;
        assert(m_atomicRequestFromCache_ptr != NULL);
        m_net_ptr->setToNetQueue(m_version + base, false, 5,
                                         "atomicRequest", b);
        m_atomicRequestFromCache_ptr->setSender(this);
        m_atomicRequestFromCache_ptr->setOrdering(false);
        m_atomicRequestFromCache_ptr->resize(m_buffer_size);
        m_atomicRequestFromCache_ptr->setRecycleLatency(m_recycle_latency);
        m_atomicRequestFromCache_ptr->setDescription("[Version " + to_string(m_version) + ", L1Cache, name=atomicRequestFromCache]");
    }
    if ("responseToCache" == name) {
        m_responseToCache_ptr = b;
        assert(m_responseToCache_ptr != NULL);
        m_net_ptr->setFromNetQueue(m_version + base, false, 4,
                                         "response", b);
        m_responseToCache_ptr->setReceiver(this);
        m_responseToCache_ptr->setOrdering(false);
        m_responseToCache_ptr->resize(m_buffer_size);
        m_responseToCache_ptr->setRecycleLatency(m_recycle_latency);
        m_responseToCache_ptr->setDescription("[Version " + to_string(m_version) + ", L1Cache, name=responseToCache]");
    }
    if ("requestToCache" == name) {
        m_requestToCache_ptr = b;
        assert(m_requestToCache_ptr != NULL);
        m_net_ptr->setFromNetQueue(m_version + base, false, 2,
                                         "request", b);
        m_requestToCache_ptr->setReceiver(this);
        m_requestToCache_ptr->setOrdering(false);
        m_requestToCache_ptr->resize(m_buffer_size);
        m_requestToCache_ptr->setRecycleLatency(m_recycle_latency);
        m_requestToCache_ptr->setDescription("[Version " + to_string(m_version) + ", L1Cache, name=requestToCache]");
    }
    if ("requestToCacheWB" == name) {
        m_requestToCacheWB_ptr = b;
        assert(m_requestToCacheWB_ptr != NULL);
        m_net_ptr->setFromNetQueue(m_version + base, false, 6,
                                         "requestWB", b);
        m_requestToCacheWB_ptr->setReceiver(this);
        m_requestToCacheWB_ptr->setOrdering(false);
        m_requestToCacheWB_ptr->resize(m_buffer_size);
        m_requestToCacheWB_ptr->setRecycleLatency(m_recycle_latency);
        m_requestToCacheWB_ptr->setDescription("[Version " + to_string(m_version) + ", L1Cache, name=requestToCacheWB]");
    }
    if ("atomicRequestToCache" == name) {
        m_atomicRequestToCache_ptr = b;
        assert(m_atomicRequestToCache_ptr != NULL);
        m_net_ptr->setFromNetQueue(m_version + base, false, 5,
                                         "atomicRequest", b);
        m_atomicRequestToCache_ptr->setReceiver(this);
        m_atomicRequestToCache_ptr->setOrdering(false);
        m_atomicRequestToCache_ptr->resize(m_buffer_size);
        m_atomicRequestToCache_ptr->setRecycleLatency(m_recycle_latency);
        m_atomicRequestToCache_ptr->setDescription("[Version " + to_string(m_version) + ", L1Cache, name=atomicRequestToCache]");
    }
}

void
L1Cache_Controller::init()
{
    // initialize objects

    m_TBEs_ptr  = new TBETable<L1Cache_TBE>(m_number_of_TBEs);
    assert(m_TBEs_ptr != NULL);
    assert(m_mandatoryQueue_ptr != NULL);
    m_mandatoryQueue_ptr->setOrdering(false);
    m_mandatoryQueue_ptr->setRecycleLatency(m_recycle_latency);
    m_TimerTable_CC_ptr  = new TimerTable();
    assert(m_TimerTable_CC_ptr != NULL);
    m_TimerTable_CC_ptr->setClockObj(this);
    m_TimerTable_CN_ptr  = new TimerTable();
    assert(m_TimerTable_CN_ptr != NULL);
    m_TimerTable_CN_ptr->setClockObj(this);
    m_TimerTable_NC_ptr  = new TimerTable();
    assert(m_TimerTable_NC_ptr != NULL);
    m_TimerTable_NC_ptr->setClockObj(this);
    m_TimerTable_NN_ptr  = new TimerTable();
    assert(m_TimerTable_NN_ptr != NULL);
    m_TimerTable_NN_ptr->setClockObj(this);


    (*m_responseToCache_ptr).setConsumer(this);
    (*m_responseToCache_ptr).setDescription("[Version " + to_string(m_version) + ", L1Cache, responseNetwork_in]");
    (*m_requestToCache_ptr).setConsumer(this);
    (*m_requestToCache_ptr).setDescription("[Version " + to_string(m_version) + ", L1Cache, requestNetwork_in]");
    (*m_requestToCacheWB_ptr).setConsumer(this);
    (*m_requestToCacheWB_ptr).setDescription("[Version " + to_string(m_version) + ", L1Cache, requestNetworkWB_in]");
    (*m_mandatoryQueue_ptr).setConsumer(this);
    (*m_mandatoryQueue_ptr).setDescription("[Version " + to_string(m_version) + ", L1Cache, mandatoryQueue_in]");
    (*m_TimerTable_CC_ptr).setConsumer(this);
    (*m_TimerTable_CC_ptr).setDescription("[Version " + to_string(m_version) + ", L1Cache, TimerTable_CC_in]");
    (*m_TimerTable_CN_ptr).setConsumer(this);
    (*m_TimerTable_CN_ptr).setDescription("[Version " + to_string(m_version) + ", L1Cache, TimerTable_CN_in]");
    (*m_TimerTable_NC_ptr).setConsumer(this);
    (*m_TimerTable_NC_ptr).setDescription("[Version " + to_string(m_version) + ", L1Cache, TimerTable_NC_in]");
    (*m_TimerTable_NN_ptr).setConsumer(this);
    (*m_TimerTable_NN_ptr).setDescription("[Version " + to_string(m_version) + ", L1Cache, TimerTable_NN_in]");

    possibleTransition(L1Cache_State_I, L1Cache_Event_Load);
    possibleTransition(L1Cache_State_I, L1Cache_Event_Ifetch);
    possibleTransition(L1Cache_State_IS_AD, L1Cache_Event_OWN_GETS);
    possibleTransition(L1Cache_State_IS_AD, L1Cache_Event_OWN_GETI);
    possibleTransition(L1Cache_State_IS_AD, L1Cache_Event_Other_GETS_C);
    possibleTransition(L1Cache_State_IS_AD, L1Cache_Event_Other_GETS_NC);
    possibleTransition(L1Cache_State_IS_AD, L1Cache_Event_Other_GETM_C);
    possibleTransition(L1Cache_State_IS_AD, L1Cache_Event_Other_GETM_NC);
    possibleTransition(L1Cache_State_IS_AD, L1Cache_Event_Other_GETI_C);
    possibleTransition(L1Cache_State_IS_AD, L1Cache_Event_Other_GETI_NC);
    possibleTransition(L1Cache_State_IM_AD, L1Cache_Event_Other_GETS_C);
    possibleTransition(L1Cache_State_IM_AD, L1Cache_Event_Other_GETS_NC);
    possibleTransition(L1Cache_State_IM_AD, L1Cache_Event_Other_GETM_C);
    possibleTransition(L1Cache_State_IM_AD, L1Cache_Event_Other_GETM_NC);
    possibleTransition(L1Cache_State_IM_AD, L1Cache_Event_Other_GETI_C);
    possibleTransition(L1Cache_State_IM_AD, L1Cache_Event_Other_GETI_NC);
    possibleTransition(L1Cache_State_S_TI, L1Cache_Event_Other_GETS_C);
    possibleTransition(L1Cache_State_S_TI, L1Cache_Event_Other_GETS_NC);
    possibleTransition(L1Cache_State_S_TI, L1Cache_Event_Other_GETM_C);
    possibleTransition(L1Cache_State_S_TI, L1Cache_Event_Other_GETM_NC);
    possibleTransition(L1Cache_State_S_TI, L1Cache_Event_Other_GETI_C);
    possibleTransition(L1Cache_State_S_TI, L1Cache_Event_Other_GETI_NC);
    possibleTransition(L1Cache_State_I, L1Cache_Event_Other_GETS_C);
    possibleTransition(L1Cache_State_I, L1Cache_Event_Other_GETS_NC);
    possibleTransition(L1Cache_State_I, L1Cache_Event_Other_GETM_C);
    possibleTransition(L1Cache_State_I, L1Cache_Event_Other_GETM_NC);
    possibleTransition(L1Cache_State_I, L1Cache_Event_Other_GETI_C);
    possibleTransition(L1Cache_State_I, L1Cache_Event_Other_GETI_NC);
    possibleTransition(L1Cache_State_IS_D, L1Cache_Event_Other_GETM_C);
    possibleTransition(L1Cache_State_IS_DI, L1Cache_Event_Other_GETM_C);
    possibleTransition(L1Cache_State_IS_D, L1Cache_Event_Other_GETS_C);
    possibleTransition(L1Cache_State_IS_D, L1Cache_Event_Other_GETS_NC);
    possibleTransition(L1Cache_State_IS_D, L1Cache_Event_Other_GETI_C);
    possibleTransition(L1Cache_State_IS_D, L1Cache_Event_Other_GETI_NC);
    possibleTransition(L1Cache_State_IS_D, L1Cache_Event_Other_GETM_NC);
    possibleTransition(L1Cache_State_IS_D, L1Cache_Event_Data);
    possibleTransition(L1Cache_State_IS_DI, L1Cache_Event_Data);
    possibleTransition(L1Cache_State_IS_DI, L1Cache_Event_Other_GETS_C);
    possibleTransition(L1Cache_State_IS_DI, L1Cache_Event_Other_GETS_NC);
    possibleTransition(L1Cache_State_IS_DI, L1Cache_Event_Other_GETM_NC);
    possibleTransition(L1Cache_State_IS_DI, L1Cache_Event_Other_GETI_C);
    possibleTransition(L1Cache_State_IS_DI, L1Cache_Event_Other_GETI_NC);
    possibleTransition(L1Cache_State_S, L1Cache_Event_Load);
    possibleTransition(L1Cache_State_S_TI, L1Cache_Event_Load);
    possibleTransition(L1Cache_State_SI_A, L1Cache_Event_Load);
    possibleTransition(L1Cache_State_S_MA, L1Cache_Event_Load);
    possibleTransition(L1Cache_State_SI, L1Cache_Event_Load);
    possibleTransition(L1Cache_State_SI, L1Cache_Event_Ifetch);
    possibleTransition(L1Cache_State_S, L1Cache_Event_Ifetch);
    possibleTransition(L1Cache_State_SI_A, L1Cache_Event_Ifetch);
    possibleTransition(L1Cache_State_S_MA, L1Cache_Event_Ifetch);
    possibleTransition(L1Cache_State_S_TI, L1Cache_Event_Ifetch);
    possibleTransition(L1Cache_State_M, L1Cache_Event_Ifetch);
    possibleTransition(L1Cache_State_M_TI, L1Cache_Event_Ifetch);
    possibleTransition(L1Cache_State_S, L1Cache_Event_Other_GETM_NC);
    possibleTransition(L1Cache_State_S, L1Cache_Event_Other_GETM_C);
    possibleTransition(L1Cache_State_S, L1Cache_Event_Other_GETS_C);
    possibleTransition(L1Cache_State_S, L1Cache_Event_Other_GETS_NC);
    possibleTransition(L1Cache_State_S, L1Cache_Event_Other_GETI_C);
    possibleTransition(L1Cache_State_S, L1Cache_Event_Other_GETI_NC);
    possibleTransition(L1Cache_State_S_TM, L1Cache_Event_Other_GETS_C);
    possibleTransition(L1Cache_State_S_TM, L1Cache_Event_Other_GETS_NC);
    possibleTransition(L1Cache_State_S_TM, L1Cache_Event_Other_GETI_C);
    possibleTransition(L1Cache_State_S_TM, L1Cache_Event_Other_GETI_NC);
    possibleTransition(L1Cache_State_S_TM, L1Cache_Event_Other_GETM_C);
    possibleTransition(L1Cache_State_S_TM, L1Cache_Event_Other_GETM_NC);
    possibleTransition(L1Cache_State_S_TI, L1Cache_Event_Timeout_NC);
    possibleTransition(L1Cache_State_S_TI, L1Cache_Event_Timeout_CC);
    possibleTransition(L1Cache_State_S_TI, L1Cache_Event_Timeout_NN);
    possibleTransition(L1Cache_State_S_TI, L1Cache_Event_Timeout_CN);
    possibleTransition(L1Cache_State_SI, L1Cache_Event_ALLINV);
    possibleTransition(L1Cache_State_SI_A, L1Cache_Event_Own_SELFINV_SPL);
    possibleTransition(L1Cache_State_SI, L1Cache_Event_Own_SELFINV_SPL);
    possibleTransition(L1Cache_State_SI_A, L1Cache_Event_Own_SELFINV);
    possibleTransition(L1Cache_State_SI_A, L1Cache_Event_Other_SELFINV);
    possibleTransition(L1Cache_State_S_TI, L1Cache_Event_Other_SELFINV);
    possibleTransition(L1Cache_State_S, L1Cache_Event_Other_SELFINV);
    possibleTransition(L1Cache_State_I, L1Cache_Event_Other_SELFINV);
    possibleTransition(L1Cache_State_IM_AD, L1Cache_Event_Other_SELFINV);
    possibleTransition(L1Cache_State_IM_D, L1Cache_Event_Other_SELFINV);
    possibleTransition(L1Cache_State_SI, L1Cache_Event_Other_SELFINV);
    possibleTransition(L1Cache_State_S_TM, L1Cache_Event_Other_SELFINV);
    possibleTransition(L1Cache_State_IS_D, L1Cache_Event_Other_SELFINV);
    possibleTransition(L1Cache_State_IS_AD, L1Cache_Event_Other_SELFINV);
    possibleTransition(L1Cache_State_IM_DI, L1Cache_Event_Other_SELFINV);
    possibleTransition(L1Cache_State_IS_DI, L1Cache_Event_Other_SELFINV);
    possibleTransition(L1Cache_State_M, L1Cache_Event_Other_SELFINV);
    possibleTransition(L1Cache_State_M_TI, L1Cache_Event_Other_SELFINV);
    possibleTransition(L1Cache_State_MI_A, L1Cache_Event_Other_SELFINV);
    possibleTransition(L1Cache_State_MI_R, L1Cache_Event_Other_SELFINV);
    possibleTransition(L1Cache_State_IM_AD, L1Cache_Event_ALLINV);
    possibleTransition(L1Cache_State_IM_D, L1Cache_Event_ALLINV);
    possibleTransition(L1Cache_State_I, L1Cache_Event_ALLINV);
    possibleTransition(L1Cache_State_IS_AD, L1Cache_Event_ALLINV);
    possibleTransition(L1Cache_State_IS_D, L1Cache_Event_ALLINV);
    possibleTransition(L1Cache_State_IM_DI, L1Cache_Event_ALLINV);
    possibleTransition(L1Cache_State_IS_DI, L1Cache_Event_ALLINV);
    possibleTransition(L1Cache_State_IM_AD, L1Cache_Event_Own_SELFINV);
    possibleTransition(L1Cache_State_IM_AD, L1Cache_Event_Own_SELFINV_SPL);
    possibleTransition(L1Cache_State_IM_D, L1Cache_Event_Own_SELFINV);
    possibleTransition(L1Cache_State_IM_D, L1Cache_Event_Own_SELFINV_SPL);
    possibleTransition(L1Cache_State_I, L1Cache_Event_Own_SELFINV);
    possibleTransition(L1Cache_State_I, L1Cache_Event_Own_SELFINV_SPL);
    possibleTransition(L1Cache_State_IS_AD, L1Cache_Event_Own_SELFINV);
    possibleTransition(L1Cache_State_IS_AD, L1Cache_Event_Own_SELFINV_SPL);
    possibleTransition(L1Cache_State_IS_D, L1Cache_Event_Own_SELFINV);
    possibleTransition(L1Cache_State_IS_D, L1Cache_Event_Own_SELFINV_SPL);
    possibleTransition(L1Cache_State_IM_DI, L1Cache_Event_Own_SELFINV);
    possibleTransition(L1Cache_State_IM_DI, L1Cache_Event_Own_SELFINV_SPL);
    possibleTransition(L1Cache_State_S_TI, L1Cache_Event_Timeout_noinv);
    possibleTransition(L1Cache_State_S_TI, L1Cache_Event_Timeout_noinv_noncric);
    possibleTransition(L1Cache_State_S, L1Cache_Event_Timeout_NC);
    possibleTransition(L1Cache_State_S, L1Cache_Event_Timeout_CC);
    possibleTransition(L1Cache_State_S, L1Cache_Event_Timeout_NN);
    possibleTransition(L1Cache_State_S, L1Cache_Event_Timeout_CN);
    possibleTransition(L1Cache_State_S, L1Cache_Event_Store);
    possibleTransition(L1Cache_State_S, L1Cache_Event_RMW_Read);
    possibleTransition(L1Cache_State_S, L1Cache_Event_RMW_Write);
    possibleTransition(L1Cache_State_S_TI, L1Cache_Event_Store);
    possibleTransition(L1Cache_State_S_TI, L1Cache_Event_RMW_Read);
    possibleTransition(L1Cache_State_S_TI, L1Cache_Event_RMW_Write);
    possibleTransition(L1Cache_State_SI_A, L1Cache_Event_Store);
    possibleTransition(L1Cache_State_SI_A, L1Cache_Event_RMW_Read);
    possibleTransition(L1Cache_State_SI_A, L1Cache_Event_RMW_Write);
    possibleTransition(L1Cache_State_SI, L1Cache_Event_Store);
    possibleTransition(L1Cache_State_SI, L1Cache_Event_RMW_Read);
    possibleTransition(L1Cache_State_SI, L1Cache_Event_RMW_Write);
    possibleTransition(L1Cache_State_S_TM, L1Cache_Event_Timeout_NC);
    possibleTransition(L1Cache_State_S_TM, L1Cache_Event_Timeout_CC);
    possibleTransition(L1Cache_State_S_TM, L1Cache_Event_Timeout_NN);
    possibleTransition(L1Cache_State_S_TM, L1Cache_Event_Timeout_CN);
    possibleTransition(L1Cache_State_S_MA, L1Cache_Event_ALLINV);
    possibleTransition(L1Cache_State_S_MA, L1Cache_Event_OWN_GETM);
    possibleTransition(L1Cache_State_S_MA, L1Cache_Event_Own_SELFINV);
    possibleTransition(L1Cache_State_S_MA, L1Cache_Event_Own_SELFINV_SPL);
    possibleTransition(L1Cache_State_S_MA, L1Cache_Event_Other_SELFINV);
    possibleTransition(L1Cache_State_S_MA, L1Cache_Event_Other_GETM_C);
    possibleTransition(L1Cache_State_S_MA, L1Cache_Event_Other_GETM_NC);
    possibleTransition(L1Cache_State_S_MA, L1Cache_Event_Other_GETS_NC);
    possibleTransition(L1Cache_State_S_MA, L1Cache_Event_Other_GETI_NC);
    possibleTransition(L1Cache_State_S_MA, L1Cache_Event_Other_GETS_C);
    possibleTransition(L1Cache_State_S_MA, L1Cache_Event_Other_GETI_C);
    possibleTransition(L1Cache_State_I, L1Cache_Event_Store);
    possibleTransition(L1Cache_State_I, L1Cache_Event_RMW_Read);
    possibleTransition(L1Cache_State_I, L1Cache_Event_RMW_Write);
    possibleTransition(L1Cache_State_IM_AD, L1Cache_Event_OWN_GETM);
    possibleTransition(L1Cache_State_IM_D, L1Cache_Event_Other_GETM_C);
    possibleTransition(L1Cache_State_IM_D, L1Cache_Event_Other_GETS_C);
    possibleTransition(L1Cache_State_IM_D, L1Cache_Event_Other_GETI_C);
    possibleTransition(L1Cache_State_IM_DI, L1Cache_Event_Other_GETM_C);
    possibleTransition(L1Cache_State_IM_DI, L1Cache_Event_Other_GETS_C);
    possibleTransition(L1Cache_State_IM_DI, L1Cache_Event_Other_GETI_C);
    possibleTransition(L1Cache_State_IM_D, L1Cache_Event_Other_GETM_NC);
    possibleTransition(L1Cache_State_IM_D, L1Cache_Event_Other_GETS_NC);
    possibleTransition(L1Cache_State_IM_D, L1Cache_Event_Other_GETI_NC);
    possibleTransition(L1Cache_State_IM_DI, L1Cache_Event_Other_GETM_NC);
    possibleTransition(L1Cache_State_IM_DI, L1Cache_Event_Other_GETS_NC);
    possibleTransition(L1Cache_State_IM_DI, L1Cache_Event_Other_GETI_NC);
    possibleTransition(L1Cache_State_IM_D, L1Cache_Event_Data);
    possibleTransition(L1Cache_State_IM_DI, L1Cache_Event_Data);
    possibleTransition(L1Cache_State_M, L1Cache_Event_Load);
    possibleTransition(L1Cache_State_M_TI, L1Cache_Event_Load);
    possibleTransition(L1Cache_State_MI_A, L1Cache_Event_Load);
    possibleTransition(L1Cache_State_MI_R, L1Cache_Event_Load);
    possibleTransition(L1Cache_State_M, L1Cache_Event_Store);
    possibleTransition(L1Cache_State_M_TI, L1Cache_Event_Store);
    possibleTransition(L1Cache_State_MI_A, L1Cache_Event_Store);
    possibleTransition(L1Cache_State_MI_R, L1Cache_Event_Store);
    possibleTransition(L1Cache_State_M, L1Cache_Event_RMW_Read);
    possibleTransition(L1Cache_State_M, L1Cache_Event_RMW_Write);
    possibleTransition(L1Cache_State_M_TI, L1Cache_Event_RMW_Read);
    possibleTransition(L1Cache_State_M_TI, L1Cache_Event_RMW_Write);
    possibleTransition(L1Cache_State_MI_A, L1Cache_Event_RMW_Read);
    possibleTransition(L1Cache_State_MI_A, L1Cache_Event_RMW_Write);
    possibleTransition(L1Cache_State_MI_R, L1Cache_Event_RMW_Read);
    possibleTransition(L1Cache_State_MI_R, L1Cache_Event_RMW_Write);
    possibleTransition(L1Cache_State_M, L1Cache_Event_Other_GETM_NC);
    possibleTransition(L1Cache_State_M, L1Cache_Event_Other_GETS_NC);
    possibleTransition(L1Cache_State_M, L1Cache_Event_Other_GETI_NC);
    possibleTransition(L1Cache_State_M, L1Cache_Event_Other_GETM_C);
    possibleTransition(L1Cache_State_M, L1Cache_Event_Other_GETS_C);
    possibleTransition(L1Cache_State_M, L1Cache_Event_Other_GETI_C);
    possibleTransition(L1Cache_State_M_TI, L1Cache_Event_Other_GETM_NC);
    possibleTransition(L1Cache_State_M_TI, L1Cache_Event_Other_GETM_C);
    possibleTransition(L1Cache_State_M_TI, L1Cache_Event_Other_GETS_C);
    possibleTransition(L1Cache_State_M_TI, L1Cache_Event_Other_GETS_NC);
    possibleTransition(L1Cache_State_M_TI, L1Cache_Event_Other_GETI_C);
    possibleTransition(L1Cache_State_M_TI, L1Cache_Event_Other_GETI_NC);
    possibleTransition(L1Cache_State_M, L1Cache_Event_Timeout_NC);
    possibleTransition(L1Cache_State_M, L1Cache_Event_Timeout_CC);
    possibleTransition(L1Cache_State_M, L1Cache_Event_Timeout_NN);
    possibleTransition(L1Cache_State_M, L1Cache_Event_Timeout_CN);
    possibleTransition(L1Cache_State_MI_R, L1Cache_Event_OWN_PUTM);
    possibleTransition(L1Cache_State_MI_A, L1Cache_Event_OWN_PUTM);
    possibleTransition(L1Cache_State_IS_AD, L1Cache_Event_OWN_PUTM);
    possibleTransition(L1Cache_State_IS_AD, L1Cache_Event_Other_PUTM);
    possibleTransition(L1Cache_State_IM_AD, L1Cache_Event_OWN_PUTM);
    possibleTransition(L1Cache_State_IM_AD, L1Cache_Event_Other_PUTM);
    possibleTransition(L1Cache_State_I, L1Cache_Event_OWN_PUTM);
    possibleTransition(L1Cache_State_I, L1Cache_Event_Other_PUTM);
    possibleTransition(L1Cache_State_IM_DI, L1Cache_Event_OWN_PUTM);
    possibleTransition(L1Cache_State_IM_DI, L1Cache_Event_Other_PUTM);
    possibleTransition(L1Cache_State_IS_DI, L1Cache_Event_OWN_PUTM);
    possibleTransition(L1Cache_State_IS_DI, L1Cache_Event_Other_PUTM);
    possibleTransition(L1Cache_State_IS_D, L1Cache_Event_OWN_PUTM);
    possibleTransition(L1Cache_State_IS_D, L1Cache_Event_Other_PUTM);
    possibleTransition(L1Cache_State_IM_D, L1Cache_Event_OWN_PUTM);
    possibleTransition(L1Cache_State_IM_D, L1Cache_Event_Other_PUTM);
    possibleTransition(L1Cache_State_S, L1Cache_Event_OWN_PUTM);
    possibleTransition(L1Cache_State_S, L1Cache_Event_Other_PUTM);
    possibleTransition(L1Cache_State_S_TI, L1Cache_Event_OWN_PUTM);
    possibleTransition(L1Cache_State_S_TI, L1Cache_Event_Other_PUTM);
    possibleTransition(L1Cache_State_M, L1Cache_Event_OWN_PUTM);
    possibleTransition(L1Cache_State_M, L1Cache_Event_Other_PUTM);
    possibleTransition(L1Cache_State_M_TI, L1Cache_Event_OWN_PUTM);
    possibleTransition(L1Cache_State_M_TI, L1Cache_Event_Other_PUTM);
    possibleTransition(L1Cache_State_S_TM, L1Cache_Event_Other_PUTM);
    possibleTransition(L1Cache_State_S_MA, L1Cache_Event_Other_PUTM);
    possibleTransition(L1Cache_State_MI_R, L1Cache_Event_Other_PUTM);
    possibleTransition(L1Cache_State_MI_A, L1Cache_Event_Other_PUTM);
    possibleTransition(L1Cache_State_M, L1Cache_Event_INV);
    possibleTransition(L1Cache_State_I, L1Cache_Event_INV);
    possibleTransition(L1Cache_State_IS_AD, L1Cache_Event_INV);
    possibleTransition(L1Cache_State_IS_D, L1Cache_Event_INV);
    possibleTransition(L1Cache_State_IS_DI, L1Cache_Event_INV);
    possibleTransition(L1Cache_State_IM_AD, L1Cache_Event_INV);
    possibleTransition(L1Cache_State_IM_D, L1Cache_Event_INV);
    possibleTransition(L1Cache_State_IM_DI, L1Cache_Event_INV);
    possibleTransition(L1Cache_State_MI_A, L1Cache_Event_INV);
    possibleTransition(L1Cache_State_MI_R, L1Cache_Event_INV);
    possibleTransition(L1Cache_State_S, L1Cache_Event_INV);
    possibleTransition(L1Cache_State_S, L1Cache_Event_Replacement);
    possibleTransition(L1Cache_State_SI_A, L1Cache_Event_INV);
    possibleTransition(L1Cache_State_SI_A, L1Cache_Event_Replacement);
    possibleTransition(L1Cache_State_SI, L1Cache_Event_INV);
    possibleTransition(L1Cache_State_SI, L1Cache_Event_Replacement);
    possibleTransition(L1Cache_State_S_TI, L1Cache_Event_INV);
    possibleTransition(L1Cache_State_S_TI, L1Cache_Event_Replacement);
    possibleTransition(L1Cache_State_M, L1Cache_Event_Replacement);
    possibleTransition(L1Cache_State_M_TI, L1Cache_Event_Replacement);
    possibleTransition(L1Cache_State_IS_AD, L1Cache_Event_Load);
    possibleTransition(L1Cache_State_IS_AD, L1Cache_Event_Ifetch);
    possibleTransition(L1Cache_State_IS_AD, L1Cache_Event_Store);
    possibleTransition(L1Cache_State_IS_AD, L1Cache_Event_Replacement);
    possibleTransition(L1Cache_State_IS_AD, L1Cache_Event_RMW_Read);
    possibleTransition(L1Cache_State_IS_AD, L1Cache_Event_RMW_Write);
    possibleTransition(L1Cache_State_IS_D, L1Cache_Event_Load);
    possibleTransition(L1Cache_State_IS_D, L1Cache_Event_Ifetch);
    possibleTransition(L1Cache_State_IS_D, L1Cache_Event_Store);
    possibleTransition(L1Cache_State_IS_D, L1Cache_Event_Replacement);
    possibleTransition(L1Cache_State_IS_D, L1Cache_Event_RMW_Read);
    possibleTransition(L1Cache_State_IS_D, L1Cache_Event_RMW_Write);
    possibleTransition(L1Cache_State_IS_DI, L1Cache_Event_Load);
    possibleTransition(L1Cache_State_IS_DI, L1Cache_Event_Ifetch);
    possibleTransition(L1Cache_State_IS_DI, L1Cache_Event_Store);
    possibleTransition(L1Cache_State_IS_DI, L1Cache_Event_Replacement);
    possibleTransition(L1Cache_State_IS_DI, L1Cache_Event_RMW_Read);
    possibleTransition(L1Cache_State_IS_DI, L1Cache_Event_RMW_Write);
    possibleTransition(L1Cache_State_IM_AD, L1Cache_Event_Load);
    possibleTransition(L1Cache_State_IM_AD, L1Cache_Event_Ifetch);
    possibleTransition(L1Cache_State_IM_AD, L1Cache_Event_Store);
    possibleTransition(L1Cache_State_IM_AD, L1Cache_Event_Replacement);
    possibleTransition(L1Cache_State_IM_AD, L1Cache_Event_RMW_Read);
    possibleTransition(L1Cache_State_IM_AD, L1Cache_Event_RMW_Write);
    possibleTransition(L1Cache_State_IM_D, L1Cache_Event_Load);
    possibleTransition(L1Cache_State_IM_D, L1Cache_Event_Ifetch);
    possibleTransition(L1Cache_State_IM_D, L1Cache_Event_Store);
    possibleTransition(L1Cache_State_IM_D, L1Cache_Event_Replacement);
    possibleTransition(L1Cache_State_IM_D, L1Cache_Event_RMW_Read);
    possibleTransition(L1Cache_State_IM_D, L1Cache_Event_RMW_Write);
    possibleTransition(L1Cache_State_IM_DI, L1Cache_Event_Load);
    possibleTransition(L1Cache_State_IM_DI, L1Cache_Event_Ifetch);
    possibleTransition(L1Cache_State_IM_DI, L1Cache_Event_Store);
    possibleTransition(L1Cache_State_IM_DI, L1Cache_Event_Replacement);
    possibleTransition(L1Cache_State_IM_DI, L1Cache_Event_RMW_Read);
    possibleTransition(L1Cache_State_IM_DI, L1Cache_Event_RMW_Write);
    possibleTransition(L1Cache_State_MI_R, L1Cache_Event_Replacement);
    possibleTransition(L1Cache_State_M_TI, L1Cache_Event_Timeout_NC);
    possibleTransition(L1Cache_State_M_TI, L1Cache_Event_Timeout_CC);
    possibleTransition(L1Cache_State_M_TI, L1Cache_Event_Timeout_NN);
    possibleTransition(L1Cache_State_M_TI, L1Cache_Event_Timeout_CN);
    possibleTransition(L1Cache_State_M_TI, L1Cache_Event_Timeout_noinv);
    possibleTransition(L1Cache_State_M_TI, L1Cache_Event_Timeout_noinv_noncric);
    possibleTransition(L1Cache_State_MI_A, L1Cache_Event_Other_GETM_NC);
    possibleTransition(L1Cache_State_MI_A, L1Cache_Event_Other_GETS_NC);
    possibleTransition(L1Cache_State_MI_A, L1Cache_Event_Other_GETI_NC);
    possibleTransition(L1Cache_State_MI_A, L1Cache_Event_Other_GETM_C);
    possibleTransition(L1Cache_State_MI_A, L1Cache_Event_Other_GETS_C);
    possibleTransition(L1Cache_State_MI_A, L1Cache_Event_Other_GETI_C);
    possibleTransition(L1Cache_State_SI_A, L1Cache_Event_Other_GETS_NC);
    possibleTransition(L1Cache_State_SI_A, L1Cache_Event_Other_GETI_NC);
    possibleTransition(L1Cache_State_SI_A, L1Cache_Event_Other_GETS_C);
    possibleTransition(L1Cache_State_SI_A, L1Cache_Event_Other_GETI_C);
    possibleTransition(L1Cache_State_SI, L1Cache_Event_Other_GETM_C);
    possibleTransition(L1Cache_State_SI, L1Cache_Event_Other_GETM_NC);
    possibleTransition(L1Cache_State_SI, L1Cache_Event_Other_GETS_NC);
    possibleTransition(L1Cache_State_SI, L1Cache_Event_Other_GETI_NC);
    possibleTransition(L1Cache_State_SI, L1Cache_Event_Other_GETS_C);
    possibleTransition(L1Cache_State_SI, L1Cache_Event_Other_GETI_C);
    possibleTransition(L1Cache_State_SI_A, L1Cache_Event_Other_GETM_C);
    possibleTransition(L1Cache_State_SI_A, L1Cache_Event_Other_GETM_NC);
    possibleTransition(L1Cache_State_MI_R, L1Cache_Event_Other_GETM_C);
    possibleTransition(L1Cache_State_MI_R, L1Cache_Event_Other_GETS_C);
    possibleTransition(L1Cache_State_MI_R, L1Cache_Event_Other_GETI_C);
    possibleTransition(L1Cache_State_MI_R, L1Cache_Event_Other_GETM_NC);
    possibleTransition(L1Cache_State_MI_R, L1Cache_Event_Other_GETS_NC);
    possibleTransition(L1Cache_State_MI_R, L1Cache_Event_Other_GETI_NC);
    possibleTransition(L1Cache_State_MI_A, L1Cache_Event_Own_SENDDATA);
    possibleTransition(L1Cache_State_MI_R, L1Cache_Event_Own_SENDDATA);
    possibleTransition(L1Cache_State_MI_A, L1Cache_Event_Replacement);
    possibleTransition(L1Cache_State_I, L1Cache_Event_Other_SENDDATA);
    possibleTransition(L1Cache_State_IM_D, L1Cache_Event_Other_SENDDATA);
    possibleTransition(L1Cache_State_IM_DI, L1Cache_Event_Other_SENDDATA);
    possibleTransition(L1Cache_State_IS_D, L1Cache_Event_Other_SENDDATA);
    possibleTransition(L1Cache_State_IS_DI, L1Cache_Event_Other_SENDDATA);
    possibleTransition(L1Cache_State_IM_AD, L1Cache_Event_Other_SENDDATA);
    possibleTransition(L1Cache_State_IS_AD, L1Cache_Event_Other_SENDDATA);
    AbstractController::init();
    resetStats();
}

void
L1Cache_Controller::regStats()
{
    AbstractController::regStats();

    if (m_version == 0) {
        for (L1Cache_Event event = L1Cache_Event_FIRST;
             event < L1Cache_Event_NUM; ++event) {
            Stats::Vector *t = new Stats::Vector();
            t->init(m_num_controllers);
            t->name(g_system_ptr->name() + ".L1Cache_Controller." +
                L1Cache_Event_to_string(event));
            t->flags(Stats::pdf | Stats::total | Stats::oneline |
                     Stats::nozero);

            eventVec.push_back(t);
        }

        for (L1Cache_State state = L1Cache_State_FIRST;
             state < L1Cache_State_NUM; ++state) {

            transVec.push_back(std::vector<Stats::Vector *>());

            for (L1Cache_Event event = L1Cache_Event_FIRST;
                 event < L1Cache_Event_NUM; ++event) {

                Stats::Vector *t = new Stats::Vector();
                t->init(m_num_controllers);
                t->name(g_system_ptr->name() + ".L1Cache_Controller." +
                        L1Cache_State_to_string(state) +
                        "." + L1Cache_Event_to_string(event));

                t->flags(Stats::pdf | Stats::total | Stats::oneline |
                         Stats::nozero);
                transVec[state].push_back(t);
            }
        }
    }
}

void
L1Cache_Controller::collateStats()
{
    for (L1Cache_Event event = L1Cache_Event_FIRST;
         event < L1Cache_Event_NUM; ++event) {
        for (unsigned int i = 0; i < m_num_controllers; ++i) {
            std::map<uint32_t, AbstractController *>::iterator it =
                                g_abs_controls[MachineType_L1Cache].find(i);
            assert(it != g_abs_controls[MachineType_L1Cache].end());
            (*eventVec[event])[i] =
                ((L1Cache_Controller *)(*it).second)->getEventCount(event);
        }
    }

    for (L1Cache_State state = L1Cache_State_FIRST;
         state < L1Cache_State_NUM; ++state) {

        for (L1Cache_Event event = L1Cache_Event_FIRST;
             event < L1Cache_Event_NUM; ++event) {

            for (unsigned int i = 0; i < m_num_controllers; ++i) {
                std::map<uint32_t, AbstractController *>::iterator it =
                                g_abs_controls[MachineType_L1Cache].find(i);
                assert(it != g_abs_controls[MachineType_L1Cache].end());
                (*transVec[state][event])[i] =
                    ((L1Cache_Controller *)(*it).second)->getTransitionCount(state, event);
            }
        }
    }
}

void
L1Cache_Controller::countTransition(L1Cache_State state, L1Cache_Event event)
{
    assert(m_possible[state][event]);
    m_counters[state][event]++;
    m_event_counters[event]++;
}
void
L1Cache_Controller::possibleTransition(L1Cache_State state,
                             L1Cache_Event event)
{
    m_possible[state][event] = true;
}

uint64
L1Cache_Controller::getEventCount(L1Cache_Event event)
{
    return m_event_counters[event];
}

bool
L1Cache_Controller::isPossible(L1Cache_State state, L1Cache_Event event)
{
    return m_possible[state][event];
}

uint64
L1Cache_Controller::getTransitionCount(L1Cache_State state,
                             L1Cache_Event event)
{
    return m_counters[state][event];
}

int
L1Cache_Controller::getNumControllers()
{
    return m_num_controllers;
}

MessageBuffer*
L1Cache_Controller::getMandatoryQueue() const
{
    return m_mandatoryQueue_ptr;
}

Sequencer*
L1Cache_Controller::getSequencer() const
{
    return m_sequencer_ptr;
}

void
L1Cache_Controller::print(ostream& out) const
{
    out << "[L1Cache_Controller " << m_version << "]";
}

void L1Cache_Controller::resetStats()
{
    for (int state = 0; state < L1Cache_State_NUM; state++) {
        for (int event = 0; event < L1Cache_Event_NUM; event++) {
            m_counters[state][event] = 0;
        }
    }

    for (int event = 0; event < L1Cache_Event_NUM; event++) {
        m_event_counters[event] = 0;
    }

    AbstractController::resetStats();
}

// Set and Reset for cache_entry variable
void
L1Cache_Controller::set_cache_entry(L1Cache_Entry*& m_cache_entry_ptr, AbstractCacheEntry* m_new_cache_entry)
{
  m_cache_entry_ptr = (L1Cache_Entry*)m_new_cache_entry;
}

void
L1Cache_Controller::unset_cache_entry(L1Cache_Entry*& m_cache_entry_ptr)
{
  m_cache_entry_ptr = 0;
}

//cache_approx
// Set and Reset for svc_entry variable
void
L1Cache_Controller::set_svc_entry(L1Cache_Entry*& m_cache_entry_ptr, AbstractCacheEntry* m_new_cache_entry)
{
  m_cache_entry_ptr = (L1Cache_Entry*)m_new_cache_entry;
}

void
L1Cache_Controller::unset_svc_entry(L1Cache_Entry*& m_cache_entry_ptr)
{
  m_cache_entry_ptr = 0;
}


// Set and Reset for tbe variable
void
L1Cache_Controller::set_tbe(L1Cache_TBE*& m_tbe_ptr, L1Cache_TBE* m_new_tbe)
{
  m_tbe_ptr = m_new_tbe;
}

void
L1Cache_Controller::unset_tbe(L1Cache_TBE*& m_tbe_ptr)
{
  m_tbe_ptr = NULL;
}

void
L1Cache_Controller::recordCacheTrace(int cntrl, CacheRecorder* tr)
{
    m_Icache_ptr->recordCacheContents(cntrl, tr);
    m_Dcache_ptr->recordCacheContents(cntrl, tr);
}

// Actions
/** \brief Issue GETS */
void
L1Cache_Controller::as_issueGETSMem(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing as_issueGETSMem on address %s\n", addr);
    {
    std::shared_ptr<RequestMsg> out_msg = std::make_shared<RequestMsg>(clockEdge());
    (*out_msg).m_Addr = addr;
    (*out_msg).m_Type = CoherenceRequestType_GETS;
    (*out_msg).m_Requestor = m_machineID;
    (((*out_msg).m_Destination).broadcast(MachineType_L1Cache));
    (((*out_msg).m_Destination).add((map_Address_to_Directory(addr))));
    (*out_msg).m_MessageSize = MessageSizeType_Control;
    ((*m_requestFromCache_ptr)).enqueue(out_msg, Cycles(m_l1_request_latency));
}

}

/** \brief Issue GETINSTR */
void
L1Cache_Controller::ai_issueGETINSTR(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing ai_issueGETINSTR on address %s\n", addr);
    {
    std::shared_ptr<RequestMsg> out_msg = std::make_shared<RequestMsg>(clockEdge());
    (*out_msg).m_Addr = addr;
    (*out_msg).m_Type = CoherenceRequestType_GETI;
    (*out_msg).m_Requestor = m_machineID;
    (((*out_msg).m_Destination).broadcast(MachineType_L1Cache));
    (((*out_msg).m_Destination).add((map_Address_to_Directory(addr))));
    (*out_msg).m_MessageSize = MessageSizeType_Control;
    ((*m_requestFromCache_ptr)).enqueue(out_msg, Cycles(m_l1_request_latency));
}

}

/** \brief Issue GETM */
void
L1Cache_Controller::bm_issueGETM(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing bm_issueGETM on address %s\n", addr);
    {
    std::shared_ptr<RequestMsg> out_msg = std::make_shared<RequestMsg>(clockEdge());
    (*out_msg).m_Addr = addr;
    (*out_msg).m_Type = CoherenceRequestType_GETM;
    (*out_msg).m_Requestor = m_machineID;
    (((*out_msg).m_Destination).broadcast(MachineType_L1Cache));
    (((*out_msg).m_Destination).add((map_Address_to_Directory(addr))));
    (*out_msg).m_MessageSize = MessageSizeType_Control;
    ((*m_requestFromCache_ptr)).enqueue(out_msg, Cycles(m_l1_request_latency));
}

}

/** \brief Issue inv when latest */
void
L1Cache_Controller::issueInv(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing issueInv on address %s\n", addr);
    {
    std::shared_ptr<RequestMsg> out_msg = std::make_shared<RequestMsg>(clockEdge());
    (*out_msg).m_Addr = addr;
    (*out_msg).m_Type = CoherenceRequestType_SELFINV;
    (*out_msg).m_Requestor = m_machineID;
    (((*out_msg).m_Destination).broadcast(MachineType_L1Cache));
    (((*out_msg).m_Destination).add((map_Address_to_Directory(addr))));
    (*out_msg).m_MessageSize = MessageSizeType_Control;
    (((*out_msg).m_DataDestination).addNetDest((*m_cache_entry_ptr).m_finalDestinationSet_NC));
    (((*out_msg).m_DataDestination).addNetDest((*m_cache_entry_ptr).m_finalDestinationSet_C));
    ((*m_requestFromCacheWB_ptr)).enqueue(out_msg, Cycles(m_l1_request_latency));
}

}

/** \brief f */
void
L1Cache_Controller::setownfinaldest(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing setownfinaldest on address %s\n", addr);
    (((*m_cache_entry_ptr).m_finalDestinationSet_NC).clear());
(((*m_cache_entry_ptr).m_finalDestinationSet_C).clear());
int numproc_c
 = (getnumproc());
    if (((IDToInt(((m_machineID).getNum()))) >= numproc_c)) {
        (((*m_cache_entry_ptr).m_finalDestinationSet_NC).add(m_machineID));
    } else {
            if (((IDToInt(((m_machineID).getNum()))) < numproc_c)) {
                (((*m_cache_entry_ptr).m_finalDestinationSet_C).add(m_machineID));
            }
        }

}

/** \brief f */
void
L1Cache_Controller::setDestmir(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing setDestmir on address %s\n", addr);
    (((*m_cache_entry_ptr).m_finalDestinationSet_NC).clear());
(((*m_cache_entry_ptr).m_finalDestinationSet_C).clear());
int numproc_c
 = (getnumproc());
{
    // Declare message
    const RequestMsg* in_msg_ptr M5_VAR_USED;
    in_msg_ptr = dynamic_cast<const RequestMsg *>(((*m_requestToCache_ptr)).peek());
    assert(in_msg_ptr != NULL); // Check the cast result
    if (((IDToInt(((m_machineID).getNum()))) >= numproc_c)) {
        (((*m_cache_entry_ptr).m_finalDestinationSet_NC).add(((*in_msg_ptr)).m_Requestor));
            if ((((*in_msg_ptr)).m_Type == CoherenceRequestType_GETS)) {
                (((*m_cache_entry_ptr).m_finalDestinationSet_NC).add((map_Address_to_Directory(((*in_msg_ptr)).m_Addr))));
            }
        } else {
                if (((IDToInt(((m_machineID).getNum()))) < numproc_c)) {
                    (((*m_cache_entry_ptr).m_finalDestinationSet_C).add(((*in_msg_ptr)).m_Requestor));
                        if ((((*in_msg_ptr)).m_Type == CoherenceRequestType_GETS)) {
                            (((*m_cache_entry_ptr).m_finalDestinationSet_C).add((map_Address_to_Directory(((*in_msg_ptr)).m_Addr))));
                        }
                    }
                }
                }

}

/** \brief adding the destination before reissuing */
void
L1Cache_Controller::addDest(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing addDest on address %s\n", addr);
    int numproc_c
 = (getnumproc());
    if (((IDToInt(((m_machineID).getNum()))) < numproc_c)) {
        (((*m_cache_entry_ptr).m_finalDestinationSet_C).add(m_machineID));
    } else {
            if (((IDToInt(((m_machineID).getNum()))) >= numproc_c)) {
                (((*m_cache_entry_ptr).m_finalDestinationSet_NC).add(m_machineID));
            }
        }

}

/** \brief adding the destination before reissuing */
void
L1Cache_Controller::addReqDest(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing addReqDest on address %s\n", addr);
    {
    // Declare message
    const RequestMsg* in_msg_ptr M5_VAR_USED;
    in_msg_ptr = dynamic_cast<const RequestMsg *>(((*m_requestToCache_ptr)).peek());
    assert(in_msg_ptr != NULL); // Check the cast result
int numproc_c
 = (getnumproc());
    if (((IDToInt(((((*in_msg_ptr)).m_Requestor).getNum()))) < numproc_c)) {
        (((*m_cache_entry_ptr).m_finalDestinationSet_C).add(((*in_msg_ptr)).m_Requestor));
    } else {
            if (((IDToInt(((((*in_msg_ptr)).m_Requestor).getNum()))) >= numproc_c)) {
                (((*m_cache_entry_ptr).m_finalDestinationSet_NC).add(((*in_msg_ptr)).m_Requestor));
            }
        }
        }

}

/** \brief Issue PUTM */
void
L1Cache_Controller::bpm_issuePUTM(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing bpm_issuePUTM on address %s\n", addr);
    {
    std::shared_ptr<RequestMsg> out_msg = std::make_shared<RequestMsg>(clockEdge());
    (*out_msg).m_Addr = addr;
    (*out_msg).m_Type = CoherenceRequestType_PUTM;
    (*out_msg).m_Requestor = m_machineID;
    (((*out_msg).m_Destination).broadcast(MachineType_L1Cache));
    (((*out_msg).m_Destination).add((map_Address_to_Directory(addr))));
    (*out_msg).m_MessageSize = MessageSizeType_Control;
    (*out_msg).m_DataBlk = (*m_cache_entry_ptr).m_DataBlk;
    ((*m_requestFromCacheWB_ptr)).enqueue(out_msg, Cycles(m_l1_request_latency));
}

}

/** \brief Issue SENDDATA */
void
L1Cache_Controller::bpm_issuesendData(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing bpm_issuesendData on address %s\n", addr);
    {
    std::shared_ptr<RequestMsg> out_msg = std::make_shared<RequestMsg>(clockEdge());
    (*out_msg).m_Addr = addr;
    (*out_msg).m_Type = CoherenceRequestType_SENDDATA;
    (*out_msg).m_Requestor = m_machineID;
    (((*out_msg).m_Destination).broadcast(MachineType_L1Cache));
    (((*out_msg).m_Destination).add((map_Address_to_Directory(addr))));
    (*out_msg).m_MessageSize = MessageSizeType_Control;
    (*out_msg).m_DataBlk = (*m_cache_entry_ptr).m_DataBlk;
        if ((((*m_cache_entry_ptr).m_finalDestinationSet_C).isEmpty())) {
            (((*out_msg).m_DataDestination).addNetDest((*m_cache_entry_ptr).m_finalDestinationSet_NC));
        } else {
            (((*out_msg).m_DataDestination).addNetDest((*m_cache_entry_ptr).m_finalDestinationSet_C));
        }
        ((*m_requestFromCacheWB_ptr)).enqueue(out_msg, Cycles(m_l1_request_latency));
    }

}

/** \brief Send data from cache to directory */
void
L1Cache_Controller::cc_sendDataCacheToDir(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing cc_sendDataCacheToDir on address %s\n", addr);
    {
    std::shared_ptr<ResponseMsg> out_msg = std::make_shared<ResponseMsg>(clockEdge());
    #ifndef NDEBUG
    if (!((m_cache_entry_ptr != NULL))) {
        panic("Runtime Error at MSI_Snooping_Time_based_base-cache.sm:1133: %s.\n", "assert failure");

    }
    #endif
    ;
    (*out_msg).m_Addr = addr;
    (*out_msg).m_Type = CoherenceResponseType_DATA_TO_WB;
    (*out_msg).m_Sender = m_machineID;
    (((*out_msg).m_Destination).add((map_Address_to_Directory(addr))));
    (*out_msg).m_DataBlk = (*m_cache_entry_ptr).m_DataBlk;
    (*out_msg).m_MessageSize = MessageSizeType_Control;
    ((*m_responseFromCache_ptr)).enqueue(out_msg, Cycles(m_l1_response_latency));
}

}

/** \brief Send data from cache to cache */
void
L1Cache_Controller::cc_sendDataCacheToCache(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing cc_sendDataCacheToCache on address %s\n", addr);
    {
    // Declare message
    const RequestMsg* in_msg_ptr M5_VAR_USED;
    in_msg_ptr = dynamic_cast<const RequestMsg *>(((*m_requestToCacheWB_ptr)).peek());
    assert(in_msg_ptr != NULL); // Check the cast result
{
    std::shared_ptr<ResponseMsg> out_msg = std::make_shared<ResponseMsg>(clockEdge());
    #ifndef NDEBUG
    if (!((m_cache_entry_ptr != NULL))) {
        panic("Runtime Error at MSI_Snooping_Time_based_base-cache.sm:1147: %s.\n", "assert failure");

    }
    #endif
    ;
    (*out_msg).m_Addr = addr;
    (*out_msg).m_Type = CoherenceResponseType_DATA_TO_CACHE;
    (*out_msg).m_Sender = m_machineID;
    (((*out_msg).m_Destination).addNetDest(((*in_msg_ptr)).m_DataDestination));
    (*out_msg).m_DataBlk = (*m_cache_entry_ptr).m_DataBlk;
    (*out_msg).m_MessageSize = MessageSizeType_Control;
    (((*m_cache_entry_ptr).m_finalDestinationSet_NC).clear());
    (((*m_cache_entry_ptr).m_finalDestinationSet_C).clear());
    ((*m_responseFromCache_ptr)).enqueue(out_msg, Cycles(m_l1_response_latency));
}
}

}

/** \brief sends eviction information to the processor */
void
L1Cache_Controller::forward_eviction_to_cpu(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing forward_eviction_to_cpu on address %s\n", addr);
        if (m_send_evictions) {
        (((*m_sequencer_ptr)).evictionCallback(addr));
    }

}

/** \brief new data from mem, notify sequencer the load completed. */
void
L1Cache_Controller::r_load_hit(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing r_load_hit on address %s\n", addr);
    #ifndef NDEBUG
if (!((m_cache_entry_ptr != NULL))) {
    panic("Runtime Error at MSI_Snooping_Time_based_base-cache.sm:1178: %s.\n", "assert failure");

}
#endif
;
(((*m_sequencer_ptr)).readCallback(addr, (*m_cache_entry_ptr).m_DataBlk, (false)));

}

/** \brief data already present, notify sequencer the load completed. */
void
L1Cache_Controller::rx_load_hit(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing rx_load_hit on address %s\n", addr);
    #ifndef NDEBUG
if (!((m_cache_entry_ptr != NULL))) {
    panic("Runtime Error at MSI_Snooping_Time_based_base-cache.sm:1186: %s.\n", "assert failure");

}
#endif
;
(((*m_sequencer_ptr)).readCallback(addr, (*m_cache_entry_ptr).m_DataBlk, (true)));

}

/** \brief If not prefetch, notify sequencer that store completed. */
void
L1Cache_Controller::s_store_hit(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing s_store_hit on address %s\n", addr);
    #ifndef NDEBUG
if (!((m_cache_entry_ptr != NULL))) {
    panic("Runtime Error at MSI_Snooping_Time_based_base-cache.sm:1194: %s.\n", "assert failure");

}
#endif
;
(((*m_sequencer_ptr)).writeCallback(addr, (*m_cache_entry_ptr).m_DataBlk));
(*m_cache_entry_ptr).m_Dirty = (true);

}

/** \brief If not prefetch, notify sequencer that store completed. */
void
L1Cache_Controller::sx_store_hit(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing sx_store_hit on address %s\n", addr);
    #ifndef NDEBUG
if (!((m_cache_entry_ptr != NULL))) {
    panic("Runtime Error at MSI_Snooping_Time_based_base-cache.sm:1203: %s.\n", "assert failure");

}
#endif
;
(((*m_sequencer_ptr)).writeCallback(addr, (*m_cache_entry_ptr).m_DataBlk, (true)));
(*m_cache_entry_ptr).m_Dirty = (true);

}

/** \brief Allocate TBE (isPrefetch=0, number of invalidates=0) */
void
L1Cache_Controller::i_allocateTBE(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing i_allocateTBE on address %s\n", addr);
    #ifndef NDEBUG
if (!((m_cache_entry_ptr != NULL))) {
    panic("Runtime Error at MSI_Snooping_Time_based_base-cache.sm:1212: %s.\n", "assert failure");

}
#endif
;
(((*m_TBEs_ptr)).allocate(addr));
set_tbe(m_tbe_ptr, (((*m_TBEs_ptr)).lookup(addr)));;
(*m_tbe_ptr).m_Dirty = (*m_cache_entry_ptr).m_Dirty;
(*m_tbe_ptr).m_DataBlk = (*m_cache_entry_ptr).m_DataBlk;
(*m_tbe_ptr).m_isAtomic = (false);

}

/** \brief Pop mandatory queue. */
void
L1Cache_Controller::k_popMandatoryQueue(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing k_popMandatoryQueue on address %s\n", addr);
    (((*m_mandatoryQueue_ptr)).dequeue());

}

/** \brief Calculate latency profile of request */
void
L1Cache_Controller::calcLatencyProfile(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing calcLatencyProfile on address %s\n", addr);
    DPRINTF(RubySlicc, "MSI_Snooping_Time_based_base-cache.sm:1227: LATENCY PROFILE Addr: %s\n", addr);
DPRINTF(RubySlicc, "MSI_Snooping_Time_based_base-cache.sm:1228: START TIME: %s\n", (*m_tbe_ptr).m_startTime);
DPRINTF(RubySlicc, "MSI_Snooping_Time_based_base-cache.sm:1229: FIRST SCHEDULED TIME: %s\n", (*m_tbe_ptr).m_firstScheduledTime);
(*m_tbe_ptr).m_endTime = (((*m_responseToCache_ptr)).dequeue());
DPRINTF(RubySlicc, "MSI_Snooping_Time_based_base-cache.sm:1231: END TIME: %s\n", (*m_tbe_ptr).m_endTime);
Cycles time_S_to_M
 = (zero_time());
    if (((*m_tbe_ptr).m_startTime_SM == (zero_time()))) {
        time_S_to_M = (*m_tbe_ptr).m_startTime_SM;
    } else {
        time_S_to_M = ((*m_tbe_ptr).m_startTime_SM - (*m_tbe_ptr).m_startTime);
    }
    DPRINTF(RubySlicc, "MSI_Snooping_Time_based_base-cache.sm:1240: time_S_to_M: %s\n", time_S_to_M);
    (*m_tbe_ptr).m_arbitLatency = (((*m_tbe_ptr).m_firstScheduledTime - (*m_tbe_ptr).m_startTime) - time_S_to_M);
    (*m_tbe_ptr).m_interCoreCohLatency = ((getNearestStartSlot((*m_tbe_ptr).m_endTime)) - (*m_tbe_ptr).m_firstScheduledTime);
    (*m_tbe_ptr).m_intraCoreCohLatency = (((*m_tbe_ptr).m_endTime - (*m_tbe_ptr).m_startTime) - (((*m_tbe_ptr).m_interCoreCohLatency + (*m_tbe_ptr).m_arbitLatency) + time_S_to_M));
    DPRINTF(RubySlicc, "MSI_Snooping_Time_based_base-cache.sm:1248: ARB LAT: %s\n", (*m_tbe_ptr).m_arbitLatency);
    DPRINTF(RubySlicc, "MSI_Snooping_Time_based_base-cache.sm:1249: INTRA CORE LAT: %s\n", (*m_tbe_ptr).m_intraCoreCohLatency);
    DPRINTF(RubySlicc, "MSI_Snooping_Time_based_base-cache.sm:1250: INTER CORE LAT: %s\n", (*m_tbe_ptr).m_interCoreCohLatency);
    DPRINTF(RubySlicc, "MSI_Snooping_Time_based_base-cache.sm:1251: TOTAL LATENCY: %s\n", ((*m_tbe_ptr).m_endTime - (*m_tbe_ptr).m_startTime));
    m_total_latency = (m_total_latency + ((*m_tbe_ptr).m_endTime - (*m_tbe_ptr).m_startTime));
    DPRINTF(RubySlicc, "MSI_Snooping_Time_based_base-cache.sm:1254: TOTAL LATENCY PER CORE : %s\n", m_total_latency);
        if ((m_max_wcet_latency < ((*m_tbe_ptr).m_endTime - (*m_tbe_ptr).m_startTime))) {
            m_max_wcet_latency = ((*m_tbe_ptr).m_endTime - (*m_tbe_ptr).m_startTime);
            DPRINTF(RubySlicc, "MSI_Snooping_Time_based_base-cache.sm:1258: MW|C%s|%s\n", (*m_tbe_ptr).m_CoreID, m_max_wcet_latency);
        }
            if ((m_max_arbit_latency < (*m_tbe_ptr).m_arbitLatency)) {
                m_max_arbit_latency = (*m_tbe_ptr).m_arbitLatency;
                DPRINTF(RubySlicc, "MSI_Snooping_Time_based_base-cache.sm:1262: MAL|C%s|%s\n", (*m_tbe_ptr).m_CoreID, m_max_arbit_latency);
            }
                if ((m_max_interC_latency < (*m_tbe_ptr).m_interCoreCohLatency)) {
                    m_max_interC_latency = (*m_tbe_ptr).m_interCoreCohLatency;
                    DPRINTF(RubySlicc, "MSI_Snooping_Time_based_base-cache.sm:1266: MIC|C%s|%s\n", (*m_tbe_ptr).m_CoreID, m_max_interC_latency);
                }
                    if ((m_max_intraC_latency < (*m_tbe_ptr).m_intraCoreCohLatency)) {
                        m_max_intraC_latency = (*m_tbe_ptr).m_intraCoreCohLatency;
                        DPRINTF(RubySlicc, "MSI_Snooping_Time_based_base-cache.sm:1270: MICA|C%s|%s\n", (*m_tbe_ptr).m_CoreID, m_max_intraC_latency);
                    }
                        if ((m_max_S_to_M_latency < time_S_to_M)) {
                            m_max_S_to_M_latency = time_S_to_M;
                            DPRINTF(RubySlicc, "MSI_Snooping_Time_based_base-cache.sm:1274: MSM|C%s|%s\n", (*m_tbe_ptr).m_CoreID, m_max_S_to_M_latency);
                        }

}

/** \brief Set start time of request */
void
L1Cache_Controller::setStartTime(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing setStartTime on address %s\n", addr);
    (*m_tbe_ptr).m_startTime = (((*m_mandatoryQueue_ptr)).dequeue());

}

/** \brief Set start time of request */
void
L1Cache_Controller::setTime_SM(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing setTime_SM on address %s\n", addr);
    (*m_tbe_ptr).m_startTime_SM = (curCycle());

}

/** \brief Set start time of request cache entry */
void
L1Cache_Controller::setStartTimeCE(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing setStartTimeCE on address %s\n", addr);
    (*m_cache_entry_ptr).m_startTime = (((*m_mandatoryQueue_ptr)).dequeue());

}

/** \brief Copy start time from CE to TBE */
void
L1Cache_Controller::copyStartTimeCETBE(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing copyStartTimeCETBE on address %s\n", addr);
    (*m_tbe_ptr).m_startTime = (*m_cache_entry_ptr).m_startTime;

}

/** \brief Set scheduled time */
void
L1Cache_Controller::setFirstScheduledTime(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing setFirstScheduledTime on address %s\n", addr);
        if (((*m_tbe_ptr).m_firstScheduledTime == (zero_time()))) {
        (*m_tbe_ptr).m_firstScheduledTime = (((*m_requestToCache_ptr)).dequeue());
        (*m_tbe_ptr).m_firstScheduledTime = (getNearestStartSlot((*m_tbe_ptr).m_firstScheduledTime));
    } else {
        (((*m_requestToCache_ptr)).dequeue());
    }

}

/** \brief Set scheduled time */
void
L1Cache_Controller::setFirstScheduledTimeCE(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing setFirstScheduledTimeCE on address %s\n", addr);
        if (((*m_cache_entry_ptr).m_firstScheduledTime == (zero_time()))) {
        (*m_cache_entry_ptr).m_firstScheduledTime = (((*m_requestToCache_ptr)).dequeue());
        (*m_cache_entry_ptr).m_firstScheduledTime = (refineFirstScheduledTime((*m_cache_entry_ptr).m_firstScheduledTime));
    } else {
        (((*m_requestToCache_ptr)).dequeue());
    }

}

/** \brief Pop incoming request queue and profile the delay within this virtual network */
void
L1Cache_Controller::l_popRequestQueue(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing l_popRequestQueue on address %s\n", addr);
    (profileMsgDelay((2), (((*m_requestToCache_ptr)).dequeue())));

}

/** \brief Pop incoming request queue and profile the delay within this virtual network */
void
L1Cache_Controller::l_popRequestWBQueue(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing l_popRequestWBQueue on address %s\n", addr);
    (profileMsgDelay((2), (((*m_requestToCacheWB_ptr)).dequeue())));

}

/** \brief Pop Incoming Response queue and profile the delay within this virtual network */
void
L1Cache_Controller::o_popIncomingResponseQueue(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing o_popIncomingResponseQueue on address %s\n", addr);
    (profileMsgDelay((1), (((*m_responseToCache_ptr)).dequeue())));

}

/** \brief Deallocate TBE */
void
L1Cache_Controller::s_deallocateTBE(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing s_deallocateTBE on address %s\n", addr);
    (((*m_TBEs_ptr)).deallocate(addr));
unset_tbe(m_tbe_ptr);;

}

/** \brief Write data to cache */
void
L1Cache_Controller::u_writeDataToL1Cache(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing u_writeDataToL1Cache on address %s\n", addr);
    {
    // Declare message
    const ResponseMsg* in_msg_ptr M5_VAR_USED;
    in_msg_ptr = dynamic_cast<const ResponseMsg *>(((*m_responseToCache_ptr)).peek());
    assert(in_msg_ptr != NULL); // Check the cast result
#ifndef NDEBUG
if (!((m_cache_entry_ptr != NULL))) {
    panic("Runtime Error at MSI_Snooping_Time_based_base-cache.sm:1336: %s.\n", "assert failure");

}
#endif
;
(*m_cache_entry_ptr).m_DataBlk = ((*in_msg_ptr)).m_DataBlk;
(*m_cache_entry_ptr).m_Dirty = ((*in_msg_ptr)).m_Dirty;
(*m_tbe_ptr).m_CoreID = ((((*in_msg_ptr)).m_Destination).smallestNodeID());
}

}

/** \brief unset cache entry */
void
L1Cache_Controller::unset_cache_entry(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing unset_cache_entry on address %s\n", addr);
    unset_cache_entry(m_cache_entry_ptr);;

}

/** \brief Deallocate L1 cache block.  Sets the cache to not present, allowing a replacement in parallel with a fetch. */
void
L1Cache_Controller::ff_deallocateL1CacheBlock(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing ff_deallocateL1CacheBlock on address %s\n", addr);
        if ((((*m_Dcache_ptr)).isTagPresent(addr))) {
        (((*m_Dcache_ptr)).deallocate(addr));
    } else {
        (((*m_Icache_ptr)).deallocate(addr));
    }
    unset_cache_entry(m_cache_entry_ptr);;

}

/** \brief Set L1 D-cache tag equal to tag of block B. */
void
L1Cache_Controller::oo_allocateL1DCacheBlock(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing oo_allocateL1DCacheBlock on address %s\n", addr);
        if ((m_cache_entry_ptr == NULL)) {
        set_cache_entry(m_cache_entry_ptr, (((*m_Dcache_ptr)).allocate(addr, new L1Cache_Entry)));;
    }

}

/** \brief Set L1 I-cache tag equal to tag of block B. */
void
L1Cache_Controller::pp_allocateL1ICacheBlock(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing pp_allocateL1ICacheBlock on address %s\n", addr);
        if ((m_cache_entry_ptr == NULL)) {
        set_cache_entry(m_cache_entry_ptr, (((*m_Icache_ptr)).allocate(addr, new L1Cache_Entry)));;
    }

}

/** \brief recycle L1 request queue */
void
L1Cache_Controller::z_stallAndWaitMandatoryQueue(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing z_stallAndWaitMandatoryQueue on address %s\n", addr);
            stallBuffer(&((*m_mandatoryQueue_ptr)), addr);
        (*m_mandatoryQueue_ptr).stallMessage(addr);
        

}

/** \brief recycle L1 response queue */
void
L1Cache_Controller::z_stallAndWaitRequestQueue(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing z_stallAndWaitRequestQueue on address %s\n", addr);
            stallBuffer(&((*m_requestToCache_ptr)), addr);
        (*m_requestToCache_ptr).stallMessage(addr);
        

}

/** \brief recycle l1 request WB queue */
void
L1Cache_Controller::z_stallAndWaitRequestWBQueue(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing z_stallAndWaitRequestWBQueue on address %s\n", addr);
            stallBuffer(&((*m_requestToCacheWB_ptr)), addr);
        (*m_requestToCacheWB_ptr).stallMessage(addr);
        

}

/** \brief wake-up dependents */
void
L1Cache_Controller::kd_wakeUpDependents(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing kd_wakeUpDependents on address %s\n", addr);
        if ((isAtomic(addr))) {
    } else {
        (wakeUpBuffers(addr));
    }

}

/** \brief wake-up all dependents */
void
L1Cache_Controller::ka_wakeUpAllDependents(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing ka_wakeUpAllDependents on address %s\n", addr);
        if ((isAtomic(addr))) {
    } else {
        (wakeUpAllBuffers());
    }

}

/** \brief Profile the demand miss */
void
L1Cache_Controller::uu_profileInstMiss(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing uu_profileInstMiss on address %s\n", addr);
    (++ ((*m_Icache_ptr)).m_demand_misses);

}

/** \brief Profile the demand hit */
void
L1Cache_Controller::uu_profileInstHit(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing uu_profileInstHit on address %s\n", addr);
    (++ ((*m_Icache_ptr)).m_demand_hits);

}

/** \brief Profile the demand miss */
void
L1Cache_Controller::uu_profileDataMiss(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing uu_profileDataMiss on address %s\n", addr);
    (++ ((*m_Dcache_ptr)).m_demand_misses);

}

/** \brief Profile the demand hit */
void
L1Cache_Controller::uu_profileDataHit(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing uu_profileDataHit on address %s\n", addr);
    (++ ((*m_Dcache_ptr)).m_demand_hits);

}

/** \brief Profile the demand hit */
void
L1Cache_Controller::uu_profileLoadHit(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing uu_profileLoadHit on address %s\n", addr);
    (++ ((*m_Dcache_ptr)).m_load_hits);

}

/** \brief Profile the demand hit */
void
L1Cache_Controller::uu_profileStoreHit(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing uu_profileStoreHit on address %s\n", addr);
    (++ ((*m_Dcache_ptr)).m_store_hits);

}

/** \brief Profile the demand hit */
void
L1Cache_Controller::uu_profileLoadMiss(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing uu_profileLoadMiss on address %s\n", addr);
    (++ ((*m_Dcache_ptr)).m_load_misses);

}

/** \brief Profile the demand hit */
void
L1Cache_Controller::uu_profileStoreMiss(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing uu_profileStoreMiss on address %s\n", addr);
    (++ ((*m_Dcache_ptr)).m_store_misses);

}

/** \brief Profile the upgrade miss : Store on S */
void
L1Cache_Controller::uu_profileUpgradeMiss(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing uu_profileUpgradeMiss on address %s\n", addr);
    (++ ((*m_Dcache_ptr)).m_upgrade_misses);

}

/** \brief Profile the demand hit */
void
L1Cache_Controller::profile_inv_S(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing profile_inv_S on address %s\n", addr);
    (++ ((*m_Dcache_ptr)).m_inv_S);

}

/** \brief Profile the demand hit */
void
L1Cache_Controller::profile_inv_S_repl(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing profile_inv_S_repl on address %s\n", addr);
    (++ ((*m_Dcache_ptr)).m_inv_S_repl);

}

/** \brief Profile the demand hit */
void
L1Cache_Controller::profile_inv_M(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing profile_inv_M on address %s\n", addr);
    (++ ((*m_Dcache_ptr)).m_inv_M);

}

/** \brief unset timer */
void
L1Cache_Controller::unsetcriticalTimer(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing unsetcriticalTimer on address %s\n", addr);
    int numproc_c
 = (getnumproc());
    if (((IDToInt(((m_machineID).getNum()))) < numproc_c)) {
        (((*m_TimerTable_CC_ptr)).unset(addr));
    } else {
        (((*m_TimerTable_NC_ptr)).unset(addr));
    }

}

/** \brief unset timer */
void
L1Cache_Controller::unsetnoncriticalTimer(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing unsetnoncriticalTimer on address %s\n", addr);
    int numproc_c
 = (getnumproc());
    if (((IDToInt(((m_machineID).getNum()))) < numproc_c)) {
        (((*m_TimerTable_CN_ptr)).unset(addr));
    } else {
        (((*m_TimerTable_NN_ptr)).unset(addr));
    }

}

/** \brief unset 2 timers */
void
L1Cache_Controller::unsetTimers(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing unsetTimers on address %s\n", addr);
    int numproc_c
 = (getnumproc());
    if (((IDToInt(((m_machineID).getNum()))) < numproc_c)) {
            if ((((*m_TimerTable_CC_ptr)).isSet(addr))) {
                (((*m_TimerTable_CC_ptr)).unset(addr));
            }
                if ((((*m_TimerTable_CN_ptr)).isSet(addr))) {
                    (((*m_TimerTable_CN_ptr)).unset(addr));
                }
            } else {
                    if ((((*m_TimerTable_NC_ptr)).isSet(addr))) {
                        (((*m_TimerTable_NC_ptr)).unset(addr));
                    }
                        if ((((*m_TimerTable_NN_ptr)).isSet(addr))) {
                            (((*m_TimerTable_NN_ptr)).unset(addr));
                        }
                    }

}

/** \brief start 2 timers */
void
L1Cache_Controller::setTimers(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing setTimers on address %s\n", addr);
    Cycles timeToSubtract
 = ((*m_tbe_ptr).m_endTime - (getNearestStartSlot((*m_tbe_ptr).m_endTime)));
int numproc_c
 = (getnumproc());
    if (((IDToInt(((m_machineID).getNum()))) < numproc_c)) {
        (((*m_TimerTable_CC_ptr)).set(addr, (((m_timeout_latency_CC - timeToSubtract) - (10)) + (50))));
        (((*m_TimerTable_CN_ptr)).set(addr, (((m_timeout_latency_CN - timeToSubtract) - (10)) + (50))));
    } else {
        (((*m_TimerTable_NC_ptr)).set(addr, (((m_timeout_latency_NC - timeToSubtract) - (10)) + (50))));
        (((*m_TimerTable_NN_ptr)).set(addr, (((m_timeout_latency_NN - timeToSubtract) - (10)) + (50))));
    }

}

/** \brief restart 2 timers */
void
L1Cache_Controller::restartTimers(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing restartTimers on address %s\n", addr);
    int numproc_c
 = (getnumproc());
    if (((IDToInt(((m_machineID).getNum()))) < numproc_c)) {
        (((*m_TimerTable_CC_ptr)).set(addr, m_timeout_latency_CC));
        (((*m_TimerTable_CN_ptr)).set(addr, m_timeout_latency_CN));
    } else {
        (((*m_TimerTable_NC_ptr)).set(addr, m_timeout_latency_NC));
        (((*m_TimerTable_NN_ptr)).set(addr, m_timeout_latency_NN));
    }

}

/** \brief restart 2 timers */
void
L1Cache_Controller::restartcriticalTimer(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing restartcriticalTimer on address %s\n", addr);
    int numproc_c
 = (getnumproc());
    if (((IDToInt(((m_machineID).getNum()))) < numproc_c)) {
        (((*m_TimerTable_CC_ptr)).set(addr, m_timeout_latency_CC));
    } else {
        (((*m_TimerTable_NC_ptr)).set(addr, m_timeout_latency_NC));
    }

}

/** \brief restart 2 timers */
void
L1Cache_Controller::restartnoncriticalTimer(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing restartnoncriticalTimer on address %s\n", addr);
    int numproc_c
 = (getnumproc());
    if (((IDToInt(((m_machineID).getNum()))) < numproc_c)) {
        (((*m_TimerTable_CN_ptr)).set(addr, m_timeout_latency_CN));
    } else {
        (((*m_TimerTable_NN_ptr)).set(addr, m_timeout_latency_NN));
    }

}

/** \brief clearing the destination before reissuing */
void
L1Cache_Controller::clearDest(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr)
{
    DPRINTF(RubyGenerated, "executing clearDest on address %s\n", addr);
    (((*m_cache_entry_ptr).m_finalDestinationSet_NC).clear());
(((*m_cache_entry_ptr).m_finalDestinationSet_C).clear());

}

L1Cache_Entry*
L1Cache_Controller::getL1CacheEntry(const Address& param_addr)
{
L1Cache_Entry* Dcache_entry
 = static_cast<L1Cache_Entry *>((((*m_Dcache_ptr)).lookup(param_addr)))
;
    if ((Dcache_entry != NULL)) {
        return Dcache_entry;
    }
    L1Cache_Entry* Icache_entry
     = static_cast<L1Cache_Entry *>((((*m_Icache_ptr)).lookup(param_addr)))
    ;
    return Icache_entry;

}
bool
L1Cache_Controller::isAtomic(const Address& param_addr)
{
L1Cache_Entry* cache_entry
 = (getL1CacheEntry(param_addr));
    if ((cache_entry != NULL)) {
        return (*cache_entry).m_isAtomic;
    }
    return (false);

}
L1Cache_Entry*
L1Cache_Controller::getDCacheEntry(const Address& param_addr)
{
L1Cache_Entry* Dcache_entry
 = static_cast<L1Cache_Entry *>((((*m_Dcache_ptr)).lookup(param_addr)))
;
return Dcache_entry;

}
L1Cache_Entry*
L1Cache_Controller::getICacheEntry(const Address& param_addr)
{
L1Cache_Entry* Icache_entry
 = static_cast<L1Cache_Entry *>((((*m_Icache_ptr)).lookup(param_addr)))
;
return Icache_entry;

}
L1Cache_State
L1Cache_Controller::getState(L1Cache_TBE* param_tbe, L1Cache_Entry* param_cache_entry, const Address& param_addr)
{
#ifndef NDEBUG
if (!((((((*m_Dcache_ptr)).isTagPresent(param_addr)) && (((*m_Icache_ptr)).isTagPresent(param_addr))) == (false)))) {
    panic("Runtime Error at MSI_Snooping_Time_based_base-cache.sm:268: %s.\n", "assert failure");

}
#endif
;
    if ((param_tbe != NULL)) {
        return (*param_tbe).m_TBEState;
    } else {
            if ((param_cache_entry != NULL)) {
                return (*param_cache_entry).m_CacheState;
            }
        }
        return L1Cache_State_I;

}
Cycles
L1Cache_Controller::setTimeoutLatency(const bool& param_current, const bool& param_requestor)
{
    if (((param_current == (true)) && (param_requestor == (true)))) {
        return m_timeout_latency_CC;
    } else {
            if (((param_current == (true)) && (param_requestor == (false)))) {
                return m_timeout_latency_CN;
            } else {
                    if (((param_current == (false)) && (param_requestor == (true)))) {
                        return m_timeout_latency_NC;
                    } else {
                        return m_timeout_latency_NN;
                    }
                }
            }

}
bool
L1Cache_Controller::is_dataSendState(L1Cache_TBE* param_tbe, const Address& param_addr, L1Cache_Entry* param_cache_entry)
{
L1Cache_State returnState
 = (getState(param_tbe, param_cache_entry, param_addr));
    if ((((((returnState == L1Cache_State_M) || (returnState == L1Cache_State_MI_A)) || (returnState == L1Cache_State_IM_D)) || (returnState == L1Cache_State_IM_DI)) || (returnState == L1Cache_State_M_TI))) {
        return (true);
    }
    return (false);

}
bool
L1Cache_Controller::is_dataInvState(L1Cache_TBE* param_tbe, const Address& param_addr, L1Cache_Entry* param_cache_entry)
{
L1Cache_State returnState
 = (getState(param_tbe, param_cache_entry, param_addr));
    if (((((((returnState == L1Cache_State_S) || (returnState == L1Cache_State_SI_A)) || (returnState == L1Cache_State_S_TI)) || (returnState == L1Cache_State_IS_D)) || (returnState == L1Cache_State_IS_D)) || (returnState == L1Cache_State_S_TM))) {
        return (true);
    }
    return (false);

}
bool
L1Cache_Controller::is_noncriticalotherGETM(L1Cache_TBE* param_tbe, const Address& param_addr, L1Cache_Entry* param_cache_entry)
{
L1Cache_State returnState
 = (getState(param_tbe, param_cache_entry, param_addr));
    if (((((returnState == L1Cache_State_S) || (returnState == L1Cache_State_M)) || (returnState == L1Cache_State_S_TI)) || (returnState == L1Cache_State_M_TI))) {
        return (true);
    }
    return (false);

}
bool
L1Cache_Controller::is_noncriticalotherGETS(L1Cache_TBE* param_tbe, const Address& param_addr, L1Cache_Entry* param_cache_entry)
{
L1Cache_State returnState
 = (getState(param_tbe, param_cache_entry, param_addr));
    if (((returnState == L1Cache_State_M) || (returnState == L1Cache_State_M_TI))) {
        return (true);
    }
    return (false);

}
bool
L1Cache_Controller::is_criticalotherGETM(L1Cache_TBE* param_tbe, const Address& param_addr, L1Cache_Entry* param_cache_entry)
{
L1Cache_State returnState
 = (getState(param_tbe, param_cache_entry, param_addr));
    if (((((((((returnState == L1Cache_State_S) || (returnState == L1Cache_State_M)) || (returnState == L1Cache_State_S_TI)) || (returnState == L1Cache_State_M_TI)) || (returnState == L1Cache_State_IM_D)) || (returnState == L1Cache_State_IM_DI)) || (returnState == L1Cache_State_IS_D)) || (returnState == L1Cache_State_IS_DI))) {
        return (true);
    }
    return (false);

}
bool
L1Cache_Controller::is_criticalotherGETS(L1Cache_TBE* param_tbe, const Address& param_addr, L1Cache_Entry* param_cache_entry)
{
L1Cache_State returnState
 = (getState(param_tbe, param_cache_entry, param_addr));
    if (((((returnState == L1Cache_State_M) || (returnState == L1Cache_State_M_TI)) || (returnState == L1Cache_State_IM_D)) || (returnState == L1Cache_State_IM_DI))) {
        return (true);
    }
    return (false);

}
bool
L1Cache_Controller::is_dataWaitingState(L1Cache_TBE* param_tbe, const Address& param_addr, L1Cache_Entry* param_cache_entry)
{
L1Cache_State returnState
 = (getState(param_tbe, param_cache_entry, param_addr));
    if (((returnState == L1Cache_State_IM_D) || (returnState == L1Cache_State_M))) {
        return (true);
    }
    return (false);

}
bool
L1Cache_Controller::is_atomicInvariant(L1Cache_TBE* param_tbe, const Address& param_addr, L1Cache_Entry* param_cache_entry)
{
L1Cache_State returnState
 = (getState(param_tbe, param_cache_entry, param_addr));
    if ((((returnState == L1Cache_State_M) || (returnState == L1Cache_State_S)) || (returnState == L1Cache_State_I))) {
        return (false);
    }
    return (true);

}
void
L1Cache_Controller::setState(L1Cache_TBE* param_tbe, L1Cache_Entry* param_cache_entry, const Address& param_addr, const L1Cache_State& param_state)
{
#ifndef NDEBUG
if (!((((((*m_Dcache_ptr)).isTagPresent(param_addr)) && (((*m_Icache_ptr)).isTagPresent(param_addr))) == (false)))) {
    panic("Runtime Error at MSI_Snooping_Time_based_base-cache.sm:358: %s.\n", "assert failure");

}
#endif
;
    if ((param_tbe != NULL)) {
        (*param_tbe).m_TBEState = param_state;
    }
        if ((param_cache_entry != NULL)) {
            (*param_cache_entry).m_CacheState = param_state;
        }

}
AccessPermission
L1Cache_Controller::getAccessPermission(const Address& param_addr)
{
L1Cache_TBE* tbe
 = (((*m_TBEs_ptr)).lookup(param_addr));
    if ((tbe != NULL)) {
        return (L1Cache_State_to_permission((*tbe).m_TBEState));
    }
    L1Cache_Entry* cache_entry
     = (getL1CacheEntry(param_addr));
        if ((cache_entry != NULL)) {
            return (L1Cache_State_to_permission((*cache_entry).m_CacheState));
        }
        return AccessPermission_NotPresent;

}
void
L1Cache_Controller::functionalRead(const Address& param_addr, Packet* param_pkt)
{
L1Cache_TBE* tbe
 = (((*m_TBEs_ptr)).lookup(param_addr));
    if ((tbe != NULL)) {
        (testAndRead(param_addr, (*tbe).m_DataBlk, param_pkt));
    } else {
        (testAndRead(param_addr, (*(getL1CacheEntry(param_addr))).m_DataBlk, param_pkt));
    }

}
int
L1Cache_Controller::functionalWrite(const Address& param_addr, Packet* param_pkt)
{
int num_functional_writes
 = (0);
L1Cache_TBE* tbe
 = (((*m_TBEs_ptr)).lookup(param_addr));
    if ((tbe != NULL)) {
        num_functional_writes = (num_functional_writes + (testAndWrite(param_addr, (*tbe).m_DataBlk, param_pkt)));
        return num_functional_writes;
    }
    num_functional_writes = (num_functional_writes + (testAndWrite(param_addr, (*(getL1CacheEntry(param_addr))).m_DataBlk, param_pkt)));
    return num_functional_writes;

}
void
L1Cache_Controller::setAccessPermission(L1Cache_Entry* param_cache_entry, const Address& param_addr, const L1Cache_State& param_state)
{
    if ((param_cache_entry != NULL)) {
        ((*(param_cache_entry)).changePermission((L1Cache_State_to_permission(param_state))));
    }

}
L1Cache_Event
L1Cache_Controller::mandatory_request_type_to_event(const RubyRequestType& param_type)
{
    if ((param_type == RubyRequestType_LD)) {
        (++ ((*m_sequencer_ptr)).m_loads);
        return L1Cache_Event_Load;
    } else {
            if ((param_type == RubyRequestType_IFETCH)) {
                (++ ((*m_sequencer_ptr)).m_ifetch);
                return L1Cache_Event_Ifetch;
            } else {
                    if (((param_type == RubyRequestType_ST) || (param_type == RubyRequestType_ATOMIC))) {
                        (++ ((*m_sequencer_ptr)).m_stores);
                        return L1Cache_Event_Store;
                    } else {
                            if ((param_type == RubyRequestType_Locked_RMW_Read)) {
                                return L1Cache_Event_RMW_Read;
                                (++ ((*m_sequencer_ptr)).m_stores);
                            } else {
                                    if ((param_type == RubyRequestType_Locked_RMW_Write)) {
                                        return L1Cache_Event_RMW_Write;
                                        (++ ((*m_sequencer_ptr)).m_stores);
                                    } else {
                                        panic("Runtime Error at MSI_Snooping_Time_based_base-cache.sm:446: %s.\n", ("Invalid RubyRequestType"));
                                        ;
                                    }
                                }
                            }
                        }
                    }

}
int
L1Cache_Controller::getPendingAcks(L1Cache_TBE* param_tbe)
{
return (*param_tbe).m_pendingAcks;

}
int
L1Cache_Controller::functionalWriteBuffers(PacketPtr& pkt)
{
    int num_functional_writes = 0;
num_functional_writes += m_mandatoryQueue_ptr->functionalWrite(pkt);
num_functional_writes += m_requestFromCache_ptr->functionalWrite(pkt);
num_functional_writes += m_requestFromCacheWB_ptr->functionalWrite(pkt);
num_functional_writes += m_responseFromCache_ptr->functionalWrite(pkt);
num_functional_writes += m_atomicRequestFromCache_ptr->functionalWrite(pkt);
num_functional_writes += m_responseToCache_ptr->functionalWrite(pkt);
num_functional_writes += m_requestToCache_ptr->functionalWrite(pkt);
num_functional_writes += m_requestToCacheWB_ptr->functionalWrite(pkt);
num_functional_writes += m_atomicRequestToCache_ptr->functionalWrite(pkt);
    return num_functional_writes;
}
