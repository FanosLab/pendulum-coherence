/** \file L1Cache_Entry.cc
 *
 * Auto generated C++ code started by /home/mohamed/hourglass/gem5-stable/src/mem/slicc/symbols/Type.py:410
 */

#include <iostream>
#include <memory>

#include "mem/protocol/L1Cache_Entry.hh"
#include "mem/ruby/common/Global.hh"
#include "mem/ruby/system/System.hh"

using namespace std;
/** \brief Print the state of this object */
void
L1Cache_Entry::print(ostream& out) const
{
    out << "[L1Cache_Entry: ";
    out << "CacheState = " << m_CacheState << " ";
    out << "DataBlk = " << m_DataBlk << " ";
    out << "Dirty = " << m_Dirty << " ";
    out << "finalDestination = " << m_finalDestination << " ";
    out << "finalDestinationSet_NC = " << m_finalDestinationSet_NC << " ";
    out << "finalDestinationSet_C = " << m_finalDestinationSet_C << " ";
    out << "Timeout = " << m_Timeout << " ";
    out << "isAtomic = " << m_isAtomic << " ";
    out << "startTime = " << m_startTime << " ";
    out << "endTime = " << m_endTime << " ";
    out << "arbitLatency = " << m_arbitLatency << " ";
    out << "interCoreCohLatency = " << m_interCoreCohLatency << " ";
    out << "intraCoreCohLatency = " << m_intraCoreCohLatency << " ";
    out << "firstScheduledTime = " << m_firstScheduledTime << " ";
    out << "CoreID = " << m_CoreID << " ";
    out << "zeroArb = " << m_zeroArb << " ";
    out << "]";
}
