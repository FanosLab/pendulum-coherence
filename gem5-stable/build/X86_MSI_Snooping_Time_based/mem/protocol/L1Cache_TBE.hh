/** \file L1Cache_TBE.hh
 *
 *
 * Auto generated C++ code started by /home/mohamed/hourglass/gem5-stable/src/mem/slicc/symbols/Type.py:200
 */

#ifndef __L1Cache_TBE_HH__
#define __L1Cache_TBE_HH__

#include <iostream>

#include "mem/ruby/slicc_interface/RubySlicc_Util.hh"
#include "mem/protocol/Address.hh"
#include "mem/protocol/L1Cache_State.hh"
#include "mem/protocol/DataBlock.hh"
class L1Cache_TBE
{
  public:
    L1Cache_TBE
()
		{
        // m_Addr has no default
        m_TBEState = L1Cache_State_I; // default value of L1Cache_State
        // m_DataBlk has no default
        m_Dirty = false; // default for this field
        m_pendingAcks = 0; // default for this field
        m_isAtomic = false; // default for this field
        m_startTime = Cycles(0); // default for this field
        m_endTime = Cycles(0); // default for this field
        m_arbitLatency = Cycles(0); // default for this field
        m_interCoreCohLatency = Cycles(0); // default for this field
        m_intraCoreCohLatency = Cycles(0); // default for this field
        m_firstScheduledTime = Cycles(0); // default for this field
        m_CoreID = 0; // default value of int
        m_zeroArb = false; // default for this field
        m_startTime_SM = Cycles(0); // default for this field
    }
    L1Cache_TBE(const L1Cache_TBE&other)
    {
        m_Addr = other.m_Addr;
        m_TBEState = other.m_TBEState;
        m_DataBlk = other.m_DataBlk;
        m_Dirty = other.m_Dirty;
        m_pendingAcks = other.m_pendingAcks;
        m_isAtomic = other.m_isAtomic;
        m_startTime = other.m_startTime;
        m_endTime = other.m_endTime;
        m_arbitLatency = other.m_arbitLatency;
        m_interCoreCohLatency = other.m_interCoreCohLatency;
        m_intraCoreCohLatency = other.m_intraCoreCohLatency;
        m_firstScheduledTime = other.m_firstScheduledTime;
        m_CoreID = other.m_CoreID;
        m_zeroArb = other.m_zeroArb;
        m_startTime_SM = other.m_startTime_SM;
    }
    L1Cache_TBE(const Address& local_Addr, const L1Cache_State& local_TBEState, const DataBlock& local_DataBlk, const bool& local_Dirty, const int& local_pendingAcks, const bool& local_isAtomic, const Cycles& local_startTime, const Cycles& local_endTime, const Cycles& local_arbitLatency, const Cycles& local_interCoreCohLatency, const Cycles& local_intraCoreCohLatency, const Cycles& local_firstScheduledTime, const int& local_CoreID, const bool& local_zeroArb, const Cycles& local_startTime_SM)
    {
        m_Addr = local_Addr;
        m_TBEState = local_TBEState;
        m_DataBlk = local_DataBlk;
        m_Dirty = local_Dirty;
        m_pendingAcks = local_pendingAcks;
        m_isAtomic = local_isAtomic;
        m_startTime = local_startTime;
        m_endTime = local_endTime;
        m_arbitLatency = local_arbitLatency;
        m_interCoreCohLatency = local_interCoreCohLatency;
        m_intraCoreCohLatency = local_intraCoreCohLatency;
        m_firstScheduledTime = local_firstScheduledTime;
        m_CoreID = local_CoreID;
        m_zeroArb = local_zeroArb;
        m_startTime_SM = local_startTime_SM;
    }
    L1Cache_TBE*
    clone() const
    {
         return new L1Cache_TBE(*this);
    }
    // Const accessors methods for each field
    /** \brief Const accessor method for Addr field.
     *  \return Addr field
     */
    const Address&
    getAddr() const
    {
        return m_Addr;
    }
    /** \brief Const accessor method for TBEState field.
     *  \return TBEState field
     */
    const L1Cache_State&
    getTBEState() const
    {
        return m_TBEState;
    }
    /** \brief Const accessor method for DataBlk field.
     *  \return DataBlk field
     */
    const DataBlock&
    getDataBlk() const
    {
        return m_DataBlk;
    }
    /** \brief Const accessor method for Dirty field.
     *  \return Dirty field
     */
    const bool&
    getDirty() const
    {
        return m_Dirty;
    }
    /** \brief Const accessor method for pendingAcks field.
     *  \return pendingAcks field
     */
    const int&
    getpendingAcks() const
    {
        return m_pendingAcks;
    }
    /** \brief Const accessor method for isAtomic field.
     *  \return isAtomic field
     */
    const bool&
    getisAtomic() const
    {
        return m_isAtomic;
    }
    /** \brief Const accessor method for startTime field.
     *  \return startTime field
     */
    const Cycles&
    getstartTime() const
    {
        return m_startTime;
    }
    /** \brief Const accessor method for endTime field.
     *  \return endTime field
     */
    const Cycles&
    getendTime() const
    {
        return m_endTime;
    }
    /** \brief Const accessor method for arbitLatency field.
     *  \return arbitLatency field
     */
    const Cycles&
    getarbitLatency() const
    {
        return m_arbitLatency;
    }
    /** \brief Const accessor method for interCoreCohLatency field.
     *  \return interCoreCohLatency field
     */
    const Cycles&
    getinterCoreCohLatency() const
    {
        return m_interCoreCohLatency;
    }
    /** \brief Const accessor method for intraCoreCohLatency field.
     *  \return intraCoreCohLatency field
     */
    const Cycles&
    getintraCoreCohLatency() const
    {
        return m_intraCoreCohLatency;
    }
    /** \brief Const accessor method for firstScheduledTime field.
     *  \return firstScheduledTime field
     */
    const Cycles&
    getfirstScheduledTime() const
    {
        return m_firstScheduledTime;
    }
    /** \brief Const accessor method for CoreID field.
     *  \return CoreID field
     */
    const int&
    getCoreID() const
    {
        return m_CoreID;
    }
    /** \brief Const accessor method for zeroArb field.
     *  \return zeroArb field
     */
    const bool&
    getzeroArb() const
    {
        return m_zeroArb;
    }
    /** \brief Const accessor method for startTime_SM field.
     *  \return startTime_SM field
     */
    const Cycles&
    getstartTime_SM() const
    {
        return m_startTime_SM;
    }
    // Non const Accessors methods for each field
    /** \brief Non-const accessor method for Addr field.
     *  \return Addr field
     */
    Address&
    getAddr()
    {
        return m_Addr;
    }
    /** \brief Non-const accessor method for TBEState field.
     *  \return TBEState field
     */
    L1Cache_State&
    getTBEState()
    {
        return m_TBEState;
    }
    /** \brief Non-const accessor method for DataBlk field.
     *  \return DataBlk field
     */
    DataBlock&
    getDataBlk()
    {
        return m_DataBlk;
    }
    /** \brief Non-const accessor method for Dirty field.
     *  \return Dirty field
     */
    bool&
    getDirty()
    {
        return m_Dirty;
    }
    /** \brief Non-const accessor method for pendingAcks field.
     *  \return pendingAcks field
     */
    int&
    getpendingAcks()
    {
        return m_pendingAcks;
    }
    /** \brief Non-const accessor method for isAtomic field.
     *  \return isAtomic field
     */
    bool&
    getisAtomic()
    {
        return m_isAtomic;
    }
    /** \brief Non-const accessor method for startTime field.
     *  \return startTime field
     */
    Cycles&
    getstartTime()
    {
        return m_startTime;
    }
    /** \brief Non-const accessor method for endTime field.
     *  \return endTime field
     */
    Cycles&
    getendTime()
    {
        return m_endTime;
    }
    /** \brief Non-const accessor method for arbitLatency field.
     *  \return arbitLatency field
     */
    Cycles&
    getarbitLatency()
    {
        return m_arbitLatency;
    }
    /** \brief Non-const accessor method for interCoreCohLatency field.
     *  \return interCoreCohLatency field
     */
    Cycles&
    getinterCoreCohLatency()
    {
        return m_interCoreCohLatency;
    }
    /** \brief Non-const accessor method for intraCoreCohLatency field.
     *  \return intraCoreCohLatency field
     */
    Cycles&
    getintraCoreCohLatency()
    {
        return m_intraCoreCohLatency;
    }
    /** \brief Non-const accessor method for firstScheduledTime field.
     *  \return firstScheduledTime field
     */
    Cycles&
    getfirstScheduledTime()
    {
        return m_firstScheduledTime;
    }
    /** \brief Non-const accessor method for CoreID field.
     *  \return CoreID field
     */
    int&
    getCoreID()
    {
        return m_CoreID;
    }
    /** \brief Non-const accessor method for zeroArb field.
     *  \return zeroArb field
     */
    bool&
    getzeroArb()
    {
        return m_zeroArb;
    }
    /** \brief Non-const accessor method for startTime_SM field.
     *  \return startTime_SM field
     */
    Cycles&
    getstartTime_SM()
    {
        return m_startTime_SM;
    }
    // Mutator methods for each field
    /** \brief Mutator method for Addr field */
    void
    setAddr(const Address& local_Addr)
    {
        m_Addr = local_Addr;
    }
    /** \brief Mutator method for TBEState field */
    void
    setTBEState(const L1Cache_State& local_TBEState)
    {
        m_TBEState = local_TBEState;
    }
    /** \brief Mutator method for DataBlk field */
    void
    setDataBlk(const DataBlock& local_DataBlk)
    {
        m_DataBlk = local_DataBlk;
    }
    /** \brief Mutator method for Dirty field */
    void
    setDirty(const bool& local_Dirty)
    {
        m_Dirty = local_Dirty;
    }
    /** \brief Mutator method for pendingAcks field */
    void
    setpendingAcks(const int& local_pendingAcks)
    {
        m_pendingAcks = local_pendingAcks;
    }
    /** \brief Mutator method for isAtomic field */
    void
    setisAtomic(const bool& local_isAtomic)
    {
        m_isAtomic = local_isAtomic;
    }
    /** \brief Mutator method for startTime field */
    void
    setstartTime(const Cycles& local_startTime)
    {
        m_startTime = local_startTime;
    }
    /** \brief Mutator method for endTime field */
    void
    setendTime(const Cycles& local_endTime)
    {
        m_endTime = local_endTime;
    }
    /** \brief Mutator method for arbitLatency field */
    void
    setarbitLatency(const Cycles& local_arbitLatency)
    {
        m_arbitLatency = local_arbitLatency;
    }
    /** \brief Mutator method for interCoreCohLatency field */
    void
    setinterCoreCohLatency(const Cycles& local_interCoreCohLatency)
    {
        m_interCoreCohLatency = local_interCoreCohLatency;
    }
    /** \brief Mutator method for intraCoreCohLatency field */
    void
    setintraCoreCohLatency(const Cycles& local_intraCoreCohLatency)
    {
        m_intraCoreCohLatency = local_intraCoreCohLatency;
    }
    /** \brief Mutator method for firstScheduledTime field */
    void
    setfirstScheduledTime(const Cycles& local_firstScheduledTime)
    {
        m_firstScheduledTime = local_firstScheduledTime;
    }
    /** \brief Mutator method for CoreID field */
    void
    setCoreID(const int& local_CoreID)
    {
        m_CoreID = local_CoreID;
    }
    /** \brief Mutator method for zeroArb field */
    void
    setzeroArb(const bool& local_zeroArb)
    {
        m_zeroArb = local_zeroArb;
    }
    /** \brief Mutator method for startTime_SM field */
    void
    setstartTime_SM(const Cycles& local_startTime_SM)
    {
        m_startTime_SM = local_startTime_SM;
    }
    void print(std::ostream& out) const;
  //private:
    /** Physical address for this TBE */
    Address m_Addr;
    /** Transient state */
    L1Cache_State m_TBEState;
    /** Buffer for the data block */
    DataBlock m_DataBlk;
    /** data is dirty */
    bool m_Dirty;
    /** number of pending acks */
    int m_pendingAcks;
    /** data is atomic */
    bool m_isAtomic;
    /** start time of request */
    Cycles m_startTime;
    /** end time of request */
    Cycles m_endTime;
    /** arbit latency of request */
    Cycles m_arbitLatency;
    /** inter-core coherence latency of request */
    Cycles m_interCoreCohLatency;
    /** intra-core coherence latency of request */
    Cycles m_intraCoreCohLatency;
    /** first scheduled time */
    Cycles m_firstScheduledTime;
    int m_CoreID;
    /** zero arbitration */
    bool m_zeroArb;
    /** start time of request */
    Cycles m_startTime_SM;
};
inline std::ostream&
operator<<(std::ostream& out, const L1Cache_TBE& obj)
{
    obj.print(out);
    out << std::flush;
    return out;
}

#endif // __L1Cache_TBE_HH__
