// Auto generated C++ code started by /home/mohamed/hourglass/gem5-stable/src/mem/slicc/symbols/StateMachine.py:1159
// L1Cache: MSI Snooping L1 Cache CMP

#include <cassert>

#include "base/misc.hh"
#include "base/trace.hh"
#include "debug/ProtocolTrace.hh"
#include "debug/RubyGenerated.hh"
#include "mem/protocol/L1Cache_Controller.hh"
#include "mem/protocol/L1Cache_Event.hh"
#include "mem/protocol/L1Cache_State.hh"
#include "mem/protocol/Types.hh"
#include "mem/ruby/common/Global.hh"
#include "mem/ruby/system/System.hh"

#define HASH_FUN(state, event)  ((int(state)*L1Cache_Event_NUM)+int(event))

#define GET_TRANSITION_COMMENT() (L1Cache_transitionComment.str())
#define CLEAR_TRANSITION_COMMENT() (L1Cache_transitionComment.str(""))

TransitionResult
L1Cache_Controller::doTransition(L1Cache_Event event,
                                  L1Cache_Entry* m_cache_entry_ptr,
                                  L1Cache_TBE* m_tbe_ptr,
                                  const Address addr)
{
    L1Cache_State state = getState(m_tbe_ptr, m_cache_entry_ptr, addr);
    L1Cache_State next_state = state;

    DPRINTF(RubyGenerated, "%s, Time: %lld, state: %s, event: %s, addr: %s\n",
            *this, curCycle(), L1Cache_State_to_string(state),
            L1Cache_Event_to_string(event), addr);

    TransitionResult result =
    doTransitionWorker(event, state, next_state, m_tbe_ptr, m_cache_entry_ptr, addr);

    if (result == TransitionResult_Valid) {
        DPRINTF(RubyGenerated, "next_state: %s\n",
                L1Cache_State_to_string(next_state));
        countTransition(state, event);

        DPRINTFR(ProtocolTrace, "%15d %3s %10s%20s %6s>%-6s %s %s\n",
                 curTick(), m_version, "L1Cache",
                 L1Cache_Event_to_string(event),
                 L1Cache_State_to_string(state),
                 L1Cache_State_to_string(next_state),
                 addr, GET_TRANSITION_COMMENT());

        CLEAR_TRANSITION_COMMENT();
    setState(m_tbe_ptr, m_cache_entry_ptr, addr, next_state);
    setAccessPermission(m_cache_entry_ptr, addr, next_state);
    } else if (result == TransitionResult_ResourceStall) {
        DPRINTFR(ProtocolTrace, "%15s %3s %10s%20s %6s>%-6s %s %s\n",
                 curTick(), m_version, "L1Cache",
                 L1Cache_Event_to_string(event),
                 L1Cache_State_to_string(state),
                 L1Cache_State_to_string(next_state),
                 addr, "Resource Stall");
    } else if (result == TransitionResult_ProtocolStall) {
        DPRINTF(RubyGenerated, "stalling\n");
        DPRINTFR(ProtocolTrace, "%15s %3s %10s%20s %6s>%-6s %s %s\n",
                 curTick(), m_version, "L1Cache",
                 L1Cache_Event_to_string(event),
                 L1Cache_State_to_string(state),
                 L1Cache_State_to_string(next_state),
                 addr, "Protocol Stall");
    }

    return result;
}

TransitionResult
L1Cache_Controller::doTransitionWorker(L1Cache_Event event,
                                        L1Cache_State state,
                                        L1Cache_State& next_state,
                                        L1Cache_TBE*& m_tbe_ptr,
                                        L1Cache_Entry*& m_cache_entry_ptr,
                                        const Address& addr)
{
    switch(HASH_FUN(state, event)) {
  case HASH_FUN(L1Cache_State_I, L1Cache_Event_Load):
    next_state = L1Cache_State_IS_AD;
    if (!(*m_TBEs_ptr).areNSlotsAvailable(1))
        return TransitionResult_ResourceStall;
    if (!(*m_requestFromCache_ptr).areNSlotsAvailable(1))
        return TransitionResult_ResourceStall;
    oo_allocateL1DCacheBlock(m_tbe_ptr, m_cache_entry_ptr, addr);
    i_allocateTBE(m_tbe_ptr, m_cache_entry_ptr, addr);
    uu_profileDataMiss(m_tbe_ptr, m_cache_entry_ptr, addr);
    uu_profileLoadMiss(m_tbe_ptr, m_cache_entry_ptr, addr);
    as_issueGETSMem(m_tbe_ptr, m_cache_entry_ptr, addr);
    setStartTime(m_tbe_ptr, m_cache_entry_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(L1Cache_State_I, L1Cache_Event_Ifetch):
    next_state = L1Cache_State_IS_AD;
    if (!(*m_TBEs_ptr).areNSlotsAvailable(1))
        return TransitionResult_ResourceStall;
    if (!(*m_requestFromCache_ptr).areNSlotsAvailable(1))
        return TransitionResult_ResourceStall;
    pp_allocateL1ICacheBlock(m_tbe_ptr, m_cache_entry_ptr, addr);
    i_allocateTBE(m_tbe_ptr, m_cache_entry_ptr, addr);
    uu_profileInstMiss(m_tbe_ptr, m_cache_entry_ptr, addr);
    ai_issueGETINSTR(m_tbe_ptr, m_cache_entry_ptr, addr);
    setStartTime(m_tbe_ptr, m_cache_entry_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(L1Cache_State_IS_AD, L1Cache_Event_OWN_GETS):
  case HASH_FUN(L1Cache_State_IS_AD, L1Cache_Event_OWN_GETI):
    next_state = L1Cache_State_IS_D;
    setFirstScheduledTime(m_tbe_ptr, m_cache_entry_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(L1Cache_State_IS_AD, L1Cache_Event_Other_GETS_C):
  case HASH_FUN(L1Cache_State_IS_AD, L1Cache_Event_Other_GETS_NC):
  case HASH_FUN(L1Cache_State_IS_AD, L1Cache_Event_Other_GETM_C):
  case HASH_FUN(L1Cache_State_IS_AD, L1Cache_Event_Other_GETM_NC):
  case HASH_FUN(L1Cache_State_IS_AD, L1Cache_Event_Other_GETI_C):
  case HASH_FUN(L1Cache_State_IS_AD, L1Cache_Event_Other_GETI_NC):
  case HASH_FUN(L1Cache_State_IM_AD, L1Cache_Event_Other_GETS_C):
  case HASH_FUN(L1Cache_State_IM_AD, L1Cache_Event_Other_GETS_NC):
  case HASH_FUN(L1Cache_State_IM_AD, L1Cache_Event_Other_GETM_C):
  case HASH_FUN(L1Cache_State_IM_AD, L1Cache_Event_Other_GETM_NC):
  case HASH_FUN(L1Cache_State_IM_AD, L1Cache_Event_Other_GETI_C):
  case HASH_FUN(L1Cache_State_IM_AD, L1Cache_Event_Other_GETI_NC):
  case HASH_FUN(L1Cache_State_S_TI, L1Cache_Event_Other_GETS_C):
  case HASH_FUN(L1Cache_State_S_TI, L1Cache_Event_Other_GETS_NC):
  case HASH_FUN(L1Cache_State_S_TI, L1Cache_Event_Other_GETM_C):
  case HASH_FUN(L1Cache_State_S_TI, L1Cache_Event_Other_GETM_NC):
  case HASH_FUN(L1Cache_State_S_TI, L1Cache_Event_Other_GETI_C):
  case HASH_FUN(L1Cache_State_S_TI, L1Cache_Event_Other_GETI_NC):
  case HASH_FUN(L1Cache_State_I, L1Cache_Event_Other_GETS_C):
  case HASH_FUN(L1Cache_State_I, L1Cache_Event_Other_GETS_NC):
  case HASH_FUN(L1Cache_State_I, L1Cache_Event_Other_GETM_C):
  case HASH_FUN(L1Cache_State_I, L1Cache_Event_Other_GETM_NC):
  case HASH_FUN(L1Cache_State_I, L1Cache_Event_Other_GETI_C):
  case HASH_FUN(L1Cache_State_I, L1Cache_Event_Other_GETI_NC):
  case HASH_FUN(L1Cache_State_IS_D, L1Cache_Event_Other_GETS_C):
  case HASH_FUN(L1Cache_State_IS_D, L1Cache_Event_Other_GETS_NC):
  case HASH_FUN(L1Cache_State_IS_D, L1Cache_Event_Other_GETI_C):
  case HASH_FUN(L1Cache_State_IS_D, L1Cache_Event_Other_GETI_NC):
  case HASH_FUN(L1Cache_State_IS_DI, L1Cache_Event_Other_GETS_C):
  case HASH_FUN(L1Cache_State_IS_DI, L1Cache_Event_Other_GETS_NC):
  case HASH_FUN(L1Cache_State_IS_DI, L1Cache_Event_Other_GETM_NC):
  case HASH_FUN(L1Cache_State_IS_DI, L1Cache_Event_Other_GETI_C):
  case HASH_FUN(L1Cache_State_IS_DI, L1Cache_Event_Other_GETI_NC):
  case HASH_FUN(L1Cache_State_S, L1Cache_Event_Other_GETS_C):
  case HASH_FUN(L1Cache_State_S, L1Cache_Event_Other_GETS_NC):
  case HASH_FUN(L1Cache_State_S, L1Cache_Event_Other_GETI_C):
  case HASH_FUN(L1Cache_State_S, L1Cache_Event_Other_GETI_NC):
  case HASH_FUN(L1Cache_State_S_TM, L1Cache_Event_Other_GETS_C):
  case HASH_FUN(L1Cache_State_S_TM, L1Cache_Event_Other_GETS_NC):
  case HASH_FUN(L1Cache_State_S_TM, L1Cache_Event_Other_GETI_C):
  case HASH_FUN(L1Cache_State_S_TM, L1Cache_Event_Other_GETI_NC):
  case HASH_FUN(L1Cache_State_S_TM, L1Cache_Event_Other_GETM_C):
  case HASH_FUN(L1Cache_State_S_TM, L1Cache_Event_Other_GETM_NC):
  case HASH_FUN(L1Cache_State_S_MA, L1Cache_Event_Other_GETS_NC):
  case HASH_FUN(L1Cache_State_S_MA, L1Cache_Event_Other_GETI_NC):
  case HASH_FUN(L1Cache_State_S_MA, L1Cache_Event_Other_GETS_C):
  case HASH_FUN(L1Cache_State_S_MA, L1Cache_Event_Other_GETI_C):
  case HASH_FUN(L1Cache_State_IM_DI, L1Cache_Event_Other_GETM_NC):
  case HASH_FUN(L1Cache_State_IM_DI, L1Cache_Event_Other_GETS_NC):
  case HASH_FUN(L1Cache_State_IM_DI, L1Cache_Event_Other_GETI_NC):
  case HASH_FUN(L1Cache_State_M_TI, L1Cache_Event_Other_GETM_NC):
  case HASH_FUN(L1Cache_State_M_TI, L1Cache_Event_Other_GETM_C):
  case HASH_FUN(L1Cache_State_M_TI, L1Cache_Event_Other_GETS_C):
  case HASH_FUN(L1Cache_State_M_TI, L1Cache_Event_Other_GETS_NC):
  case HASH_FUN(L1Cache_State_M_TI, L1Cache_Event_Other_GETI_C):
  case HASH_FUN(L1Cache_State_M_TI, L1Cache_Event_Other_GETI_NC):
  case HASH_FUN(L1Cache_State_I, L1Cache_Event_INV):
  case HASH_FUN(L1Cache_State_MI_A, L1Cache_Event_Other_GETM_NC):
  case HASH_FUN(L1Cache_State_MI_A, L1Cache_Event_Other_GETS_NC):
  case HASH_FUN(L1Cache_State_MI_A, L1Cache_Event_Other_GETI_NC):
  case HASH_FUN(L1Cache_State_SI_A, L1Cache_Event_Other_GETS_NC):
  case HASH_FUN(L1Cache_State_SI_A, L1Cache_Event_Other_GETI_NC):
  case HASH_FUN(L1Cache_State_SI_A, L1Cache_Event_Other_GETS_C):
  case HASH_FUN(L1Cache_State_SI_A, L1Cache_Event_Other_GETI_C):
  case HASH_FUN(L1Cache_State_SI, L1Cache_Event_Other_GETM_C):
  case HASH_FUN(L1Cache_State_SI, L1Cache_Event_Other_GETM_NC):
  case HASH_FUN(L1Cache_State_SI, L1Cache_Event_Other_GETS_NC):
  case HASH_FUN(L1Cache_State_SI, L1Cache_Event_Other_GETI_NC):
  case HASH_FUN(L1Cache_State_SI, L1Cache_Event_Other_GETS_C):
  case HASH_FUN(L1Cache_State_SI, L1Cache_Event_Other_GETI_C):
    l_popRequestQueue(m_tbe_ptr, m_cache_entry_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(L1Cache_State_IS_D, L1Cache_Event_Other_GETM_C):
  case HASH_FUN(L1Cache_State_IS_DI, L1Cache_Event_Other_GETM_C):
    next_state = L1Cache_State_IS_AD;
    if (!(*m_requestFromCache_ptr).areNSlotsAvailable(1))
        return TransitionResult_ResourceStall;
    clearDest(m_tbe_ptr, m_cache_entry_ptr, addr);
    as_issueGETSMem(m_tbe_ptr, m_cache_entry_ptr, addr);
    l_popRequestQueue(m_tbe_ptr, m_cache_entry_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(L1Cache_State_IS_D, L1Cache_Event_Other_GETM_NC):
    next_state = L1Cache_State_IS_DI;
    l_popRequestQueue(m_tbe_ptr, m_cache_entry_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(L1Cache_State_IS_D, L1Cache_Event_Data):
    next_state = L1Cache_State_S;
    u_writeDataToL1Cache(m_tbe_ptr, m_cache_entry_ptr, addr);
    r_load_hit(m_tbe_ptr, m_cache_entry_ptr, addr);
    calcLatencyProfile(m_tbe_ptr, m_cache_entry_ptr, addr);
    setTimers(m_tbe_ptr, m_cache_entry_ptr, addr);
    s_deallocateTBE(m_tbe_ptr, m_cache_entry_ptr, addr);
    kd_wakeUpDependents(m_tbe_ptr, m_cache_entry_ptr, addr);
    ka_wakeUpAllDependents(m_tbe_ptr, m_cache_entry_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(L1Cache_State_IS_DI, L1Cache_Event_Data):
    next_state = L1Cache_State_S_TI;
    u_writeDataToL1Cache(m_tbe_ptr, m_cache_entry_ptr, addr);
    r_load_hit(m_tbe_ptr, m_cache_entry_ptr, addr);
    calcLatencyProfile(m_tbe_ptr, m_cache_entry_ptr, addr);
    setTimers(m_tbe_ptr, m_cache_entry_ptr, addr);
    s_deallocateTBE(m_tbe_ptr, m_cache_entry_ptr, addr);
    kd_wakeUpDependents(m_tbe_ptr, m_cache_entry_ptr, addr);
    ka_wakeUpAllDependents(m_tbe_ptr, m_cache_entry_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(L1Cache_State_S, L1Cache_Event_Load):
  case HASH_FUN(L1Cache_State_S_TI, L1Cache_Event_Load):
  case HASH_FUN(L1Cache_State_SI_A, L1Cache_Event_Load):
  case HASH_FUN(L1Cache_State_S_MA, L1Cache_Event_Load):
  case HASH_FUN(L1Cache_State_SI, L1Cache_Event_Load):
  case HASH_FUN(L1Cache_State_M, L1Cache_Event_Load):
  case HASH_FUN(L1Cache_State_M_TI, L1Cache_Event_Load):
  case HASH_FUN(L1Cache_State_MI_A, L1Cache_Event_Load):
  case HASH_FUN(L1Cache_State_MI_R, L1Cache_Event_Load):
    rx_load_hit(m_tbe_ptr, m_cache_entry_ptr, addr);
    uu_profileDataHit(m_tbe_ptr, m_cache_entry_ptr, addr);
    uu_profileLoadHit(m_tbe_ptr, m_cache_entry_ptr, addr);
    k_popMandatoryQueue(m_tbe_ptr, m_cache_entry_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(L1Cache_State_SI, L1Cache_Event_Ifetch):
  case HASH_FUN(L1Cache_State_S, L1Cache_Event_Ifetch):
  case HASH_FUN(L1Cache_State_SI_A, L1Cache_Event_Ifetch):
  case HASH_FUN(L1Cache_State_S_MA, L1Cache_Event_Ifetch):
  case HASH_FUN(L1Cache_State_S_TI, L1Cache_Event_Ifetch):
  case HASH_FUN(L1Cache_State_M, L1Cache_Event_Ifetch):
  case HASH_FUN(L1Cache_State_M_TI, L1Cache_Event_Ifetch):
    rx_load_hit(m_tbe_ptr, m_cache_entry_ptr, addr);
    uu_profileInstHit(m_tbe_ptr, m_cache_entry_ptr, addr);
    k_popMandatoryQueue(m_tbe_ptr, m_cache_entry_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(L1Cache_State_S, L1Cache_Event_Other_GETM_NC):
  case HASH_FUN(L1Cache_State_S, L1Cache_Event_Other_GETM_C):
    next_state = L1Cache_State_S_TI;
    profile_inv_S(m_tbe_ptr, m_cache_entry_ptr, addr);
    l_popRequestQueue(m_tbe_ptr, m_cache_entry_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(L1Cache_State_S_TI, L1Cache_Event_Timeout_NC):
  case HASH_FUN(L1Cache_State_S_TI, L1Cache_Event_Timeout_CC):
  case HASH_FUN(L1Cache_State_S_TI, L1Cache_Event_Timeout_NN):
  case HASH_FUN(L1Cache_State_S_TI, L1Cache_Event_Timeout_CN):
    next_state = L1Cache_State_SI_A;
    if (!(*m_requestFromCacheWB_ptr).areNSlotsAvailable(1))
        return TransitionResult_ResourceStall;
    issueInv(m_tbe_ptr, m_cache_entry_ptr, addr);
    unsetTimers(m_tbe_ptr, m_cache_entry_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(L1Cache_State_SI, L1Cache_Event_ALLINV):
  case HASH_FUN(L1Cache_State_SI_A, L1Cache_Event_Own_SELFINV_SPL):
  case HASH_FUN(L1Cache_State_SI, L1Cache_Event_Own_SELFINV_SPL):
    next_state = L1Cache_State_I;
    ff_deallocateL1CacheBlock(m_tbe_ptr, m_cache_entry_ptr, addr);
    l_popRequestWBQueue(m_tbe_ptr, m_cache_entry_ptr, addr);
    ka_wakeUpAllDependents(m_tbe_ptr, m_cache_entry_ptr, addr);
    kd_wakeUpDependents(m_tbe_ptr, m_cache_entry_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(L1Cache_State_SI_A, L1Cache_Event_Own_SELFINV):
    next_state = L1Cache_State_SI;
    l_popRequestWBQueue(m_tbe_ptr, m_cache_entry_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(L1Cache_State_SI_A, L1Cache_Event_Other_SELFINV):
  case HASH_FUN(L1Cache_State_S_TI, L1Cache_Event_Other_SELFINV):
  case HASH_FUN(L1Cache_State_S, L1Cache_Event_Other_SELFINV):
  case HASH_FUN(L1Cache_State_I, L1Cache_Event_Other_SELFINV):
  case HASH_FUN(L1Cache_State_IM_AD, L1Cache_Event_Other_SELFINV):
  case HASH_FUN(L1Cache_State_IM_D, L1Cache_Event_Other_SELFINV):
  case HASH_FUN(L1Cache_State_SI, L1Cache_Event_Other_SELFINV):
  case HASH_FUN(L1Cache_State_S_TM, L1Cache_Event_Other_SELFINV):
  case HASH_FUN(L1Cache_State_IS_D, L1Cache_Event_Other_SELFINV):
  case HASH_FUN(L1Cache_State_IS_AD, L1Cache_Event_Other_SELFINV):
  case HASH_FUN(L1Cache_State_IM_DI, L1Cache_Event_Other_SELFINV):
  case HASH_FUN(L1Cache_State_IS_DI, L1Cache_Event_Other_SELFINV):
  case HASH_FUN(L1Cache_State_M, L1Cache_Event_Other_SELFINV):
  case HASH_FUN(L1Cache_State_M_TI, L1Cache_Event_Other_SELFINV):
  case HASH_FUN(L1Cache_State_MI_A, L1Cache_Event_Other_SELFINV):
  case HASH_FUN(L1Cache_State_MI_R, L1Cache_Event_Other_SELFINV):
  case HASH_FUN(L1Cache_State_IM_AD, L1Cache_Event_ALLINV):
  case HASH_FUN(L1Cache_State_IM_D, L1Cache_Event_ALLINV):
  case HASH_FUN(L1Cache_State_I, L1Cache_Event_ALLINV):
  case HASH_FUN(L1Cache_State_IS_AD, L1Cache_Event_ALLINV):
  case HASH_FUN(L1Cache_State_IS_D, L1Cache_Event_ALLINV):
  case HASH_FUN(L1Cache_State_IM_DI, L1Cache_Event_ALLINV):
  case HASH_FUN(L1Cache_State_IS_DI, L1Cache_Event_ALLINV):
  case HASH_FUN(L1Cache_State_IM_AD, L1Cache_Event_Own_SELFINV):
  case HASH_FUN(L1Cache_State_IM_AD, L1Cache_Event_Own_SELFINV_SPL):
  case HASH_FUN(L1Cache_State_IM_D, L1Cache_Event_Own_SELFINV):
  case HASH_FUN(L1Cache_State_IM_D, L1Cache_Event_Own_SELFINV_SPL):
  case HASH_FUN(L1Cache_State_I, L1Cache_Event_Own_SELFINV):
  case HASH_FUN(L1Cache_State_I, L1Cache_Event_Own_SELFINV_SPL):
  case HASH_FUN(L1Cache_State_IS_AD, L1Cache_Event_Own_SELFINV):
  case HASH_FUN(L1Cache_State_IS_AD, L1Cache_Event_Own_SELFINV_SPL):
  case HASH_FUN(L1Cache_State_IS_D, L1Cache_Event_Own_SELFINV):
  case HASH_FUN(L1Cache_State_IS_D, L1Cache_Event_Own_SELFINV_SPL):
  case HASH_FUN(L1Cache_State_IM_DI, L1Cache_Event_Own_SELFINV):
  case HASH_FUN(L1Cache_State_IM_DI, L1Cache_Event_Own_SELFINV_SPL):
  case HASH_FUN(L1Cache_State_S_MA, L1Cache_Event_Other_SELFINV):
  case HASH_FUN(L1Cache_State_IS_AD, L1Cache_Event_OWN_PUTM):
  case HASH_FUN(L1Cache_State_IS_AD, L1Cache_Event_Other_PUTM):
  case HASH_FUN(L1Cache_State_IM_AD, L1Cache_Event_OWN_PUTM):
  case HASH_FUN(L1Cache_State_IM_AD, L1Cache_Event_Other_PUTM):
  case HASH_FUN(L1Cache_State_I, L1Cache_Event_OWN_PUTM):
  case HASH_FUN(L1Cache_State_I, L1Cache_Event_Other_PUTM):
  case HASH_FUN(L1Cache_State_IM_DI, L1Cache_Event_OWN_PUTM):
  case HASH_FUN(L1Cache_State_IM_DI, L1Cache_Event_Other_PUTM):
  case HASH_FUN(L1Cache_State_IS_DI, L1Cache_Event_OWN_PUTM):
  case HASH_FUN(L1Cache_State_IS_DI, L1Cache_Event_Other_PUTM):
  case HASH_FUN(L1Cache_State_IS_D, L1Cache_Event_OWN_PUTM):
  case HASH_FUN(L1Cache_State_IS_D, L1Cache_Event_Other_PUTM):
  case HASH_FUN(L1Cache_State_IM_D, L1Cache_Event_OWN_PUTM):
  case HASH_FUN(L1Cache_State_IM_D, L1Cache_Event_Other_PUTM):
  case HASH_FUN(L1Cache_State_S, L1Cache_Event_OWN_PUTM):
  case HASH_FUN(L1Cache_State_S, L1Cache_Event_Other_PUTM):
  case HASH_FUN(L1Cache_State_S_TI, L1Cache_Event_OWN_PUTM):
  case HASH_FUN(L1Cache_State_S_TI, L1Cache_Event_Other_PUTM):
  case HASH_FUN(L1Cache_State_M, L1Cache_Event_OWN_PUTM):
  case HASH_FUN(L1Cache_State_M, L1Cache_Event_Other_PUTM):
  case HASH_FUN(L1Cache_State_M_TI, L1Cache_Event_OWN_PUTM):
  case HASH_FUN(L1Cache_State_M_TI, L1Cache_Event_Other_PUTM):
  case HASH_FUN(L1Cache_State_S_TM, L1Cache_Event_Other_PUTM):
  case HASH_FUN(L1Cache_State_S_MA, L1Cache_Event_Other_PUTM):
  case HASH_FUN(L1Cache_State_MI_R, L1Cache_Event_Other_PUTM):
  case HASH_FUN(L1Cache_State_MI_A, L1Cache_Event_Other_PUTM):
  case HASH_FUN(L1Cache_State_I, L1Cache_Event_Other_SENDDATA):
  case HASH_FUN(L1Cache_State_IM_D, L1Cache_Event_Other_SENDDATA):
  case HASH_FUN(L1Cache_State_IM_DI, L1Cache_Event_Other_SENDDATA):
  case HASH_FUN(L1Cache_State_IS_D, L1Cache_Event_Other_SENDDATA):
  case HASH_FUN(L1Cache_State_IS_DI, L1Cache_Event_Other_SENDDATA):
  case HASH_FUN(L1Cache_State_IM_AD, L1Cache_Event_Other_SENDDATA):
  case HASH_FUN(L1Cache_State_IS_AD, L1Cache_Event_Other_SENDDATA):
    l_popRequestWBQueue(m_tbe_ptr, m_cache_entry_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(L1Cache_State_S_TI, L1Cache_Event_Timeout_noinv):
  case HASH_FUN(L1Cache_State_S, L1Cache_Event_Timeout_NC):
  case HASH_FUN(L1Cache_State_S, L1Cache_Event_Timeout_CC):
  case HASH_FUN(L1Cache_State_M, L1Cache_Event_Timeout_NC):
  case HASH_FUN(L1Cache_State_M, L1Cache_Event_Timeout_CC):
  case HASH_FUN(L1Cache_State_M_TI, L1Cache_Event_Timeout_noinv):
    unsetcriticalTimer(m_tbe_ptr, m_cache_entry_ptr, addr);
    restartcriticalTimer(m_tbe_ptr, m_cache_entry_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(L1Cache_State_S_TI, L1Cache_Event_Timeout_noinv_noncric):
  case HASH_FUN(L1Cache_State_S, L1Cache_Event_Timeout_NN):
  case HASH_FUN(L1Cache_State_S, L1Cache_Event_Timeout_CN):
  case HASH_FUN(L1Cache_State_M, L1Cache_Event_Timeout_NN):
  case HASH_FUN(L1Cache_State_M, L1Cache_Event_Timeout_CN):
  case HASH_FUN(L1Cache_State_M_TI, L1Cache_Event_Timeout_noinv_noncric):
    unsetnoncriticalTimer(m_tbe_ptr, m_cache_entry_ptr, addr);
    restartnoncriticalTimer(m_tbe_ptr, m_cache_entry_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(L1Cache_State_S, L1Cache_Event_Store):
  case HASH_FUN(L1Cache_State_S, L1Cache_Event_RMW_Read):
  case HASH_FUN(L1Cache_State_S, L1Cache_Event_RMW_Write):
  case HASH_FUN(L1Cache_State_S_TI, L1Cache_Event_Store):
  case HASH_FUN(L1Cache_State_S_TI, L1Cache_Event_RMW_Read):
  case HASH_FUN(L1Cache_State_S_TI, L1Cache_Event_RMW_Write):
    next_state = L1Cache_State_S_TM;
    if (!(*m_TBEs_ptr).areNSlotsAvailable(1))
        return TransitionResult_ResourceStall;
    i_allocateTBE(m_tbe_ptr, m_cache_entry_ptr, addr);
    uu_profileDataMiss(m_tbe_ptr, m_cache_entry_ptr, addr);
    uu_profileUpgradeMiss(m_tbe_ptr, m_cache_entry_ptr, addr);
    setStartTime(m_tbe_ptr, m_cache_entry_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(L1Cache_State_SI_A, L1Cache_Event_Store):
  case HASH_FUN(L1Cache_State_SI_A, L1Cache_Event_RMW_Read):
  case HASH_FUN(L1Cache_State_SI_A, L1Cache_Event_RMW_Write):
    next_state = L1Cache_State_S_MA;
    if (!(*m_TBEs_ptr).areNSlotsAvailable(1))
        return TransitionResult_ResourceStall;
    if (!(*m_requestFromCacheWB_ptr).areNSlotsAvailable(1))
        return TransitionResult_ResourceStall;
    if (!(*m_requestFromCache_ptr).areNSlotsAvailable(1))
        return TransitionResult_ResourceStall;
    clearDest(m_tbe_ptr, m_cache_entry_ptr, addr);
    addDest(m_tbe_ptr, m_cache_entry_ptr, addr);
    issueInv(m_tbe_ptr, m_cache_entry_ptr, addr);
    clearDest(m_tbe_ptr, m_cache_entry_ptr, addr);
    uu_profileDataMiss(m_tbe_ptr, m_cache_entry_ptr, addr);
    uu_profileStoreMiss(m_tbe_ptr, m_cache_entry_ptr, addr);
    i_allocateTBE(m_tbe_ptr, m_cache_entry_ptr, addr);
    setStartTime(m_tbe_ptr, m_cache_entry_ptr, addr);
    bm_issueGETM(m_tbe_ptr, m_cache_entry_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(L1Cache_State_SI, L1Cache_Event_Store):
  case HASH_FUN(L1Cache_State_SI, L1Cache_Event_RMW_Read):
  case HASH_FUN(L1Cache_State_SI, L1Cache_Event_RMW_Write):
    next_state = L1Cache_State_IM_AD;
    if (!(*m_TBEs_ptr).areNSlotsAvailable(1))
        return TransitionResult_ResourceStall;
    if (!(*m_requestFromCache_ptr).areNSlotsAvailable(1))
        return TransitionResult_ResourceStall;
    clearDest(m_tbe_ptr, m_cache_entry_ptr, addr);
    uu_profileDataMiss(m_tbe_ptr, m_cache_entry_ptr, addr);
    uu_profileStoreMiss(m_tbe_ptr, m_cache_entry_ptr, addr);
    i_allocateTBE(m_tbe_ptr, m_cache_entry_ptr, addr);
    setStartTime(m_tbe_ptr, m_cache_entry_ptr, addr);
    bm_issueGETM(m_tbe_ptr, m_cache_entry_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(L1Cache_State_S_TM, L1Cache_Event_Timeout_NC):
  case HASH_FUN(L1Cache_State_S_TM, L1Cache_Event_Timeout_CC):
    next_state = L1Cache_State_S_MA;
    if (!(*m_requestFromCacheWB_ptr).areNSlotsAvailable(1))
        return TransitionResult_ResourceStall;
    if (!(*m_requestFromCache_ptr).areNSlotsAvailable(1))
        return TransitionResult_ResourceStall;
    addDest(m_tbe_ptr, m_cache_entry_ptr, addr);
    issueInv(m_tbe_ptr, m_cache_entry_ptr, addr);
    clearDest(m_tbe_ptr, m_cache_entry_ptr, addr);
    bm_issueGETM(m_tbe_ptr, m_cache_entry_ptr, addr);
    setTime_SM(m_tbe_ptr, m_cache_entry_ptr, addr);
    unsetTimers(m_tbe_ptr, m_cache_entry_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(L1Cache_State_S_TM, L1Cache_Event_Timeout_NN):
  case HASH_FUN(L1Cache_State_S_TM, L1Cache_Event_Timeout_CN):
    next_state = L1Cache_State_S_MA;
    if (!(*m_requestFromCacheWB_ptr).areNSlotsAvailable(1))
        return TransitionResult_ResourceStall;
    if (!(*m_requestFromCache_ptr).areNSlotsAvailable(1))
        return TransitionResult_ResourceStall;
    addDest(m_tbe_ptr, m_cache_entry_ptr, addr);
    issueInv(m_tbe_ptr, m_cache_entry_ptr, addr);
    clearDest(m_tbe_ptr, m_cache_entry_ptr, addr);
    unsetTimers(m_tbe_ptr, m_cache_entry_ptr, addr);
    bm_issueGETM(m_tbe_ptr, m_cache_entry_ptr, addr);
    setTime_SM(m_tbe_ptr, m_cache_entry_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(L1Cache_State_S_MA, L1Cache_Event_ALLINV):
  case HASH_FUN(L1Cache_State_S_MA, L1Cache_Event_Own_SELFINV):
  case HASH_FUN(L1Cache_State_S_MA, L1Cache_Event_Own_SELFINV_SPL):
    next_state = L1Cache_State_IM_AD;
    clearDest(m_tbe_ptr, m_cache_entry_ptr, addr);
    l_popRequestWBQueue(m_tbe_ptr, m_cache_entry_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(L1Cache_State_S_MA, L1Cache_Event_OWN_GETM):
    next_state = L1Cache_State_IM_D;
    clearDest(m_tbe_ptr, m_cache_entry_ptr, addr);
    setFirstScheduledTime(m_tbe_ptr, m_cache_entry_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(L1Cache_State_S_MA, L1Cache_Event_Other_GETM_C):
  case HASH_FUN(L1Cache_State_S_MA, L1Cache_Event_Other_GETM_NC):
  case HASH_FUN(L1Cache_State_SI_A, L1Cache_Event_Other_GETM_C):
  case HASH_FUN(L1Cache_State_SI_A, L1Cache_Event_Other_GETM_NC):
    if (!(*m_requestFromCacheWB_ptr).areNSlotsAvailable(1))
        return TransitionResult_ResourceStall;
    clearDest(m_tbe_ptr, m_cache_entry_ptr, addr);
    addReqDest(m_tbe_ptr, m_cache_entry_ptr, addr);
    issueInv(m_tbe_ptr, m_cache_entry_ptr, addr);
    l_popRequestQueue(m_tbe_ptr, m_cache_entry_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(L1Cache_State_I, L1Cache_Event_Store):
  case HASH_FUN(L1Cache_State_I, L1Cache_Event_RMW_Read):
  case HASH_FUN(L1Cache_State_I, L1Cache_Event_RMW_Write):
    next_state = L1Cache_State_IM_AD;
    if (!(*m_TBEs_ptr).areNSlotsAvailable(1))
        return TransitionResult_ResourceStall;
    if (!(*m_requestFromCache_ptr).areNSlotsAvailable(1))
        return TransitionResult_ResourceStall;
    oo_allocateL1DCacheBlock(m_tbe_ptr, m_cache_entry_ptr, addr);
    i_allocateTBE(m_tbe_ptr, m_cache_entry_ptr, addr);
    uu_profileDataMiss(m_tbe_ptr, m_cache_entry_ptr, addr);
    uu_profileStoreMiss(m_tbe_ptr, m_cache_entry_ptr, addr);
    bm_issueGETM(m_tbe_ptr, m_cache_entry_ptr, addr);
    setStartTime(m_tbe_ptr, m_cache_entry_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(L1Cache_State_IM_AD, L1Cache_Event_OWN_GETM):
    next_state = L1Cache_State_IM_D;
    setFirstScheduledTime(m_tbe_ptr, m_cache_entry_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(L1Cache_State_IM_D, L1Cache_Event_Other_GETM_C):
  case HASH_FUN(L1Cache_State_IM_D, L1Cache_Event_Other_GETS_C):
  case HASH_FUN(L1Cache_State_IM_D, L1Cache_Event_Other_GETI_C):
  case HASH_FUN(L1Cache_State_IM_DI, L1Cache_Event_Other_GETM_C):
  case HASH_FUN(L1Cache_State_IM_DI, L1Cache_Event_Other_GETS_C):
  case HASH_FUN(L1Cache_State_IM_DI, L1Cache_Event_Other_GETI_C):
    next_state = L1Cache_State_IM_AD;
    if (!(*m_requestFromCache_ptr).areNSlotsAvailable(1))
        return TransitionResult_ResourceStall;
    clearDest(m_tbe_ptr, m_cache_entry_ptr, addr);
    bm_issueGETM(m_tbe_ptr, m_cache_entry_ptr, addr);
    l_popRequestQueue(m_tbe_ptr, m_cache_entry_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(L1Cache_State_IM_D, L1Cache_Event_Other_GETM_NC):
  case HASH_FUN(L1Cache_State_IM_D, L1Cache_Event_Other_GETS_NC):
  case HASH_FUN(L1Cache_State_IM_D, L1Cache_Event_Other_GETI_NC):
    next_state = L1Cache_State_IM_DI;
    l_popRequestQueue(m_tbe_ptr, m_cache_entry_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(L1Cache_State_IM_D, L1Cache_Event_Data):
    next_state = L1Cache_State_M;
    u_writeDataToL1Cache(m_tbe_ptr, m_cache_entry_ptr, addr);
    s_store_hit(m_tbe_ptr, m_cache_entry_ptr, addr);
    calcLatencyProfile(m_tbe_ptr, m_cache_entry_ptr, addr);
    setTimers(m_tbe_ptr, m_cache_entry_ptr, addr);
    s_deallocateTBE(m_tbe_ptr, m_cache_entry_ptr, addr);
    kd_wakeUpDependents(m_tbe_ptr, m_cache_entry_ptr, addr);
    ka_wakeUpAllDependents(m_tbe_ptr, m_cache_entry_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(L1Cache_State_IM_DI, L1Cache_Event_Data):
    next_state = L1Cache_State_M_TI;
    u_writeDataToL1Cache(m_tbe_ptr, m_cache_entry_ptr, addr);
    s_store_hit(m_tbe_ptr, m_cache_entry_ptr, addr);
    calcLatencyProfile(m_tbe_ptr, m_cache_entry_ptr, addr);
    setTimers(m_tbe_ptr, m_cache_entry_ptr, addr);
    s_deallocateTBE(m_tbe_ptr, m_cache_entry_ptr, addr);
    kd_wakeUpDependents(m_tbe_ptr, m_cache_entry_ptr, addr);
    ka_wakeUpAllDependents(m_tbe_ptr, m_cache_entry_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(L1Cache_State_M, L1Cache_Event_Store):
  case HASH_FUN(L1Cache_State_M_TI, L1Cache_Event_Store):
  case HASH_FUN(L1Cache_State_MI_A, L1Cache_Event_Store):
  case HASH_FUN(L1Cache_State_MI_R, L1Cache_Event_Store):
  case HASH_FUN(L1Cache_State_M, L1Cache_Event_RMW_Read):
  case HASH_FUN(L1Cache_State_M, L1Cache_Event_RMW_Write):
  case HASH_FUN(L1Cache_State_M_TI, L1Cache_Event_RMW_Read):
  case HASH_FUN(L1Cache_State_M_TI, L1Cache_Event_RMW_Write):
    sx_store_hit(m_tbe_ptr, m_cache_entry_ptr, addr);
    uu_profileDataHit(m_tbe_ptr, m_cache_entry_ptr, addr);
    uu_profileStoreHit(m_tbe_ptr, m_cache_entry_ptr, addr);
    k_popMandatoryQueue(m_tbe_ptr, m_cache_entry_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(L1Cache_State_MI_A, L1Cache_Event_RMW_Read):
  case HASH_FUN(L1Cache_State_MI_A, L1Cache_Event_RMW_Write):
  case HASH_FUN(L1Cache_State_MI_R, L1Cache_Event_RMW_Read):
  case HASH_FUN(L1Cache_State_MI_R, L1Cache_Event_RMW_Write):
  case HASH_FUN(L1Cache_State_IS_AD, L1Cache_Event_Load):
  case HASH_FUN(L1Cache_State_IS_AD, L1Cache_Event_Ifetch):
  case HASH_FUN(L1Cache_State_IS_AD, L1Cache_Event_Store):
  case HASH_FUN(L1Cache_State_IS_AD, L1Cache_Event_Replacement):
  case HASH_FUN(L1Cache_State_IS_AD, L1Cache_Event_RMW_Read):
  case HASH_FUN(L1Cache_State_IS_AD, L1Cache_Event_RMW_Write):
  case HASH_FUN(L1Cache_State_IS_D, L1Cache_Event_Load):
  case HASH_FUN(L1Cache_State_IS_D, L1Cache_Event_Ifetch):
  case HASH_FUN(L1Cache_State_IS_D, L1Cache_Event_Store):
  case HASH_FUN(L1Cache_State_IS_D, L1Cache_Event_Replacement):
  case HASH_FUN(L1Cache_State_IS_D, L1Cache_Event_RMW_Read):
  case HASH_FUN(L1Cache_State_IS_D, L1Cache_Event_RMW_Write):
  case HASH_FUN(L1Cache_State_IS_DI, L1Cache_Event_Load):
  case HASH_FUN(L1Cache_State_IS_DI, L1Cache_Event_Ifetch):
  case HASH_FUN(L1Cache_State_IS_DI, L1Cache_Event_Store):
  case HASH_FUN(L1Cache_State_IS_DI, L1Cache_Event_Replacement):
  case HASH_FUN(L1Cache_State_IS_DI, L1Cache_Event_RMW_Read):
  case HASH_FUN(L1Cache_State_IS_DI, L1Cache_Event_RMW_Write):
  case HASH_FUN(L1Cache_State_IM_AD, L1Cache_Event_Load):
  case HASH_FUN(L1Cache_State_IM_AD, L1Cache_Event_Ifetch):
  case HASH_FUN(L1Cache_State_IM_AD, L1Cache_Event_Store):
  case HASH_FUN(L1Cache_State_IM_AD, L1Cache_Event_Replacement):
  case HASH_FUN(L1Cache_State_IM_AD, L1Cache_Event_RMW_Read):
  case HASH_FUN(L1Cache_State_IM_AD, L1Cache_Event_RMW_Write):
  case HASH_FUN(L1Cache_State_IM_D, L1Cache_Event_Load):
  case HASH_FUN(L1Cache_State_IM_D, L1Cache_Event_Ifetch):
  case HASH_FUN(L1Cache_State_IM_D, L1Cache_Event_Store):
  case HASH_FUN(L1Cache_State_IM_D, L1Cache_Event_Replacement):
  case HASH_FUN(L1Cache_State_IM_D, L1Cache_Event_RMW_Read):
  case HASH_FUN(L1Cache_State_IM_D, L1Cache_Event_RMW_Write):
  case HASH_FUN(L1Cache_State_IM_DI, L1Cache_Event_Load):
  case HASH_FUN(L1Cache_State_IM_DI, L1Cache_Event_Ifetch):
  case HASH_FUN(L1Cache_State_IM_DI, L1Cache_Event_Store):
  case HASH_FUN(L1Cache_State_IM_DI, L1Cache_Event_Replacement):
  case HASH_FUN(L1Cache_State_IM_DI, L1Cache_Event_RMW_Read):
  case HASH_FUN(L1Cache_State_IM_DI, L1Cache_Event_RMW_Write):
  case HASH_FUN(L1Cache_State_MI_R, L1Cache_Event_Replacement):
    z_stallAndWaitMandatoryQueue(m_tbe_ptr, m_cache_entry_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(L1Cache_State_M, L1Cache_Event_Other_GETM_NC):
  case HASH_FUN(L1Cache_State_M, L1Cache_Event_Other_GETS_NC):
  case HASH_FUN(L1Cache_State_M, L1Cache_Event_Other_GETI_NC):
  case HASH_FUN(L1Cache_State_M, L1Cache_Event_Other_GETM_C):
  case HASH_FUN(L1Cache_State_M, L1Cache_Event_Other_GETS_C):
  case HASH_FUN(L1Cache_State_M, L1Cache_Event_Other_GETI_C):
    next_state = L1Cache_State_M_TI;
    l_popRequestQueue(m_tbe_ptr, m_cache_entry_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(L1Cache_State_MI_R, L1Cache_Event_OWN_PUTM):
  case HASH_FUN(L1Cache_State_MI_A, L1Cache_Event_OWN_PUTM):
    next_state = L1Cache_State_I;
    if (!(*m_responseFromCache_ptr).areNSlotsAvailable(1))
        return TransitionResult_ResourceStall;
    cc_sendDataCacheToDir(m_tbe_ptr, m_cache_entry_ptr, addr);
    ff_deallocateL1CacheBlock(m_tbe_ptr, m_cache_entry_ptr, addr);
    l_popRequestWBQueue(m_tbe_ptr, m_cache_entry_ptr, addr);
    ka_wakeUpAllDependents(m_tbe_ptr, m_cache_entry_ptr, addr);
    kd_wakeUpDependents(m_tbe_ptr, m_cache_entry_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(L1Cache_State_M, L1Cache_Event_INV):
    next_state = L1Cache_State_I;
    if (!(*m_responseFromCache_ptr).areNSlotsAvailable(1))
        return TransitionResult_ResourceStall;
    unsetTimers(m_tbe_ptr, m_cache_entry_ptr, addr);
    cc_sendDataCacheToDir(m_tbe_ptr, m_cache_entry_ptr, addr);
    ff_deallocateL1CacheBlock(m_tbe_ptr, m_cache_entry_ptr, addr);
    l_popRequestQueue(m_tbe_ptr, m_cache_entry_ptr, addr);
    ka_wakeUpAllDependents(m_tbe_ptr, m_cache_entry_ptr, addr);
    kd_wakeUpDependents(m_tbe_ptr, m_cache_entry_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(L1Cache_State_IS_AD, L1Cache_Event_INV):
  case HASH_FUN(L1Cache_State_IS_D, L1Cache_Event_INV):
  case HASH_FUN(L1Cache_State_IS_DI, L1Cache_Event_INV):
  case HASH_FUN(L1Cache_State_IM_AD, L1Cache_Event_INV):
  case HASH_FUN(L1Cache_State_IM_D, L1Cache_Event_INV):
  case HASH_FUN(L1Cache_State_IM_DI, L1Cache_Event_INV):
  case HASH_FUN(L1Cache_State_MI_A, L1Cache_Event_INV):
  case HASH_FUN(L1Cache_State_MI_R, L1Cache_Event_INV):
    z_stallAndWaitRequestQueue(m_tbe_ptr, m_cache_entry_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(L1Cache_State_S, L1Cache_Event_INV):
  case HASH_FUN(L1Cache_State_S, L1Cache_Event_Replacement):
    next_state = L1Cache_State_SI_A;
    if (!(*m_requestFromCacheWB_ptr).areNSlotsAvailable(1))
        return TransitionResult_ResourceStall;
    setownfinaldest(m_tbe_ptr, m_cache_entry_ptr, addr);
    issueInv(m_tbe_ptr, m_cache_entry_ptr, addr);
    unsetTimers(m_tbe_ptr, m_cache_entry_ptr, addr);
    z_stallAndWaitMandatoryQueue(m_tbe_ptr, m_cache_entry_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(L1Cache_State_SI_A, L1Cache_Event_INV):
  case HASH_FUN(L1Cache_State_SI_A, L1Cache_Event_Replacement):
    if (!(*m_requestFromCacheWB_ptr).areNSlotsAvailable(1))
        return TransitionResult_ResourceStall;
    setownfinaldest(m_tbe_ptr, m_cache_entry_ptr, addr);
    issueInv(m_tbe_ptr, m_cache_entry_ptr, addr);
    profile_inv_S_repl(m_tbe_ptr, m_cache_entry_ptr, addr);
    unsetTimers(m_tbe_ptr, m_cache_entry_ptr, addr);
    z_stallAndWaitMandatoryQueue(m_tbe_ptr, m_cache_entry_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(L1Cache_State_SI, L1Cache_Event_INV):
  case HASH_FUN(L1Cache_State_SI, L1Cache_Event_Replacement):
    next_state = L1Cache_State_I;
    ff_deallocateL1CacheBlock(m_tbe_ptr, m_cache_entry_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(L1Cache_State_S_TI, L1Cache_Event_INV):
  case HASH_FUN(L1Cache_State_S_TI, L1Cache_Event_Replacement):
    next_state = L1Cache_State_SI_A;
    if (!(*m_requestFromCacheWB_ptr).areNSlotsAvailable(2))
        return TransitionResult_ResourceStall;
    issueInv(m_tbe_ptr, m_cache_entry_ptr, addr);
    setownfinaldest(m_tbe_ptr, m_cache_entry_ptr, addr);
    issueInv(m_tbe_ptr, m_cache_entry_ptr, addr);
    unsetTimers(m_tbe_ptr, m_cache_entry_ptr, addr);
    profile_inv_S_repl(m_tbe_ptr, m_cache_entry_ptr, addr);
    z_stallAndWaitMandatoryQueue(m_tbe_ptr, m_cache_entry_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(L1Cache_State_M, L1Cache_Event_Replacement):
    next_state = L1Cache_State_MI_R;
    if (!(*m_requestFromCacheWB_ptr).areNSlotsAvailable(1))
        return TransitionResult_ResourceStall;
    unsetTimers(m_tbe_ptr, m_cache_entry_ptr, addr);
    bpm_issuePUTM(m_tbe_ptr, m_cache_entry_ptr, addr);
    z_stallAndWaitMandatoryQueue(m_tbe_ptr, m_cache_entry_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(L1Cache_State_M_TI, L1Cache_Event_Replacement):
    next_state = L1Cache_State_MI_R;
    if (!(*m_requestFromCacheWB_ptr).areNSlotsAvailable(2))
        return TransitionResult_ResourceStall;
    unsetTimers(m_tbe_ptr, m_cache_entry_ptr, addr);
    bpm_issuesendData(m_tbe_ptr, m_cache_entry_ptr, addr);
    bpm_issuePUTM(m_tbe_ptr, m_cache_entry_ptr, addr);
    z_stallAndWaitMandatoryQueue(m_tbe_ptr, m_cache_entry_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(L1Cache_State_M_TI, L1Cache_Event_Timeout_NC):
  case HASH_FUN(L1Cache_State_M_TI, L1Cache_Event_Timeout_CC):
  case HASH_FUN(L1Cache_State_M_TI, L1Cache_Event_Timeout_NN):
  case HASH_FUN(L1Cache_State_M_TI, L1Cache_Event_Timeout_CN):
    next_state = L1Cache_State_MI_A;
    if (!(*m_requestFromCacheWB_ptr).areNSlotsAvailable(1))
        return TransitionResult_ResourceStall;
    bpm_issuesendData(m_tbe_ptr, m_cache_entry_ptr, addr);
    unsetTimers(m_tbe_ptr, m_cache_entry_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(L1Cache_State_MI_A, L1Cache_Event_Other_GETM_C):
  case HASH_FUN(L1Cache_State_MI_A, L1Cache_Event_Other_GETS_C):
  case HASH_FUN(L1Cache_State_MI_A, L1Cache_Event_Other_GETI_C):
    if (!(*m_requestFromCacheWB_ptr).areNSlotsAvailable(1))
        return TransitionResult_ResourceStall;
    bpm_issuesendData(m_tbe_ptr, m_cache_entry_ptr, addr);
    l_popRequestQueue(m_tbe_ptr, m_cache_entry_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(L1Cache_State_MI_R, L1Cache_Event_Other_GETM_C):
  case HASH_FUN(L1Cache_State_MI_R, L1Cache_Event_Other_GETS_C):
  case HASH_FUN(L1Cache_State_MI_R, L1Cache_Event_Other_GETI_C):
  case HASH_FUN(L1Cache_State_MI_R, L1Cache_Event_Other_GETM_NC):
  case HASH_FUN(L1Cache_State_MI_R, L1Cache_Event_Other_GETS_NC):
  case HASH_FUN(L1Cache_State_MI_R, L1Cache_Event_Other_GETI_NC):
    next_state = L1Cache_State_MI_A;
    if (!(*m_requestFromCacheWB_ptr).areNSlotsAvailable(1))
        return TransitionResult_ResourceStall;
    setDestmir(m_tbe_ptr, m_cache_entry_ptr, addr);
    bpm_issuesendData(m_tbe_ptr, m_cache_entry_ptr, addr);
    l_popRequestQueue(m_tbe_ptr, m_cache_entry_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(L1Cache_State_MI_A, L1Cache_Event_Own_SENDDATA):
  case HASH_FUN(L1Cache_State_MI_R, L1Cache_Event_Own_SENDDATA):
    next_state = L1Cache_State_I;
    if (!(*m_responseFromCache_ptr).areNSlotsAvailable(1))
        return TransitionResult_ResourceStall;
    profile_inv_M(m_tbe_ptr, m_cache_entry_ptr, addr);
    cc_sendDataCacheToCache(m_tbe_ptr, m_cache_entry_ptr, addr);
    ff_deallocateL1CacheBlock(m_tbe_ptr, m_cache_entry_ptr, addr);
    l_popRequestWBQueue(m_tbe_ptr, m_cache_entry_ptr, addr);
    ka_wakeUpAllDependents(m_tbe_ptr, m_cache_entry_ptr, addr);
    kd_wakeUpDependents(m_tbe_ptr, m_cache_entry_ptr, addr);
    return TransitionResult_Valid;

  case HASH_FUN(L1Cache_State_MI_A, L1Cache_Event_Replacement):
    next_state = L1Cache_State_MI_R;
    if (!(*m_requestFromCacheWB_ptr).areNSlotsAvailable(1))
        return TransitionResult_ResourceStall;
    bpm_issuePUTM(m_tbe_ptr, m_cache_entry_ptr, addr);
    z_stallAndWaitMandatoryQueue(m_tbe_ptr, m_cache_entry_ptr, addr);
    return TransitionResult_Valid;

      default:
        fatal("Invalid transition\n"
              "%s time: %d addr: %s event: %s state: %s\n",
              name(), curCycle(), addr, event, state);
    }

    return TransitionResult_Valid;
}
