/** \file RequestMsg.hh
 *
 *
 * Auto generated C++ code started by /home/mohamed/hourglass/gem5-stable/src/mem/slicc/symbols/Type.py:200
 */

#ifndef __RequestMsg_HH__
#define __RequestMsg_HH__

#include <iostream>

#include "mem/ruby/slicc_interface/RubySlicc_Util.hh"
#include "mem/protocol/Address.hh"
#include "mem/protocol/CoherenceRequestType.hh"
#include "mem/protocol/MachineID.hh"
#include "mem/protocol/NetDest.hh"
#include "mem/protocol/DataBlock.hh"
#include "mem/protocol/MessageSizeType.hh"
#include "mem/protocol/NetDest.hh"
#include "mem/protocol/NetworkMessage.hh"
class RequestMsg :  public NetworkMessage
{
  public:
    RequestMsg
(Tick curTime) : NetworkMessage(curTime) {
        // m_Addr has no default
        m_Type = CoherenceRequestType_NUM; // default value of CoherenceRequestType
        // m_Requestor has no default
        // m_Destination has no default
        // m_DataBlk has no default
        m_MessageSize = MessageSizeType_NUM; // default value of MessageSizeType
        m_Len = 0; // default value of int
        m_isSpecial = false; // default value of bool
        // m_DataDestination has no default
        // m_Data has no default
        // m_DataOffset has no default
        m_DataSize = 0; // default value of int
    }
    RequestMsg(const RequestMsg&other)
        : NetworkMessage(other)
    {
        m_Addr = other.m_Addr;
        m_Type = other.m_Type;
        m_Requestor = other.m_Requestor;
        m_Destination = other.m_Destination;
        m_DataBlk = other.m_DataBlk;
        m_MessageSize = other.m_MessageSize;
        m_Len = other.m_Len;
        m_isSpecial = other.m_isSpecial;
        m_DataDestination = other.m_DataDestination;
        m_Data = other.m_Data;
        m_DataOffset = other.m_DataOffset;
        m_DataSize = other.m_DataSize;
    }
    RequestMsg(const Tick curTime, const Address& local_Addr, const CoherenceRequestType& local_Type, const MachineID& local_Requestor, const NetDest& local_Destination, const DataBlock& local_DataBlk, const MessageSizeType& local_MessageSize, const int& local_Len, const bool& local_isSpecial, const NetDest& local_DataDestination, const uint64& local_Data, const uint64& local_DataOffset, const int& local_DataSize)
        : NetworkMessage(curTime)
    {
        m_Addr = local_Addr;
        m_Type = local_Type;
        m_Requestor = local_Requestor;
        m_Destination = local_Destination;
        m_DataBlk = local_DataBlk;
        m_MessageSize = local_MessageSize;
        m_Len = local_Len;
        m_isSpecial = local_isSpecial;
        m_DataDestination = local_DataDestination;
        m_Data = local_Data;
        m_DataOffset = local_DataOffset;
        m_DataSize = local_DataSize;
    }
    MsgPtr
    clone() const
    {
         return std::shared_ptr<Message>(new RequestMsg(*this));
    }
    // Const accessors methods for each field
    /** \brief Const accessor method for Addr field.
     *  \return Addr field
     */
    const Address&
    getAddr() const
    {
        return m_Addr;
    }
    /** \brief Const accessor method for Type field.
     *  \return Type field
     */
    const CoherenceRequestType&
    getType() const
    {
        return m_Type;
    }
    /** \brief Const accessor method for Requestor field.
     *  \return Requestor field
     */
    const MachineID&
    getRequestor() const
    {
        return m_Requestor;
    }
    /** \brief Const accessor method for Destination field.
     *  \return Destination field
     */
    const NetDest&
    getDestination() const
    {
        return m_Destination;
    }
    /** \brief Const accessor method for DataBlk field.
     *  \return DataBlk field
     */
    const DataBlock&
    getDataBlk() const
    {
        return m_DataBlk;
    }
    /** \brief Const accessor method for MessageSize field.
     *  \return MessageSize field
     */
    const MessageSizeType&
    getMessageSize() const
    {
        return m_MessageSize;
    }
    /** \brief Const accessor method for Len field.
     *  \return Len field
     */
    const int&
    getLen() const
    {
        return m_Len;
    }
    /** \brief Const accessor method for isSpecial field.
     *  \return isSpecial field
     */
    const bool&
    getisSpecial() const
    {
        return m_isSpecial;
    }
    /** \brief Const accessor method for DataDestination field.
     *  \return DataDestination field
     */
    const NetDest&
    getDataDestination() const
    {
        return m_DataDestination;
    }
    /** \brief Const accessor method for Data field.
     *  \return Data field
     */
    const uint64&
    getData() const
    {
        return m_Data;
    }
    /** \brief Const accessor method for DataOffset field.
     *  \return DataOffset field
     */
    const uint64&
    getDataOffset() const
    {
        return m_DataOffset;
    }
    /** \brief Const accessor method for DataSize field.
     *  \return DataSize field
     */
    const int&
    getDataSize() const
    {
        return m_DataSize;
    }
    // Non const Accessors methods for each field
    /** \brief Non-const accessor method for Addr field.
     *  \return Addr field
     */
    Address&
    getAddr()
    {
        return m_Addr;
    }
    /** \brief Non-const accessor method for Type field.
     *  \return Type field
     */
    CoherenceRequestType&
    getType()
    {
        return m_Type;
    }
    /** \brief Non-const accessor method for Requestor field.
     *  \return Requestor field
     */
    MachineID&
    getRequestor()
    {
        return m_Requestor;
    }
    /** \brief Non-const accessor method for Destination field.
     *  \return Destination field
     */
    NetDest&
    getDestination()
    {
        return m_Destination;
    }
    /** \brief Non-const accessor method for DataBlk field.
     *  \return DataBlk field
     */
    DataBlock&
    getDataBlk()
    {
        return m_DataBlk;
    }
    /** \brief Non-const accessor method for MessageSize field.
     *  \return MessageSize field
     */
    MessageSizeType&
    getMessageSize()
    {
        return m_MessageSize;
    }
    /** \brief Non-const accessor method for Len field.
     *  \return Len field
     */
    int&
    getLen()
    {
        return m_Len;
    }
    /** \brief Non-const accessor method for isSpecial field.
     *  \return isSpecial field
     */
    bool&
    getisSpecial()
    {
        return m_isSpecial;
    }
    /** \brief Non-const accessor method for DataDestination field.
     *  \return DataDestination field
     */
    NetDest&
    getDataDestination()
    {
        return m_DataDestination;
    }
    /** \brief Non-const accessor method for Data field.
     *  \return Data field
     */
    uint64&
    getData()
    {
        return m_Data;
    }
    /** \brief Non-const accessor method for DataOffset field.
     *  \return DataOffset field
     */
    uint64&
    getDataOffset()
    {
        return m_DataOffset;
    }
    /** \brief Non-const accessor method for DataSize field.
     *  \return DataSize field
     */
    int&
    getDataSize()
    {
        return m_DataSize;
    }
    // Mutator methods for each field
    /** \brief Mutator method for Addr field */
    void
    setAddr(const Address& local_Addr)
    {
        m_Addr = local_Addr;
    }
    /** \brief Mutator method for Type field */
    void
    setType(const CoherenceRequestType& local_Type)
    {
        m_Type = local_Type;
    }
    /** \brief Mutator method for Requestor field */
    void
    setRequestor(const MachineID& local_Requestor)
    {
        m_Requestor = local_Requestor;
    }
    /** \brief Mutator method for Destination field */
    void
    setDestination(const NetDest& local_Destination)
    {
        m_Destination = local_Destination;
    }
    /** \brief Mutator method for DataBlk field */
    void
    setDataBlk(const DataBlock& local_DataBlk)
    {
        m_DataBlk = local_DataBlk;
    }
    /** \brief Mutator method for MessageSize field */
    void
    setMessageSize(const MessageSizeType& local_MessageSize)
    {
        m_MessageSize = local_MessageSize;
    }
    /** \brief Mutator method for Len field */
    void
    setLen(const int& local_Len)
    {
        m_Len = local_Len;
    }
    /** \brief Mutator method for isSpecial field */
    void
    setisSpecial(const bool& local_isSpecial)
    {
        m_isSpecial = local_isSpecial;
    }
    /** \brief Mutator method for DataDestination field */
    void
    setDataDestination(const NetDest& local_DataDestination)
    {
        m_DataDestination = local_DataDestination;
    }
    /** \brief Mutator method for Data field */
    void
    setData(const uint64& local_Data)
    {
        m_Data = local_Data;
    }
    /** \brief Mutator method for DataOffset field */
    void
    setDataOffset(const uint64& local_DataOffset)
    {
        m_DataOffset = local_DataOffset;
    }
    /** \brief Mutator method for DataSize field */
    void
    setDataSize(const int& local_DataSize)
    {
        m_DataSize = local_DataSize;
    }
    void print(std::ostream& out) const;
  //private:
    /** Physical address for this request */
    Address m_Addr;
    /** Type of request (GetS, GetX, PutX, etc) */
    CoherenceRequestType m_Type;
    /** Node who initiated the request */
    MachineID m_Requestor;
    /** Multicast destination mask */
    NetDest m_Destination;
    /** data for the cache line */
    DataBlock m_DataBlk;
    /** size category of the message */
    MessageSizeType m_MessageSize;
    int m_Len;
    bool m_isSpecial;
    /** destination for cache to cache transfer */
    NetDest m_DataDestination;
    /** data */
    uint64 m_Data;
    /** offset */
    uint64 m_DataOffset;
    /**  size, no of bytes */
    int m_DataSize;
    bool functionalWrite(Packet* param_pkt);
    bool functionalRead(Packet* param_pkt);
};
inline std::ostream&
operator<<(std::ostream& out, const RequestMsg& obj)
{
    obj.print(out);
    out << std::flush;
    return out;
}

#endif // __RequestMsg_HH__
