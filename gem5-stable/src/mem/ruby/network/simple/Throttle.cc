 /*
 * Copyright (c) 1999-2008 Mark D. Hill and David A. Wood
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <cassert>

#include "base/cast.hh"
#include "base/cprintf.hh"
#include "debug/RubyNetwork.hh"
#include "mem/ruby/network/simple/Throttle.hh"
#include "mem/ruby/network/simple/PerfectSwitch.hh"
#include "mem/ruby/network/MessageBuffer.hh"
#include "mem/ruby/network/Network.hh"
#include "mem/ruby/slicc_interface/NetworkMessage.hh"
#include "mem/ruby/system/System.hh"

#include "sim/system.hh"

//niv
#include "mem/protocol/RequestMsg.hh"
#include "mem/protocol/ResponseMsg.hh"

using namespace std;

const int MESSAGE_SIZE_MULTIPLIER = 1000;
//const int BROADCAST_SCALING = 4; // Have a 16p system act like a 64p systems
const int BROADCAST_SCALING = 1;
const int PRIORITY_SWITCH_LIMIT = 128;

//niv
std::vector<Throttle*> Throttle::m_throttlePtrs;


 int network_message_to_size(NetworkMessage* net_msg_ptr);


Throttle::Throttle(int sID, NodeID node, Cycles link_latency,
                   int link_bandwidth_multiplier, int endpoint_bandwidth,
                   ClockedObject *em)
    : Consumer(em)
{
    init(node, link_latency, link_bandwidth_multiplier, endpoint_bandwidth);
    m_sID = sID;	
		doCheck = false;
}

Throttle::Throttle(NodeID node, Cycles link_latency,
                   int link_bandwidth_multiplier, int endpoint_bandwidth,
                   ClockedObject *em)
    : Consumer(em)
{
    init(node, link_latency, link_bandwidth_multiplier, endpoint_bandwidth);
    m_sID = 0;
}

void
Throttle::init(NodeID node, Cycles link_latency,
               int link_bandwidth_multiplier, int endpoint_bandwidth)
{
    m_node = node;
    m_vnets = 0;

    assert(link_bandwidth_multiplier > 0);
    m_link_bandwidth_multiplier = link_bandwidth_multiplier;

    m_link_latency = link_latency;
    m_endpoint_bandwidth = endpoint_bandwidth;

    m_wakeups_wo_switch = 0;
    m_link_utilization_proxy = 0;
}

void
Throttle::addLinks(const vector<MessageBuffer*>& in_vec,
                   const vector<MessageBuffer*>& out_vec)
{
    assert(in_vec.size() == out_vec.size());

    for (int vnet = 0; vnet < in_vec.size(); ++vnet) {
        MessageBuffer *in_ptr = in_vec[vnet];
        MessageBuffer *out_ptr = out_vec[vnet];

        m_vnets++;
        m_units_remaining.push_back(0);
        m_in.push_back(in_ptr);
        m_out.push_back(out_ptr);

        // Set consumer and description
        in_ptr->setConsumer(this);
        string desc = "[Queue to Throttle " + to_string(m_sID) + " " +
            to_string(m_node) + "]";
        in_ptr->setDescription(desc);
    }
}

void
Throttle::checkforDuplicate(MessageBuffer *in, int vnet) {

  //niv
  MsgPtr msg_ptr = NULL;
  int pos = -1;
  DPRINTF(RubyNetwork,"check for duplicate entries: vnet:%s, in :%s\n",vnet, *in);
  for(int j=1; j<in->getQSize(); j++) {
    MsgPtr msg_ptr = in->peekMsgPtr(j);
    RequestMsg* reqmsg = safe_cast<RequestMsg*>(msg_ptr.get());
    ResponseMsg* respmsg = safe_cast<ResponseMsg*>(msg_ptr.get());
    if((vnet == 4 && CoherenceResponseType_to_string(respmsg->m_Type) == "DATAACK" && respmsg->m_DataDestination.smallestNodeID() < NPROC_C) || (vnet == 6 && CoherenceRequestType_to_string(reqmsg->m_Type) == "SENDDATA" && reqmsg->m_DataDestination.smallestNodeID() < NPROC_C)) {
      DPRINTF(RubyNetwork,"found a critical DATAACK or SENDDATA\n");
      pos = j;

      if(vnet == 6 && pos>0) {
	RequestMsg* reqmsg = safe_cast<RequestMsg*>(in->peekMsgPtr(pos).get());
	for(int i=0; i<pos ; i++) {
	  DPRINTF(RubyNetwork,"datadestination :%d\n", reqmsg->m_DataDestination.smallestNodeID());
	  RequestMsg* reqmsg2 = safe_cast<RequestMsg*>(in->peekMsgPtr(i).get());
	  if(in->isReady(i) && reqmsg2->m_Type == reqmsg->m_Type && reqmsg2->m_Addr == reqmsg->m_Addr && reqmsg2->m_DataDestination.smallestNodeID() >= NPROC_C) {
	    DPRINTF(RubyNetwork,"deleting message : %s \n",*reqmsg2);
	    in->erasePacket(i);
	    break;
	  }
	}
	
      }
      else if (vnet == 4 && pos>0) {
	ResponseMsg* respmsg = safe_cast<ResponseMsg*>(in->peekMsgPtr(pos).get());
	for(int i=0; i<pos ; i++) {
	  DPRINTF(RubyNetwork,"datadestination :%d\n", respmsg->m_DataDestination.smallestNodeID());
	  ResponseMsg* respmsg2 = safe_cast<ResponseMsg*>(in->peekMsgPtr(i).get());
	  if(in->isReady(i) && respmsg2->m_Type == respmsg->m_Type && respmsg2->m_Addr == respmsg->m_Addr && respmsg2->m_DataDestination.smallestNodeID() >= NPROC_C) {
	    if(respmsg->m_isSpecial) {
	      //critical one is getm; then delete the nc before this
	      DPRINTF(RubyNetwork,"deleting message : %s \n",*respmsg2);
	      in->erasePacket(i);
	      break;
	    }
	    else if(respmsg2->m_isSpecial) {
	      //critical is gets; then delete only if nc before this is getm
	      DPRINTF(RubyNetwork,"deleting message : %s \n",*respmsg2);
	      in->erasePacket(i);
	      break;
	    }
	  }
	}
      }
    }
  }  
}

void
Throttle::deleteDataRepeated(MessageBuffer *in) {
  DPRINTF(RubyNetwork,"deleteDataRepeated\n");
  MessageBuffer *demandQueue = m_in[2];
  bool flag = false;
  for(int i = 0; i<in->getQSize(); i++) {
    DPRINTF(RubyNetwork,"deleteDataRepeated1 demandqueue size: %d\n",demandQueue->getQSize());
    ResponseMsg* respmsg = safe_cast<ResponseMsg*>(in->peekMsgPtr(i).get());
    for(int j= 0; j<demandQueue->getQSize(); j++) {
      RequestMsg* reqmsg = safe_cast<RequestMsg*>(demandQueue->peekMsgPtr(j).get());     
      if(in->isReady(i) && CoherenceResponseType_to_string(respmsg->m_Type) == "DATAACK" && reqmsg->m_Addr == respmsg->m_Addr && respmsg->m_DataDestination.smallestNodeID() == reqmsg->m_Requestor.getNum()) {
	DPRINTF(RubyNetwork,"deleting message : %s \n",*respmsg);
	in->erasePacket(i);
	flag = true;
	break;
      }
    }
    if(flag) {
      break;
    }
  }
}

void
Throttle::deleteInvforOld(MessageBuffer *in) {
  DPRINTF(RubyNetwork,"deleteinvforold\n");
  MessageBuffer *demandQueue = m_in[2];
  bool flag = false;
  for(int i = 0; i<in->getQSize(); i++) {
    RequestMsg* reqmsg = safe_cast<RequestMsg*>(in->peekMsgPtr(i).get());
    for(int j= 0; j<demandQueue->getQSize(); j++) {
      RequestMsg* demandreqmsg = safe_cast<RequestMsg*>(demandQueue->peekMsgPtr(j).get());  
      //deleting INV which were sent, but those requests were reissued; so delete inv; it will be sent again when the requests are broadcasted
      if(in->isReady(i) && CoherenceRequestType_to_string(reqmsg->m_Type) == "SELFINV" && reqmsg->m_Addr == demandreqmsg->m_Addr && reqmsg->m_DataDestination.isElement(demandreqmsg->m_Requestor) && reqmsg->m_Requestor != demandreqmsg->m_Requestor) {
	reqmsg->m_DataDestination.remove(demandreqmsg->m_Requestor);
	DPRINTF(RubyNetwork,"datadest : %s\n",reqmsg->m_DataDestination);
	if(reqmsg->m_DataDestination.isEmpty()) {
	  DPRINTF(RubyNetwork,"deleting message : %s \n",*reqmsg);
	  in->erasePacket(i);
	  flag = true;
	  break;
	}
      }
      //deleting INV if it is for own requests (store/replacement) and see a new demand request in queue for the same requestor; this means that inv is already propagated (as it is inorder core and so delete this duplicate)
      else if(in->isReady(i) && CoherenceRequestType_to_string(reqmsg->m_Type) == "SELFINV" && reqmsg->m_Addr != demandreqmsg->m_Addr && reqmsg->m_DataDestination.isElement(reqmsg->m_Requestor) && reqmsg->m_Requestor == demandreqmsg->m_Requestor) {
	in->erasePacket(i);
	flag = true;
	break;
      }
      //deleting INV if it is for other requests (store) and see a new demand request which is not the same address is issued for the requestor for which inv is present; this means that the store for which inv is sent is already completed (as it is inorder core and so delete this duplicate)                                                                                                                                                                                       
      else if(in->isReady(i) && CoherenceRequestType_to_string(reqmsg->m_Type) == "SELFINV" && reqmsg->m_Addr != demandreqmsg->m_Addr && reqmsg->m_DataDestination.isElement(demandreqmsg->m_Requestor) && reqmsg->m_Requestor != demandreqmsg->m_Requestor) {
	in->erasePacket(i);
	flag = true;
	break;
      }
      
    }
    if(flag) {
      break;
    }
  }
}

void
Throttle::deleteoldPutM(MessageBuffer *in) {
  DPRINTF(RubyNetwork,"deleteoldPutM\n");
  MessageBuffer *demandQueue = m_in[2];
  bool flag = false;
  for(int i = 0; i<in->getQSize(); i++) {
    RequestMsg* reqmsg = safe_cast<RequestMsg*>(in->peekMsgPtr(i).get());
    for(int j= 0; j<demandQueue->getQSize(); j++) {
      RequestMsg* demandreqmsg = safe_cast<RequestMsg*>(demandQueue->peekMsgPtr(j).get());   
      if(in->isReady(i) && CoherenceRequestType_to_string(reqmsg->m_Type) == "PUTM" && reqmsg->m_Requestor == demandreqmsg->m_Requestor) {
	DPRINTF(RubyNetwork,"deleting message : %s \n",*reqmsg);
	in->erasePacket(i);
	flag = true;
	break;
      }
    }
    if(flag) {
      break;
    }
  }
}

void
Throttle::deleteRepeated(MessageBuffer *in, MsgPtr msg_ptr) {

  DPRINTF(RubyNetwork,"deleteRepeated\n");
  RequestMsg* reqmsg = safe_cast<RequestMsg*>(msg_ptr.get());
  for(int i = 0; i<in->getQSize();) {
    RequestMsg* reqmsg2 = safe_cast<RequestMsg*>(in->peekMsgPtr(i).get());
    if(in->isReady(i) && (reqmsg2->m_Addr == reqmsg->m_Addr && reqmsg2->m_Requestor == reqmsg->m_Requestor) && (CoherenceRequestType_to_string(reqmsg->m_Type) == "SENDDATA" || CoherenceRequestType_to_string(reqmsg->m_Type) == "PUTM")) {
      DPRINTF(RubyNetwork,"deleting message : %s \n",*reqmsg2);
      in->erasePacket(i);
    }
    else {
      i++;
    }
    
  }
}

void
Throttle::deleteRepeatedInv(MsgPtr msg_ptr, MessageBuffer *in) {
  
  DPRINTF(RubyNetwork,"deleteRepeatedInv\n");
  RequestMsg* reqmsg = safe_cast<RequestMsg*>(msg_ptr.get());
  if(CoherenceRequestType_to_string(reqmsg->m_Type) == "SELFINV") {
    for(int i=0; i<in->getQSize();) {
      RequestMsg* reqmsg2 = safe_cast<RequestMsg*>(in->peekMsgPtr(i).get());
      if(in->isReady(i) && reqmsg2->m_Addr == reqmsg->m_Addr && reqmsg2->m_Requestor == reqmsg->m_Requestor) {
	DPRINTF(RubyNetwork,"deleting inv message : %s \n",*reqmsg2);
	in->erasePacket(i);
      }
      else {
	i++;
      }
    }
  }
  else if(CoherenceRequestType_to_string(reqmsg->m_Type) == "ALLINV") {
    for(int i=0; i<m_throttlePtrs.size(); i++) {
      Throttle *throttleptr = m_throttlePtrs[i];
      MessageBuffer *wbQueue = throttleptr->m_in[6];
      DPRINTF(RubyNetwork,"wb queue : %s\n", *wbQueue);
      for(int j=0; j<wbQueue->getQSize();) {
	RequestMsg* reqmsg2 = safe_cast<RequestMsg*>(wbQueue->peekMsgPtr(j).get());
	if(wbQueue->isReady(j) && reqmsg2->m_Addr == reqmsg->m_Addr && CoherenceRequestType_to_string(reqmsg2->m_Type) == "SELFINV") {
	  DPRINTF(RubyNetwork,"deleting inv message : %s \n",*reqmsg2);
	  wbQueue->erasePacket(j);
	}
	else {
	  j++;
	}
      }
    }
    
  }

}



void
Throttle::checkforPendingData() {
  DPRINTF(RubyNetwork,"checkforPendingData\n");
  bool flag = false;
  MachineID currentownerID = {MachineType_L1Cache, (unsigned int)currentOwner};
  DPRINTF(RubyNetwork,"currentownerid : %s \n", currentownerID);
  for(int i=0; i<m_throttlePtrs.size(); i++) {
    //to check if any data from dir is pending to be sent to any critical or non critical core
    Throttle *throttleptr = m_throttlePtrs[i];
    MessageBuffer *responseQueue = throttleptr->m_in[4];
    DPRINTF(RubyNetwork,"response queue : %s\n", *responseQueue);
    for(int j=0; j<responseQueue->getQSize();j++) {
      // NetworkMessage* net_msg_ptr = safe_cast<NetworkMessage*>(responseQueue->peekMsgPtr(j).get());
      ResponseMsg* respmsg = safe_cast<ResponseMsg*>(responseQueue->peekMsgPtr(j).get());
      //checking if the ack's destination is the owner
      if(respmsg->m_DataDestination.isElement(currentownerID)) {
	DPRINTF(RubyNetwork,"found a pending data response\n");
	isServiced = true;
	isDataSlot = true;
	flag = true;
	break;
      }
    }
    if(flag == true) {
      break;
    }
  }
  
}

bool
Throttle::isDataPendingInOtherQueues() {
  DPRINTF(RubyNetwork,"isDataPending\n");
  for(int i=0; i<m_throttlePtrs.size(); i++) {
    //to check if any data from dir is pending to be sent to any critical or non critical core
    Throttle *throttleptr = m_throttlePtrs[i];
    MessageBuffer *responseQueue = throttleptr->m_in[4];
    DPRINTF(RubyNetwork,"response queue : %s\n", *responseQueue);
    if(responseQueue->getQSize() > 0) {
      return true;
    }
  }
  return false;
}

void
Throttle::serviceRequest(MessageBuffer *in, MessageBuffer *out, int vnet, int pos) {
	MsgPtr msg_ptr = NULL;
	//niv
	DPRINTF(RubyNetwork,"Throttle::serviceRequest: SID: %d, in: %s, out: %s, vnet:%s, pos :%d, m_link_latency %s\n",this->returnSID(), *in, *out, vnet, pos, m_link_latency);
	int SID = this->returnSID();

	if (pos == -1) {
		msg_ptr = in->peekMsgPtr();
	}
	else {
		msg_ptr = in->peekMsgPtr(pos);
	}

	NetworkMessage* net_msg_ptr = safe_cast<NetworkMessage*>(msg_ptr.get());
	RequestMsg* reqmsg = safe_cast<RequestMsg*>(msg_ptr.get());


	m_units_remaining[vnet] += network_message_to_size(net_msg_ptr);

	DPRINTF(RubyNetwork, "net_msg_ptr: %s\n", *msg_ptr);

	if (pos == -1) {
		in->dequeue();
	}
	else {
		in->dequeue(pos);
	}

	out->enqueue(msg_ptr, m_link_latency);
	//deleting the invalid responses from the queue
	if(SID == (NPROC+1) && vnet == 6 && (CoherenceRequestType_to_string(reqmsg->m_Type) == "SENDDATA" ||  CoherenceRequestType_to_string(reqmsg->m_Type) == "PUTM")) {
	  deleteRepeated(in, msg_ptr);
	}

	if(SID == (NPROC+1) && vnet == 6 && (CoherenceRequestType_to_string(reqmsg->m_Type) == "SELFINV" || CoherenceRequestType_to_string(reqmsg->m_Type) == "ALLINV")) {
	  deleteRepeatedInv(msg_ptr,in);
	}

	m_msg_counts[net_msg_ptr->getMessageSize()][vnet]++;
}


void
Throttle::operateVnet(int vnet, int &bw_remaining, bool &schedule_wakeup,
                      MessageBuffer *in, MessageBuffer *out, bool recheck)
{

  DPRINTF(RubyNetwork,"operatevnet: vnet:%s, bw_remaining: %d,currentOwner:%d, isDataSlot:%d, schedule wakeup: %d, in: %s \n",vnet,bw_remaining,currentOwner, isDataSlot,schedule_wakeup, *in);
		bool slotStart = isStartOfSlot();
		bool slotStartplus2 = isStartOfSlot_2();

		MessageBuffer *wbQueue = m_in[6];
		MessageBuffer *demandQueue = m_in[2];	        
		
		if (out == nullptr || in == nullptr) {
		  return;
		}
		
		//niv
		if(this->returnSID() == (NPROC+1) && vnet==4) {
		  if(isDataPendingInOtherQueues()) {
		    schedule_wakeup = true;
		  }
		}
		if(this->returnSID() == (NPROC+1) && slotStart && vnet==4 && !isDataSlot) {
		  // checking if not serviced by a request in vnet 6
		  checkforPendingData();
		}
		//	DPRINTF(RubyNetwork,"before while m_units_remaining[vnet:%s]: %d, out->areNSlotsAvailable(1): %s\n",vnet,m_units_remaining[vnet], out->areNSlotsAvailable(1));
		assert(m_units_remaining[vnet] >= 0);	
		while (bw_remaining > 0 && (in->isReady() || m_units_remaining[vnet] > 0) &&
		       out->areNSlotsAvailable(1)) {
		  
		  PerfectSwitch* out_consumerSwitch = dynamic_cast<PerfectSwitch*>(out->getConsumer());
		  Throttle* in_consumerThrottle = safe_cast<Throttle*>(in->getConsumer());
		   		  
		  if (out_consumerSwitch && in_consumerThrottle) {
		    //niv
		    DPRINTF(RubyNetwork,"out_consumerswitch: %s, inconsumerthrottle %s\n",*out_consumerSwitch,*in_consumerThrottle);
		    SwitchID switchID = out_consumerSwitch->returnSwitchID(); // Consider only switchID for central arbiter
		    int SID = in_consumerThrottle->returnSID(); // Consider only SIDs of requestors
		    
		    DPRINTF(RubyNetwork, "slotOwner1: %s, slotStart: %s, switchID: %s, SID: %s, vnet: %s, *this: %s, this: %s\n", currentOwner, slotStart, switchID, SID, vnet, *this, this);
		    bool vnetMatch = false;

		    if (BASELINE) {
		      if (vnet == 2) vnetMatch = true;
		    }
		    else {
		      if (vnet == 6 || vnet == 2) vnetMatch = true;
		    }
		    
		    if (switchID == (NPROC+1) && SID < NPROC && vnetMatch) {
		      // This is for cases when the core sends a request (demand or WB) to the arbiter 
		      //niv
		      bool break_now = false;
		      if(vnet == 2 && in->isReady()) {
			//check if it is GETM and if that getm requestor has sent another inv in vnet 6
			RequestMsg* reqmsg = safe_cast<RequestMsg*>(in->peekMsgPtr().get());
			if(CoherenceRequestType_to_string(reqmsg->m_Type) == "GETM") {
			  for(int i = 0; i<wbQueue->getQSize();i++) {
			    RequestMsg* wbmsg = safe_cast<RequestMsg*>(wbQueue->peekMsgPtr(i).get());
			    if(CoherenceRequestType_to_string(wbmsg->m_Type) == "SELFINV" && reqmsg->m_Requestor == wbmsg->m_Requestor && reqmsg->m_Addr == wbmsg->m_Addr) {
			      DPRINTF(RubyNetwork, "dont service now\n");
			      break_now = true;
			      break;
			    }
			  }
			}
		      }
		      if(break_now) {
			schedule_wakeup = true;
			break;
		      }
		      if (m_units_remaining[vnet] == 0 && in->isReady()) {
			serviceRequest(in, out, vnet, -1);
			int diff = m_units_remaining[vnet] - bw_remaining;
			m_units_remaining[vnet] = max(0, diff);
			bw_remaining = max(0, -diff);
			if (in->isReady()) {
			  schedule_wakeup = true;
			}
			break;
		      }		    
		    } 
		    else if (switchID == (NPROC+1) && SID == NPROC) {		     
		      int pos = -1;
		      DPRINTF(RubyNetwork, "QSIZE: %s\n", in->getQSize());
		      for (int i = 0; i<in->getQSize(); i++) {
			DPRINTF(RubyNetwork, "RECEIVING SOMETHING FROM SHARED MEM\n");
			MsgPtr msg_ptr = in->peekMsgPtr(i);
			NetworkMessage* net_msg_ptr = safe_cast<NetworkMessage*>(msg_ptr.get());
			DPRINTF(RubyNetwork, "destination: %s\n", net_msg_ptr->getDestination());			
		      }		     
		      
		      if (in->isReady()) {
			DPRINTF(RubyNetwork, "SERVICING REQUEST, pos: %d, vnet:%s\n",pos, vnet);
			serviceRequest(in, out, vnet, pos);
			isServiced = true;
		      }
		      int diff = m_units_remaining[vnet] - bw_remaining;
		      m_units_remaining[vnet] = max(0, diff);
		      bw_remaining = max(0, -diff);
		      
		      if (in->isReady()) {
			schedule_wakeup = true;
		      }		      
		      break;
		    }
		     
		    // niv fixed priority and TDM
		    // shared bus
		    else if(SID == (NPROC+1)) {
		      DPRINTF(RubyNetwork,"SID ==5, slotstart: %d \n", slotStart);
		      DPRINTF(RubyNetwork, "QSIZE: %s\n", in->getQSize());

		      //data from shared memory/owner core sent
		      if(vnet == 4) {
			//data responses
			deleteDataRepeated(in);
			if(slotStart && !isDataSlot) {
			  // checking if not serviced by a request in vnet 6
			  //check for pending ack
			  checkforPendingData();
			}
			MachineID currentownerID = {MachineType_L1Cache, (unsigned int)currentOwner};
			DPRINTF(RubyNetwork,"currentownerid : %s \n", currentownerID);
			
			checkforDuplicate(in, vnet);
			
			int pos = -1;
			for(int i=0; i<in->getQSize(); i++) {
			  MsgPtr msg_ptr = in->peekMsgPtr(i);
			  NetworkMessage* net_msg_ptr = safe_cast<NetworkMessage*>(msg_ptr.get());
			  ResponseMsg* respmsg = safe_cast<ResponseMsg*>(msg_ptr.get());
			  DPRINTF(RubyNetwork, "destination: %s, respmsg->sender:%s \n", net_msg_ptr->getDestination(),MachineIDToString(respmsg->m_Sender));
			  //data sent by dir for ISAD and IMAD or data sent by another core || data to write back after putm
			  if((!slotStart && net_msg_ptr->getDestination().isElement(currentownerID) && isDataSlot) || (!slotStart && isDataSlot && respmsg->m_Sender.getNum() == currentOwner && CoherenceResponseType_to_string(respmsg->m_Type) == "DATA_TO_WB")) {
			    pos = i;
			    break;
			  }
			  else if (!slotStart && respmsg->m_DataDestination.isElement(currentownerID)) {
			    pos = i;
			    isDataSlot = true;
			    break;
			  }
			}			
			if(pos != -1) {
			  if (in->isReady(pos)) {
			    DPRINTF(RubyNetwork, "SERVICING REQUEST, pos: %d, vnet:%s\n",pos, vnet);
			    isServiced = true;
			    serviceRequest(in, out, vnet, pos);
			  }
			  int diff = m_units_remaining[vnet] - bw_remaining;
			  m_units_remaining[vnet] = max(0, diff);
			  bw_remaining = max(0, -diff);
			  
			  if (in->isReady()) {
			    DPRINTF(RubyNetwork,"schedule wakeup  \n");
			    schedule_wakeup = true;
			    break;
			  }
			}
			else {
			  schedule_wakeup = true;
			  break;
			}	
		      }
		      else if(vnet == 2) {
			//pending request messages - GETM, GETS 
			DPRINTF(RubyNetwork,"isDataSlot: %d, isInvSharerSlot: %d\n",isDataSlot, isInvSharerSlot);
			int pos = -1;
			for (int i = 0; i<in->getQSize(); i++) {
			  DPRINTF(RubyNetwork, "broadcasting for GETM, GETS\n");
			  MsgPtr msg_ptr = in->peekMsgPtr(i);
			  NetworkMessage* net_msg_ptr = safe_cast<NetworkMessage*>(msg_ptr.get());
			  RequestMsg* reqmsg = safe_cast<RequestMsg*>(msg_ptr.get());
			  DPRINTF(RubyNetwork, "destination: %s\n", net_msg_ptr->getDestination());
			  DPRINTF(RubyNetwork, "requestor: %d \n", reqmsg->m_Requestor.getNum());
			  if(currentOwner == reqmsg->m_Requestor.getNum()) {
			    //for GETM/ GETS
			    if(slotStart && !isInvSharerSlot && !isServiced) {
			      pos = i;
			      isDataSlot = true;
			      break;
			    }
			    else if(slotStartplus2 && isStoreUpdateSlot && CoherenceRequestType_to_string(reqmsg->m_Type) == "GETM") {
			      //allowing a getm after invsharer for the same address
			      DPRINTF(RubyNetwork,"startslot + 2cycles\n");
			      pos = i;
			      isDataSlot = true;
			      isInvSharerSlot = false;
			      isStoreUpdateSlot = false;
			      break;
			    }
			  }
			}			  
			if(pos != -1) {
			  if (in->isReady(pos)) {
			    DPRINTF(RubyNetwork, "SERVICING REQUEST, pos: %d, vnet:%s\n",pos, vnet);
			    isServiced = true;
			    serviceRequest(in, out, vnet, pos);   
			  }
			  int diff = m_units_remaining[vnet] - bw_remaining;
			  m_units_remaining[vnet] = max(0, diff);
			  bw_remaining = max(0, -diff);
			  
			  if (in->isReady()) {
			    schedule_wakeup = true;
			    break;
			  }
			}
			else {
			  schedule_wakeup = true;
			  break;
			}	
		      }
		      else if(vnet == 6) {
			//Pending response - self-invalidation messages and SendData messages
			deleteInvforOld(in);
			deleteoldPutM(in);
			checkforDuplicate(in, vnet);
			int pos = -1;
			MachineID ownerID = {MachineType_L1Cache, (unsigned int)currentOwner};
			DPRINTF(RubyNetwork,"owner : %d\n",currentOwner);
			for (int i = 0; i<in->getQSize(); i++) {
			  DPRINTF(RubyNetwork, "broadcasting\n");
			  MsgPtr msg_ptr = in->peekMsgPtr(i);
			  NetworkMessage* net_msg_ptr = safe_cast<NetworkMessage*>(msg_ptr.get());
			  RequestMsg* reqmsg = safe_cast<RequestMsg*>(msg_ptr.get());
			  DPRINTF(RubyNetwork, "destination: %s\n", net_msg_ptr->getDestination());
			  DPRINTF(RubyNetwork, "requestor: %d, reqmsg->type: %s \n", reqmsg->m_Requestor.getNum(), CoherenceRequestType_to_string(reqmsg->m_Type));
			  if(slotStart) {
			    if((currentOwner == reqmsg->m_Requestor.getNum() && CoherenceRequestType_to_string(reqmsg->m_Type) == "PUTM") || (reqmsg->m_DataDestination.isElement(ownerID) && CoherenceRequestType_to_string(reqmsg->m_Type) == "SENDDATA")) {
			      pos = i;
			      isDataSlot = true;
			      break;
			    }
			    else if (reqmsg->m_DataDestination.isElement(ownerID) && CoherenceRequestType_to_string(reqmsg->m_Type) == "SELFINV") {
			      pos = i;
			      isDataSlot = true;
			      //pending invsharer messages
			      for(int i = pos+1; i<in->getQSize();i++) {
				RequestMsg* reqmsg2 = safe_cast<RequestMsg*>(in->peekMsgPtr(i).get());
				if(reqmsg->m_Addr == reqmsg2->m_Addr && reqmsg->m_Type == reqmsg2->m_Type && reqmsg->m_Requestor != reqmsg2->m_Requestor) {
				  isInvSharerSlot = true;
				  break;
				}				  
			      }
			      DPRINTF(RubyNetwork, "demandqueue: %d\n",demandQueue->getQSize());
			      for(int i = 0; i<demandQueue->getQSize();i++) {
				RequestMsg* reqmsg2 = safe_cast<RequestMsg*>(demandQueue->peekMsgPtr(i).get());
				DPRINTF(RubyNetwork, "requestor: %d, reqmsg->type: %s \n", reqmsg2->m_Requestor.getNum(), CoherenceRequestType_to_string(reqmsg2->m_Type));
				if(reqmsg->m_Addr == reqmsg2->m_Addr && CoherenceRequestType_to_string(reqmsg2->m_Type) == "GETM" && reqmsg->m_Requestor == reqmsg2->m_Requestor) {
				  //to allow invsharer and getm together for cores which move from STM to IMAD; they issue bith inv and getm at the same slot
				  isStoreUpdateSlot = true;
				  DPRINTF(RubyNetwork, "setting store update to true \n");
				  break;
				}				  
			      }
			      break;
			    }
			  }
			  else {
			    if(reqmsg->m_DataDestination.isElement(ownerID) && CoherenceRequestType_to_string(reqmsg->m_Type) == "ALLINV") {
			      pos=i;
			      break;
			    }
			    //sending multiple invsharers in the same slot
			    else if(reqmsg->m_DataDestination.isElement(ownerID) && CoherenceRequestType_to_string(reqmsg->m_Type) == "SELFINV" && isInvSharerSlot) {
			      pos=i;
			      isInvSharerSlot = false;
			      //checking if pending invsharer is available or not
			      for(int i = pos+1; i<in->getQSize();i++) {
				RequestMsg* reqmsg2 = safe_cast<RequestMsg*>(in->peekMsgPtr(i).get());
				if(reqmsg->m_Addr == reqmsg2->m_Addr && reqmsg->m_Type == reqmsg2->m_Type && reqmsg->m_Requestor != reqmsg2->m_Requestor) {
				  isInvSharerSlot = true;
				  break;
				}
			      }
			      break;
			    }		      
			  }
			}
			if(pos != -1) {
			  if (in->isReady(pos)) {
			    DPRINTF(RubyNetwork, "SERVICING REQUEST, pos: %d, vnet:%s\n",pos, vnet);
			    isServiced = true;
			    serviceRequest(in, out, vnet, pos);
			  }
			  int diff = m_units_remaining[vnet] - bw_remaining;
			  m_units_remaining[vnet] = max(0, diff);
			  bw_remaining = max(0, -diff);
			  
			  if (in->isReady()) {
			    schedule_wakeup = true;
			    break;
			  }
			}
			else {
			  schedule_wakeup = true;
			  break;
			}
		      }
		      else {
			//vnets other than 2,4,6
			if (in->isReady()) {
			  schedule_wakeup = true;
			  break;
			}
		      }
		    }		    
		    else {
		      // All other cases

		      if (m_units_remaining[vnet] == 0 && in->isReady()) {
			serviceRequest(in, out, vnet, -1);
		      }
		      int diff = m_units_remaining[vnet] - bw_remaining;
		      m_units_remaining[vnet] = max(0, diff);
		      bw_remaining = max(0, -diff);
		      
		      if (in->isReady()) {
			schedule_wakeup = true;
		      }
		      break;
		    }
		  }
		  else {
		    // Default case 
		    DPRINTF(RubyNetwork,"vnet other than 2 and 6\n");
		    if (m_units_remaining[vnet] == 0 && in->isReady()) {
		      serviceRequest(in, out, vnet, -1);
		    }
		    int diff = m_units_remaining[vnet] - bw_remaining;
		    m_units_remaining[vnet] = max(0, diff);
		    bw_remaining = max(0, -diff);
		    
		    if (in->isReady()) {
		      schedule_wakeup = true;
		    }
		    break;
		  }
		}
		
		if (bw_remaining > 0 && (in->isReady() || m_units_remaining[vnet] > 0) &&
		    !out->areNSlotsAvailable(1)) {
		  DPRINTF(RubyNetwork, "vnet: %d", vnet);
		  
		  // schedule me to wakeup again because I'm waiting for my
		  // output queue to become available				
		  schedule_wakeup = true;
		}
}


void
Throttle::wakeup()
{
    // Limits the number of message sent to a limited number of bytes/cycle.
    assert(getLinkBandwidth() > 0);
    int bw_remaining = getLinkBandwidth();

    m_wakeups_wo_switch++;
    bool schedule_wakeup = false;

    // variable for deciding the direction in which to iterate
    bool iteration_direction = false;
		
    //niv
    DPRINTF(RubyNetwork, "iteration_direction: %s, m_vnets: %d,\n", iteration_direction,m_vnets);
    for(int i= 0; i < m_throttlePtrs.size(); i++) {
      DPRINTF(RubyNetwork,"throttleptrs[%d]: %s\n",i,*m_throttlePtrs[i]);
    }

    
    if (iteration_direction) {
        for (int vnet = 0; vnet < m_vnets; ++vnet) {
            operateVnet(vnet, bw_remaining, schedule_wakeup,
                        m_in[vnet], m_out[vnet], false);
        }
    } else {
      //always false for predictable arbiter
      int slotOwner1;
	slotOwner1 = getSlotOwner1();
      bool slotStart = isStartOfSlot();
      if(slotStart && this->returnSID() == (NPROC+1)) {
	isDataSlot = false;
	isInvSharerSlot = false;
	isStoreUpdateSlot = false;
	currentOwner = slotOwner1;
	isServiced = false;
	//servicing for current critical core owner based on slot owner
	for (int vnet = m_vnets-1; vnet >= 0; --vnet) {
	  operateVnet(vnet, bw_remaining, schedule_wakeup,
		      m_in[vnet], m_out[vnet], false);
	}
	
	int owner_nc = slotOwner2;
	while(!isServiced && ALL_CRITICAL==0) {
	  //if no critical core is serviced, then check for non-critical cores in round robin
	  currentOwner = slotOwner2;
	  slotOwner2++;
	  if(slotOwner2 == NPROC) {
	    slotOwner2 = NPROC_C;
	  }
	  for (int vnet = m_vnets-1; vnet >= 0; --vnet) {
	    operateVnet(vnet, bw_remaining, schedule_wakeup,
			m_in[vnet], m_out[vnet], false);
	  }
	  if(slotOwner2 == owner_nc) {
	    break;
	  }
	}

      }
      else {
	for (int vnet = m_vnets-1; vnet >= 0; --vnet) {
	  operateVnet(vnet, bw_remaining, schedule_wakeup,
		      m_in[vnet], m_out[vnet], false);
	}
	
      }
    }

    // We should only wake up when we use the bandwidth
    // This is only mostly true
    // assert(bw_remaining != getLinkBandwidth());

    // Record that we used some or all of the link bandwidth this cycle
    double ratio = 1.0 - (double(bw_remaining) / double(getLinkBandwidth()));

    // If ratio = 0, we used no bandwidth, if ratio = 1, we used all
    m_link_utilization_proxy += ratio;

    if (bw_remaining > 0 && !schedule_wakeup) {
        // We have extra bandwidth and our output buffer was
        // available, so we must not have anything else to do until
        // another message arrives.
        DPRINTF(RubyNetwork, "%s not scheduled again\n", *this);
    } else {
        DPRINTF(RubyNetwork, "%s scheduled again\n", *this);

        // We are out of bandwidth for this cycle, so wakeup next
        // cycle and continue
        scheduleEvent(Cycles(1));
    }

}

void
Throttle::regStats(string parent)
{
    m_link_utilization
        .name(parent + csprintf(".throttle%i", m_node) + ".link_utilization");

    for (MessageSizeType type = MessageSizeType_FIRST;
         type < MessageSizeType_NUM; ++type) {
        m_msg_counts[(unsigned int)type]
            .init(Network::getNumberOfVirtualNetworks())
            .name(parent + csprintf(".throttle%i", m_node) + ".msg_count." +
                    MessageSizeType_to_string(type))
            .flags(Stats::nozero)
            ;
        m_msg_bytes[(unsigned int) type]
            .name(parent + csprintf(".throttle%i", m_node) + ".msg_bytes." +
                    MessageSizeType_to_string(type))
            .flags(Stats::nozero)
            ;

        m_msg_bytes[(unsigned int) type] = m_msg_counts[type] * Stats::constant(
                Network::MessageSizeType_to_int(type));
    }
}

void
Throttle::clearStats()
{
    m_link_utilization_proxy = 0;
}

void
Throttle::collateStats()
{
    m_link_utilization = 100.0 * m_link_utilization_proxy
        / (double(g_system_ptr->curCycle() - g_ruby_start));
}

void
Throttle::print(ostream& out) const
{
    ccprintf(out,  "[%i bw: %i]", m_node, getLinkBandwidth());
}

int
network_message_to_size(NetworkMessage* net_msg_ptr)
{
    assert(net_msg_ptr != NULL);

    int size = Network::MessageSizeType_to_int(net_msg_ptr->getMessageSize());
    size *=  MESSAGE_SIZE_MULTIPLIER;

    // Artificially increase the size of broadcast messages
    if (BROADCAST_SCALING > 1 && net_msg_ptr->getDestination().isBroadcast())
        size *= BROADCAST_SCALING;

    return size;
}
